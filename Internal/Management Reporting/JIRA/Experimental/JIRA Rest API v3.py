# This code sample uses the 'requests' library:
# http://docs.python-requests.org
import requests
from requests.auth import HTTPBasicAuth
import json

url = "https://selectedinterventions.atlassian.net/rest/api/3/issue/ON-1396/worklog"

auth = HTTPBasicAuth("tim.cattermoul@selectedinterventions.com", "MGUtL8vlACPdsI9lrSJO68B9")

headers = {
  "Accept": "application/json"
}

response = requests.request(
   "GET",
   url,
   headers=headers,
   auth=auth
)

print(json.dumps(json.loads(response.text), sort_keys=True, indent=4, separators=(",", ": ")))

import pandas as pd

# Convert nested JSON to dataframe
df = pd.json_normalize(json.dumps(json.loads(response.text), sort_keys=True, indent=4, separators=(",", ": ")), sep='_')

# Print the dataframe
print(df.head())