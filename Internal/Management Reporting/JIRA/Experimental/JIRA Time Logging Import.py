# import the installed Jira library
from jira import JIRA

# Specify a server key. It should be your
# domain name link. yourdomainname.atlassian.net
jiraOptions = {'server': "https://selectedinterventions.atlassian.net"}

# Get a JIRA client instance, pass,
# Authentication parameters
# and the Server name.
# emailID = your emailID
# token = token you receive after registration
jira = JIRA(options=jiraOptions, basic_auth=(
    "tim.cattermoul@selectedinterventions.com", "MGUtL8vlACPdsI9lrSJO68B9"))

# While fetching details of a single issue,
# pass its UniqueID or Key.
singleIssue = jira.issue('ECHO-19812')
print('{}'.format(singleIssue.fields))
# print('{}: {}:{}'.format(singleIssue.key,
#                          singleIssue.fields.worklog,
#                          singleIssue.fields.summary,
#                          singleIssue.fields.reporter.displayName))

# for issue in jira.search_issues('project=Onboard', maxResults=100):
#      print('{}: {}'.format(issue.key, issue.fields.summary))

