from jira import JIRA
import pandas as pd
from datetime import datetime, timedelta
from sqlalchemy import create_engine
import pymysql
import mysql.connector

# Set up JIRA connection
jiraOptions = {'server': "https://selectedinterventions.atlassian.net"}
jira = JIRA(options=jiraOptions, basic_auth=(
    "tim.cattermoul@selectedinterventions.com", "MGUtL8vlACPdsI9lrSJO68B9"))

# Calculate the date 3 months ago
three_months_ago = datetime.now() - timedelta(days=7)
updated_date_str = three_months_ago.strftime('%Y-%m-%d')

# Define JQL query to retrieve issues updated in the last 3 months across all projects
jql_query = f'updated >= {updated_date_str}'

# Retrieve issues using JQL query
issues = jira.search_issues(jql_query, maxResults=0)
print(issues)
# Create empty dataframe to store worklogs
worklogs_df = pd.DataFrame(columns=['worklog_id','issue_key', 'author_name', 'author_email', 'comment', 'time_spent', 'time_logged'])

# Loop through issues and retrieve worklogs
for issue in issues:
    worklogs = jira.worklogs(issue)
    for worklog in worklogs:
        # Extract relevant data from worklog
        worklog_id = worklog.id
        issue_key = issue.key
        author_name = worklog.author.displayName
        author_email = worklog.author.emailAddress
        #comment = worklog.comment.text
        time_spent = worklog.timeSpentSeconds
        time_logged = pd.to_datetime(worklog.created)

        # Append data to dataframe
        worklogs_df = worklogs_df.append({'worklog_id':worklog_id, 'issue_key': issue_key, 'author_name': author_name, 'author_email': author_email, 'time_spent': time_spent, 'time_logged': time_logged}, ignore_index=True)

# Print dataframe
print(worklogs_df.head())

worklogs_df['timedataextracted'] = datetime.now()

# Set up MySQL connection
conn = mysql.connector.connect(
    user='reportinguser@simariadb', password='Hgy{<hNbWVPW6Cbb',
    host='simariadb.mariadb.database.azure.com',
    database='s_managementreporting',
    ssl_ca=r'C:\Users\tim.cattermoul\Documents\MariaDB SQL Certificate (DO NOT DELETE)\BaltimoreCyberTrustRoot.crt.pem'
)

# Define the name of the table to insert the data
table_name = 'si_engineerworkstats_jira'

# Create engine to connect to the database
engine = create_engine(r'mysql+mysqlconnector://reportinguser@simariadb:Hgy{<hNbWVPW6Cbb@simariadb.mariadb.database.azure.com/s_managementreporting')

# Insert the data into the table, overwriting any existing data
worklogs_df.to_sql(table_name, engine, if_exists='replace', index=False)

# Close MySQL connection
conn.close()


