SELECT *, CASE WHEN roundinstanceplans = 0 then 0 ELSE ROUND(cast(Roundinstanceplans as float)/cast(DailyAverage as float),2) END as Pct FROM (

	select DN.daydate, DATENAME(weekday,daydate) as DoW, ISNULL(Rip.Roundinstanceplans,0) Roundinstanceplans,
	AVG(ISNULL(Rip.Roundinstanceplans,0)) OVER(PARTITION BY DATENAME(weekday,daydate)  order by daydate rows between 26 preceding and current row) AS DailyAverage

	from daynums DN

	LEFT JOIN (select CAST(lastupdateddate as Date) as Day, 
	COUNT(DISTINCT roundinstanceplanID) as RoundInstancePLans /*,
	AVG(COUNT(DISTINCT roundinstanceplanID)) OVER(PARTITION BY DATENAME(weekday,lastupdateddate) order by CAST(lastupdateddate as Date) rows between 6 preceding and current row) AS DailyAverage
	*/
	from roundinstanceplans



	group by CAST(lastupdateddate as Date) ,DATENAME(weekday,lastupdateddate)
	) as RIP on RIP.day = CAST(dn.daydate as Date)


	WHERE daydate between '2023-03-01 00:00:00.000' and getdate()
) as A

	where datediff(day,daydate,getdate()) = 1 and CASE WHEN roundinstanceplans = 0 then 0 ELSE ROUND(cast(Roundinstanceplans as float)/cast(DailyAverage as float),2) END <= 0.25

	order by 1