from FactImport import importFact
from DimensionImport import importDimension
import os

try:
    #importFact('VEOLIAFRSQLPROD\VEOLIAFRSQLPROD1,10001','VeoliaFr_Echo_Prod',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia France\Queries\FactRoundInstances.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','fact_roundinstances_france','replace',False,'roundinstanceID')
    #importFact('VEOLIAFRSQLPROD\VEOLIAFRSQLPROD1,10001','VeoliaFr_Echo_Prod',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia France\Queries\FactLiftMatching.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','fact_liftmatching_france','replace',False,'gpseventID')
    #importFact('VEOLIAFRSQLPROD\VEOLIAFRSQLPROD1,10001','VeoliaFr_Echo_Prod',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia France\Queries\FactLifts.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','fact_lifts_france','replace',False,'gpseventID')
    #importFact('VEOLIAFRSQLPROD\VEOLIAFRSQLPROD1,10001','VeoliaFr_Echo_Prod',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia France\Queries\FactDelayedLiftsTEMP.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','fact_delayedlifts_france','replace',False,'gpseventID')
    importFact('VEOLIAFRSQLPROD\VEOLIAFRSQLPROD1,10001','VeoliaFr_Echo_Prod',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia France\Queries\FactPings.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','fact_pings_france','replace',False,'Day')
    importFact('VEOLIAFRSQLPROD\VEOLIAFRSQLPROD1,10001','VeoliaFr_Echo_Prod',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia France\Queries\FactWorkingHours.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','fact_workinghours_france','replace',False,'Day')
    #importDimension('VEOLIAFRSQLPROD\VEOLIAFRSQLPROD1,10001','VeoliaFr_Echo_Prod',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia France\Queries\DimRounds.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','dim_rounds_france','replace',False)

    import sqlalchemy

    engine = sqlalchemy.create_engine("mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",
                                      connect_args=dict(host='echobivuk.mariadb.database.azure.com', port=3306,
                                                        ssl_ca=r'C:\Users\tim.cattermoul\Documents\MariaDB SQL Certificate (DO NOT DELETE)\BaltimoreCyberTrustRoot.crt.pem'))

    import subprocess, sys
    print("Importing Onboard Crash Data")
    p = subprocess.Popen(["powershell.exe",
                  r'&"C:\Users\tim.cattermoul\Documents\Repositories\Veolia France\Scripts\OnboardCrashData.ps1"'],
                  stdout=sys.stdout)
    p.communicate()

    with engine.begin() as preConn:
        #print("Updating Missing Lifts")
        #preConn.execute("CALL create_fact_missinglifts_france();")
        #preConn.execute("CALL create_fact_duplicatelifts_france();")
        #print("Missing Lifts Updated")
        print("Calculating crashes in shift")
        preConn.execute("CALL update_crashesinshift_france();")
        print("Calculation complete")
        print("Updating Device Contracts")
        preConn.execute("CALL update_devicecontracts_france();")
        print("Device Contracts Updated")

except Exception as e:

    from ScriptFailureEmail import sendfailedscriptemail

    sendfailedscriptemail(os.path.basename(__file__), str(e))




