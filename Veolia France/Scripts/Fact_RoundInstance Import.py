import pyodbc
import pandas as pd
import datetime
import subprocess, sys, os
import sqlalchemy
import pymysql
from mysql.connector import (connection)

conn = pyodbc.connect('Driver={SQL Server Native Client 11.0};'
                      'Server=redmssqlprod-03.echo.services;'
                      'Database=Echo2_Commercial;'
                      'Trusted_Connection=yes;')

cursor = conn.cursor()
query = open(r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Commercial\Queries\FactRoundInstances.sql','r')

sql_query = pd.read_sql_query(query.read(),conn)

engine = sqlalchemy.create_engine("mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",
                       connect_args= dict(host='echobivuk.mariadb.database.azure.com', port=3306, ssl_ca=r'C:\Users\tim.cattermoul\Documents\MariaDB SQL Certificate (DO NOT DELETE)\BaltimoreCyberTrustRoot.crt.pem'))

conn2 = engine.connect()

sql_query.to_sql('temporary_table', con=conn2, if_exists='replace', index=False)

qryRemoveExisting = r'DELETE FROM fact_roundinstances_commercial WHERE roundinstanceID IN (SELECT F.roundinstanceID FROM fact_roundinstances_commercial F INNER JOIN temporary_table T ON T.roundinstanceID = F.roundinstanceID);'
qryAddValues = r'INSERT INTO fact_roundinstances_commercial SELECT * FROM temporary_table;'
qryDropTemp = r'DROP TABLE temporary_table;'
#
with engine.begin() as connection:
     connection.execute(qryRemoveExisting)
     connection.execute(qryAddValues)
     connection.execute(qryDropTemp)

print("Data imported to ")