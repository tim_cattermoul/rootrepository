from FactImport import importFact
from DimensionImport import importDimension
import os

try:
    importFact('echosqlprod-01.echo.services\cdiffsqlprod_01,10005','CDiff_Echo2_Prod',r'C:\Users\tim.cattermoul\Documents\Repositories\Cardiff\Queries\FactRoundInstances.sql',"mysql+pymysql://onboard@simariadb:afRQcMuD6j@simariadb.mariadb.database.azure.com/slareporting",'simariadb.mariadb.database.azure.com','fact_roundinstances_cardiff','replace',False,'roundinstanceID')
    #importFact('echosqlprod-01.echo.services\cdiffsqlprod_01,10005','CDiff_Echo2_Prod',r'C:\Users\tim.cattermoul\Documents\Repositories\Cardiff\Queries\FactLiftMatching.sql',"mysql+pymysql://onboard@simariadb:afRQcMuD6j@simariadb.mariadb.database.azure.com/slareporting",'simariadb.mariadb.database.azure.com','fact_liftmatching_cardiff','replace',False,'gpseventID')
    importFact('echosqlprod-01.echo.services\cdiffsqlprod_01,10005','CDiff_Echo2_Prod',r'C:\Users\tim.cattermoul\Documents\Repositories\Cardiff\Queries\FactLifts.sql',"mysql+pymysql://onboard@simariadb:afRQcMuD6j@simariadb.mariadb.database.azure.com/slareporting",'simariadb.mariadb.database.azure.com','fact_lifts_cardiff','replace',False,'gpseventID')
    #importFact('echosqlprod-01.echo.services\cdiffsqlprod_01,10005','CDiff_Echo2_Prod',r'C:\Users\tim.cattermoul\Documents\Repositories\Cardiff\Queries\FactDelayedLiftsTEMP.sql',"mysql+pymysql://onboard@simariadb:afRQcMuD6j@simariadb.mariadb.database.azure.com/slareporting",'simariadb.mariadb.database.azure.com','fact_delayedlifts_cardiff','replace',False,'gpseventID')
    importFact('echosqlprod-01.echo.services\cdiffsqlprod_01,10005','CDiff_Echo2_Prod',r'C:\Users\tim.cattermoul\Documents\Repositories\Cardiff\Queries\FactPings.sql',"mysql+pymysql://onboard@simariadb:afRQcMuD6j@simariadb.mariadb.database.azure.com/slareporting",'simariadb.mariadb.database.azure.com','fact_pings_cardiff','replace',False,'Day')
    importFact('echosqlprod-01.echo.services\cdiffsqlprod_01,10005','CDiff_Echo2_Prod',r'C:\Users\tim.cattermoul\Documents\Repositories\Cardiff\Queries\FactWorkingHours.sql',"mysql+pymysql://onboard@simariadb:afRQcMuD6j@simariadb.mariadb.database.azure.com/slareporting",'simariadb.mariadb.database.azure.com','fact_workinghours_cardiff','replace',False,'Day')
    #importDimension('echosqlprod-01.echo.services\cdiffsqlprod_01,10005','CDiff_Echo2_Prod',r'C:\Users\tim.cattermoul\Documents\Repositories\Cardiff\Queries\DimRounds.sql',"mysql+pymysql://onboard@simariadb:afRQcMuD6j@simariadb.mariadb.database.azure.com/slareporting",'simariadb.mariadb.database.azure.com','dim_rounds_cardiff','replace',False)

    import sqlalchemy

    engine = sqlalchemy.create_engine("mysql+pymysql://onboard@simariadb:afRQcMuD6j@simariadb.mariadb.database.azure.com/slareporting",
                                      connect_args=dict(host='simariadb.mariadb.database.azure.com', port=3306,
                                                        ssl_ca=r'C:\Users\tim.cattermoul\Documents\MariaDB SQL Certificate (DO NOT DELETE)\BaltimoreCyberTrustRoot.crt.pem'))

    import subprocess, sys
    print("Importing Onboard Crash Data")
    p = subprocess.Popen(["powershell.exe",
                  r'&"C:\Users\tim.cattermoul\Documents\Repositories\Cardiff\Scripts\OnboardCrashData.ps1"'],
                  stdout=sys.stdout)
    p.communicate()

    with engine.begin() as preConn:
        #print("Updating Missing Lifts")
        #preConn.execute("CALL create_fact_missinglifts_cardiff();")
        #preConn.execute("CALL create_fact_duplicatelifts_cardiff();")
        #print("Missing Lifts Updated")
        print("Calculating crashes in shift")
        preConn.execute("CALL update_crashesinshift_cardiff();")
        print("Calculation complete")
        print("Updating Device Contracts")
        preConn.execute("CALL update_devicecontracts_cardiff();")
        print("Device Contracts Updated")

except Exception as e:

    from ScriptFailureEmail import sendfailedscriptemail

    sendfailedscriptemail(os.path.basename(__file__), str(e))




