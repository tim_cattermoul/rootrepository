import pyodbc
import pandas as pd
import datetime
import subprocess, sys, os

try:
    conn = pyodbc.connect('Driver={SQL Server Native Client 11.0};'
                          'Server=redmssqlprod-03.echo.services;'
                          'Database=Echo2_Commercial;'
                          'Trusted_Connection=yes;')

    cursor = conn.cursor()
    query = open(r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Commercial\Queries\FactLiftMatching.sql','r')

    sql_query = pd.read_sql_query(query.read(),conn)


    if len(sql_query.index) > 0:
        dir_name = r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Commercial\Extracts'
        base_filename = 'Veolia Commercial Lift Matching Extract'
        now = datetime.date.today()
        fileformat = 'csv'
        fullpath = os.path.join(dir_name,base_filename+"."+fileformat)
        sql_query.to_csv(fullpath,index=False, header=True)
        print("Lift Matching Extract File produced")

        p = subprocess.Popen(["powershell.exe",
                              r'&"C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Commercial\Scripts\Fact_LiftMatching Import.ps1"'],
                             stdout=sys.stdout)
        p.communicate()
        print("Imported Lift Matching File Extract")

except Exception as e:

    from ScriptFailureEmail import sendfailedscriptemail

    sendfailedscriptemail(os.path.basename(__file__), str(e))