"""
This call sends a message to one recipient.
"""
from mailjet_rest import Client
from mysql.connector import (connection)
import datetime

import os
api_key = '1df323b2ff08c5c96b1576a3420d92f9'
api_secret = '7df40f4a7ebf616c74d85968cb5ce385'

def sendfailedscriptemail(filename,exception):
	mailjet = Client(auth=(api_key, api_secret), version='v3.1')
	data = {
	  'Messages': [
					{
							"From": {
									"Email": "no-reply@selectedinterventions.com",
									"Name": "SI Automated Script Monitoring"
							},
							"To": [
									{
											"Email": "tim.cattermoul@selectedinterventions.com",
											"Name": "Tim Cattermoul"
									}
							],
							"Subject": "Script Execution Failure: "+ filename,
							"TextPart": "This script file failed to execute properly. The exception is: " + exception,
							}
			]
	}
	result = mailjet.send.create(data=data)

	cnx = connection.MySQLConnection(user='tim.cattermoul@echobivuk', password='PHFLC7Rf4TRp',
									 host='echobivuk.mariadb.database.azure.com',
									 database='echoreporting',
									 ssl_ca=r'C:\Users\tim.cattermoul\Documents\MariaDB SQL Certificate (DO NOT DELETE)\BaltimoreCyberTrustRoot.crt.pem'
									 # autocommit=True
									 )
	cursor2 = cnx.cursor()

	sql = "INSERT INTO echoreporting.log_pythonscriptfailures (Filename,Exception,FailureDateTime) VALUES (%s,%s,%s)"
	cursor2.execute(sql,(filename,exception,str(datetime.datetime.today())))
	# the connection is not auto committed by default, so we must commit to save our changes
	cnx.commit()
	cursor2.close()
	cnx.close()

