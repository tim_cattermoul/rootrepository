WITH CTE_Lifts as (

	select
       GPS.gpseventID,
	   GPS.gpseventdate,
	   R.resourceID,
	   C.ContractID,
	   TD.deviceID,
	   GPS.guid,

	   TD.lat,
	   TD.long

from
       gpsevents GPS with (nolock)

INNER JOIN gpstraildata TD with (nolock) on TD.gpstraildataID = GPS.gpstraildataID
		left join resources R on (GPS.echotypeID =54 and GPS.echoID=R.resourceID)
		left join contracts C on C.contractID = R.contractID
         
where
       gpseventtypeID=26
       --and echotypeId=54
         and datediff(hour,gpseventdate,getdate()) <= 24     
),CTE_Tasklines as (
select t.taskID, tl.tasklineID, tl.gpseventguid, tl.completeddate, tl.autoconfirmed, T.lat, T.long


from tasklines tl
INNER JOIN Tasks T on T.taskid = tl.taskid
WHERE tl.gpseventguid in (SELECT distinct guid from CTE_Lifts)--DATEDIFF(day,T.taskscheduleddate,getdate()) = 0
),CTE_Tasks as (
select t.taskID, t.gpseventguid, T.autoconfirmed, T.lat, T.long

--into #tasks
from tasks T
WHERE gpseventguid in (SELECT distinct guid from CTE_Lifts)--convert(date,T.taskscheduleddate) >= '2021-06-18 00:00:00.000' AND DATEDIFF(day,T.taskscheduleddate,getdate()) = 0
)/*,CTE_Inspections as (
select I.inspectionID, I.inspectioncreateddate, I.sourceobjectdesc, GI.* 
--into #inspections
from inspections I
left join gpseventinspections GI on gi.targetechoID = I.inspectionID
where GI.sourceGUID in (SELECT distinct guid from CTE_Lifts)--CONVERT(DATE,inspectioncreateddate) >= '2021-06-18 00:00:00.000' AND DATEDIFF(day,inspectioncreateddate,getdate()) = 0
)*/
,CTE_Combined as (
SELECT L.gpseventID, CONVERT(date,L.gpseventdate) as Day, L.guid, L.contractID, L.resourceID, L.deviceID, L.lat, L.long, T.taskID, TL.tasklineID, /*I.inspectionID,*/ 
 
CASE WHEN t.taskID IS NOT NULL AND tl.tasklineID IS NULL THEN 'Task' WHEN TL.tasklineID IS NOT NULL THEN 'Taskline' /*WHEN I.inspectionID IS NOT NULL THEN 'Inspection'*/ ELSE 'Unmatched' END as LiftStatus, CASE WHEN T.taskID IS NULL AND tl.tasklineID IS NULL /*AND I.inspectionID IS NULL*/ THEN 0 ELSE 1 END AS Matched, 
CASE WHEN t.taskID IS NOT NULL AND tl.tasklineID IS NULL THEN T.autoconfirmed WHEN TL.tasklineID IS NOT NULL THEN TL.autoconfirmed ELSE NULL END as Autoconfirmation,
CASE	WHEN t.taskID IS NOT NULL AND tl.tasklineID IS NULL THEN GEOGRAPHY::Point(L.lat, L.long, 4326).STDistance(GEOGRAPHY::Point(T.lat, T.long, 4326))
		WHEN TL.tasklineID IS NOT NULL THEN GEOGRAPHY::Point(L.lat, L.long, 4326).STDistance(GEOGRAPHY::Point(TL.lat, TL.long, 4326)) ELSE NULL END as LiftProximity,
tl.completeddate as TLcompleteddate, /*I.inspectioncreateddate,*/ CASE WHEN CASE WHEN t.taskID IS NOT NULL AND tl.tasklineID IS NULL THEN T.autoconfirmed WHEN TL.tasklineID IS NOT NULL THEN TL.autoconfirmed ELSE NULL END = 3 THEN 1 ELSE 0 END as AutoMatched

FROM CTE_Lifts L
LEFT JOIN CTE_Tasks T on T.gpseventguid = L.guid
LEFT JOIN CTE_Tasklines TL on TL.gpseventguid = L.guid
--LEFT JOIN CTE_Inspections I on I.sourceGUID = L.guid
)

SELECT *,
CASE WHEN Autoconfirmation = 1 THEN 'Proximity' WHEN Autoconfirmation = 2 THEN 'RFID' WHEN Autoconfirmation = 3 THEN 'Autoconfirmed' WHEN Autoconfirmation = 4 THEN 'GPS Event' WHEN Autoconfirmation IS NULL THEN 'Unmatched' END as ConfirmationMethod
,getDate() as TimeDataExtracted

FROM CTE_Combined


