select 

DISTINCT NULL as ID, deviceID,r.contractID, convert(date,receiveddate) as Day, echotypeID, echoID, ra.roundinstanceID, COUNT(*) as Pings, MAX(receiveddate) as MostRecentPing 


from gpstraildata G with (nolock)

LEFT JOIN resources R on R.resourceID = G.echoID
left join resourceallocations RA on RA.resourceID = echoID and convert(date,RA.startdate) = convert(date,receiveddate) and receiveddate between RA.startdate and RA.enddate

WHERE DATEDIFF(day,receiveddate,getdate()) <= 1 and echotypeID = 54 and G.speed > 5

GROUP BY deviceID, r.contractID, convert(date,receiveddate), echotypeID, echoID,  ra.roundinstanceID