SELECT DISTINCT
			A.roundinstanceID,
			roundgroup,
			round,
			R.resource,
			A.startdate, 
			finishdate,
			ss_date,
			sf_date,
			FirstBreak,
			ret.roundeventtype,
			rie.eventstartdatetime,
			rie.eventenddatetime,
			SecondBreak,
			ret2.roundeventtype,
			rie2.eventstartdatetime,
			rie2.eventenddatetime,
			CASE WHEN ret3.roundeventtype IS NOT NULL THEN rie3.roundinstanceeventID ELSE NULL END as TippingRIEID,
			ret3.roundeventtype,
			CASE WHEN ret3.roundeventtype IS NOT NULL THEN rie3.eventstartdatetime ELSE NULL END as tippingstarttime,
			CASE WHEN ret3.roundeventtype IS NOT NULL THEN rie3.eventenddatetime ELSE NULL END as tippingendtime
			

FROM (
		select 
			DISTINCT
			ri.roundinstanceID,
			rg.roundgroup,
			r.round,
			ri.startdate, 
			ri.finishdate,
			ri.ss_date,
			ri.sf_date,
			MIN(rie.roundinstanceeventID) as FirstBreak,
			CASE WHEN MIN(rie.roundinstanceeventID) = MAX(rie.roundinstanceeventID) THEN NULL ELSE MAX(rie.roundinstanceeventID) END as SecondBreak
			--ds.devicesessionID, 
			--ds.deviceID, 
			--d.model, 
			--ds.userID, 
			--u.username, 
			--ds.startdate, 
			--ds.enddate, 
			--ds.lastaccessed 

		from roundinstances RI

		INNER JOIN devicesessions ds on ri.roundinstanceID = ds.associated_echoID and ds.associated_echotypeID = 135
		INNER JOIN rounds r on r.roundID = RI.roundID
		INNER JOIN roundgroups rg on rg.roundgroupID = r.roundgroupID
		INNER JOIN users u on u.userID = ds.userID
		inner join devices d on d.deviceID = ds.deviceID
		left join roundinstanceevents RIE on RIE.roundinstanceID = RI.roundinstanceID
		left join roundeventtypes RET on RET.roundeventtypeID = RIE.roundeventtypeID and roundeventtype like '%Break%'

		where ds.startdate between '2023-06-12 00:00:00.000' and '2023-06-19 00:00:00.000'

		and ri.contractID = 1 -- Casey

		--order by 1 asc

	GROUP BY 
			ri.roundinstanceID,
			rg.roundgroup,
			r.round,
			ri.startdate, 
			ri.finishdate,
			ri.ss_date,
			ri.sf_date) as A

			LEFT JOIN roundinstanceevents RIE on rie.roundinstanceeventID = A.FirstBreak
			LEFT JOIN roundeventtypes RET on ret.roundeventtypeID = rie.roundeventtypeID
			LEFT JOIN roundinstanceevents RIE2 on rie2.roundinstanceeventID = A.SecondBreak
			LEFT JOIN roundeventtypes RET2 on ret2.roundeventtypeID = rie2.roundeventtypeID
			LEFT JOIN roundinstanceevents RIE3 on rie3.roundinstanceID = A.roundinstanceID
			LEFT JOIN roundeventtypes RET3 on ret3.roundeventtypeID = rie3.roundeventtypeID and ret3.roundeventtype like '%Tip%'			
			LEFT JOIN (SELECT DISTINCT RA.roundinstanceID, R.ResourceID, Resource FROM ResourceAllocations RA INNER JOIN resources R on R.resourceID = RA.resourceID INNER JOIN resourcetypes RT on RT.resourcetypeID = R.resourcetypeID where resourceclassID = 1) R on R.roundinstanceID = A.roundinstanceID
			


ORDER BY a.roundinstanceID asc