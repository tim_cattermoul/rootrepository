with CTE_RI as (

select roundinstanceID, COALESCE(startdate,ss_date) as startdate, coalesce(finishdate,sf_date) as finishdate, datediff(minute,COALESCE(startdate,ss_date),coalesce(finishdate,sf_date)) as duration from roundinstances RI

where RI.contractID = 1 -- Casey
and ri.ss_date  between '2023-06-16 00:00:00.000' and '2023-06-17 00:00:00.000' 

)

,cte_RIE as
(
select 
	RIE.roundinstanceeventID,
	R.resource,
	RIE.roundinstanceID,
	RIE.roundeventtypeID,
	RET.roundeventtype,
	RIE.eventdatetime,
	RIE.eventstartdatetime,
	RIE.eventenddatetime,
	DATEDIFF(minute,RIE.eventstartdatetime,RIE.eventenddatetime) as Duration


from roundinstanceevents RIE

INNER JOIN roundinstances RI on RI.roundinstanceID = RIE.roundinstanceID
INNER JOIN roundeventtypes RET on RET.roundeventtypeID =RIE.roundeventtypeID
INNER join resourceallocations RA on RA.roundinstanceID = RI.roundinstanceID
INNER JOIN resources R on RA.resourceID = R.resourceID
INNER JOIN resourcetypes RT on RT.resourcetypeID = R.resourcetypeID

--where datediff(day,RI.ss_date,getdate()) = 1
where RI.contractID = 1 -- Casey
and ri.ss_date  between '2023-05-01 00:00:00.000' and '2023-06-19 00:00:00.000' 
and RT.resourceclassID = 1 -- vehicle
order by roundeventtype
)

, CTE_Lifts as (
-- Without Join 29214 rows
-- Left/Inner Join 31942 rows


select DISTINCT RA.roundinstanceID, R.resource, GE.gpseventID, GE.gpseventdate, GE.gpstraildataID

from gpsevents GE with (nolock) 

INNER JOIN resources R on R.resourceID = GE.echoID and GE.echotypeID = 54
INNER join resourceallocations RA on RA.resourceID = R.resourceID and gpseventdate between RA.startdate and RA.enddate

where gpseventdate > '2023-06-16 00:00:00.000' 

and R.contractID = 1 -- Casey
and gpseventtypeID in (1,26) -- Bin Lift, Bin Lift Count
)
, CTE_Trail as (
select TD.gpstraildataID, TD.positiondate, echoID, deviceID, resource from gpstraildata TD with (nolock)

INNER JOIN resources R on R.resourceID = TD.echoID and TD.echotypeID = 54

where r.contractID = 1 

and td.positiondate between '2023-06-16 00:00:00.000' and '2023-06-17 00:00:00.000' 
)

,CTE_CombinedEvents as (
SELECT roundinstanceID, gpstraildataID as ID, gpseventdate as eventdate, 'Bin Lift' as Action, 0 as Duration from CTE_Lifts

UNION

select roundinstanceID, roundinstanceeventID, eventstartdatetime, roundeventtype, Duration from CTE_RIE

--Order by 3
)

, CTE_Exclusions as (
SELECT *, datediff(second,previousactiondate,eventdate) as TimeDifferenceSecs,CASE WHEN PreviousAction = 'Tipping' THEN datediff(second,previousactiondate,eventdate) END as Exclude /* Add in logic for travel from tip and also to and from depot*/ FROM (

	select ri.roundinstanceID, ri.startdate, ri.finishdate, ri.duration as roundduration, ce.ID, ce.eventdate, ce.action, ce.duration, LAG(Action) OVER (PARTITION BY ri.roundinstanceID order by eventdate) as PreviousAction 
	,LAG(eventdate) OVER (PARTITION BY ri.roundinstanceID order by eventdate) as PreviousActionDate

	from CTE_RI RI

	INNER JOIN CTE_CombinedEvents CE on CE.roundinstanceID = RI.roundinstanceID) as A
)

SELECT DISTINCT RI.*, SUM(CASE WHEN E.Action = 'Bin Lift' then 1 END) as Lifts, SUM(E.duration) as Breaks, ROUND(SUM(Exclude/60),2) as TravelToTip FROM CTE_RI RI

INNER JOIN CTE_Exclusions E on E.roundinstanceID = RI.roundinstanceID

group by RI.roundinstanceID, RI.startdate, RI.finishdate, RI.duration

