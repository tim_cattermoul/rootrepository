from FactImport import importFact
from DimensionImport import importDimension
import os

try:
    #importFact('CWAYAZSQLPROD01.echo.services','CleanAway_Prod',r'C:\Users\tim.cattermoul\Documents\Repositories\Cleanaway Australia\Queries\FactRoundInstances.sql',"mysql+pymysql://tim.cattermoul@echobicway:echobicway.mariadb.database.azure.com@echobicway.mariadb.database.azure.com/echoreporting",'echobicway.mariadb.database.azure.com','fact_roundinstances_cleanawayaus','replace',False,'roundinstanceID')
    #importFact('CWAYAZSQLPROD01.echo.services','CleanAway_Prod',r'C:\Users\tim.cattermoul\Documents\Repositories\Cleanaway Australia\Queries\FactLiftMatching.sql',"mysql+pymysql://tim.cattermoul@echobicway:echobicway.mariadb.database.azure.com@echobicway.mariadb.database.azure.com/echoreporting",'echobicway.mariadb.database.azure.com','fact_liftmatching_cleanawayaus','replace',False,'gpseventID')
    #importFact('CWAYAZSQLPROD01.echo.services','CleanAway_Prod',r'C:\Users\tim.cattermoul\Documents\Repositories\Cleanaway Australia\Queries\FactLifts.sql',"mysql+pymysql://tim.cattermoul@echobicway:echobicway.mariadb.database.azure.com@echobicway.mariadb.database.azure.com/echoreporting",'echobicway.mariadb.database.azure.com','fact_lifts_cleanawayaus','replace',False,'gpseventID')
    #importFact('CWAYAZSQLPROD01.echo.services','CleanAway_Prod',r'C:\Users\tim.cattermoul\Documents\Repositories\Cleanaway Australia\Queries\FactDelayedLiftsTEMP.sql',"mysql+pymysql://tim.cattermoul@echobicway:echobicway.mariadb.database.azure.com@echobicway.mariadb.database.azure.com/echoreporting",'echobicway.mariadb.database.azure.com','fact_delayedlifts_cleanawayaus','replace',False,'gpseventID')
    importFact('CWAYAZSQLPROD01.echo.services','CleanAway_Prod',r'C:\Users\tim.cattermoul\Documents\Repositories\Cleanaway Australia\Queries\FactPings.sql',"mysql+pymysql://tim.cattermoul@echobicway:N?8sa9tH_7r$CD:}@echobicway.mariadb.database.azure.com/echoreporting",'echobicway.mariadb.database.azure.com','fact_pings_cleanawayaus','replace',False,'Day')
    importFact('CWAYAZSQLPROD01.echo.services','CleanAway_Prod',r'C:\Users\tim.cattermoul\Documents\Repositories\Cleanaway Australia\Queries\FactWorkingHours.sql',"mysql+pymysql://tim.cattermoul@echobicway:N?8sa9tH_7r$CD:}@echobicway.mariadb.database.azure.com/echoreporting",'echobicway.mariadb.database.azure.com','fact_workinghours_cleanawayaus','replace',False,'Day')
    #importDimension('CWAYAZSQLPROD01.echo.services','CleanAway_Prod',r'C:\Users\tim.cattermoul\Documents\Repositories\Cleanaway Australia\Queries\DimRounds.sql',"mysql+pymysql://tim.cattermoul@echobicway:echobicway.mariadb.database.azure.com@echobicway.mariadb.database.azure.com/echoreporting",'echobicway.mariadb.database.azure.com','dim_rounds_cleanawayaus','replace',False)

    import sqlalchemy

    engine = sqlalchemy.create_engine("mysql+pymysql://tim.cattermoul@echobicway:N?8sa9tH_7r$CD:}@echobicway.mariadb.database.azure.com/echoreporting",
                                      connect_args=dict(host='echobicway.mariadb.database.azure.com', port=3306,
                                                        ssl_ca=r'C:\Users\tim.cattermoul\Documents\MariaDB SQL Certificate (DO NOT DELETE)\BaltimoreCyberTrustRoot.crt.pem'))

    import subprocess, sys
    print("Importing Onboard Crash Data")
    p = subprocess.Popen(["powershell.exe",
                  r'&"C:\Users\tim.cattermoul\Documents\Repositories\Cleanaway Australia\Scripts\OnboardCrashData.ps1"'],
                  stdout=sys.stdout)
    p.communicate()

    with engine.begin() as preConn:
        #print("Updating Missing Lifts")
        #preConn.execute("CALL create_fact_missinglifts_cleanawayaus();")
        #preConn.execute("CALL create_fact_duplicatelifts_cleanawayaus();")
        #print("Missing Lifts Updated")
        print("Calculating crashes in shift")
        preConn.execute("CALL update_crashesinshift_cleanawayaus();")
        print("Calculation complete")
        print("Updating Device Contracts")
        preConn.execute("CALL update_devicecontracts_cleanawayaus();")
        print("Device Contracts Updated")

except Exception as e:

    from ScriptFailureEmail import sendfailedscriptemail

    sendfailedscriptemail(os.path.basename(__file__), str(e))




