from DimensionImport import importDimension
from FactImport import importFact

importDimension('redmssqlprod-03.echo.services','Echo2',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Municipal\Queries\DimResources.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','dim_resources_municipal','replace',False)
importDimension('redmssqlprod-03.echo.services','Echo2',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Municipal\Queries\DimDeviceConfig.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','dim_deviceconfig_municipal','replace',False)
importDimension('redmssqlprod-03.echo.services','Echo2',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Municipal\Queries\DimContracts.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','dim_contracts_municipal','replace',False)
importDimension('redmssqlprod-03.echo.services','Echo2',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Municipal\Queries\DimDevices.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','dim_devices_municipal','replace',False)
importDimension('redmssqlprod-03.echo.services','Echo2',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Municipal\Queries\DimDeviceEchoSystem.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','dim_deviceechosystems_municipal','append',False)
#importDimension('redmssqlprod-03.echo.services','Echo2l',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Municipal\Queries\FactRoundInstances.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','fact_roundinstances_municipal','replace',False)
importFact('redmssqlprod-03.echo.services','Echo2',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Municipal\Queries\FactDistanceTravelled.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','fact_distancetravelled_municipal','replace',False,'Day')
importFact('redmssqlprod-03.echo.services', 'Echo2',r'C:\Users\tim.cattermoul\Desktop\Queries\SLA Reporting\Veolia Municipal\ECHO Object Transactions per 60 minute block TC v0.5 20230207 MUN.sql',"mysql+pymysql://onboard@simariadb:afRQcMuD6j@simariadb.mariadb.database.azure.com/slareporting",'simariadb.mariadb.database.azure.com', 'sla_sp003_raw_municipal', 'replace', False, 'Block')
importDimension('redmssqlprod-03.echo.services','Echo2',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Municipal\Queries\DimDevices.sql',"mysql+pymysql://onboard@simariadb:afRQcMuD6j@simariadb.mariadb.database.azure.com/slareporting",'simariadb.mariadb.database.azure.com', 'dim_devices_municipal', 'replace', False)
importDimension('redmssqlprod-03.echo.services','Echo2',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Municipal\Queries\DimUsers.sql',"mysql+pymysql://onboard@simariadb:afRQcMuD6j@simariadb.mariadb.database.azure.com/slareporting",'simariadb.mariadb.database.azure.com', 'dim_users_municipal', 'replace', False)
# importDimension('reduatdevsql-01.echo.services','Echo2_LiveCopy',r'C:\Users\tim.cattermoul\Desktop\Queries\Customer Dashboards\Veolia UK\Resource Management Queries\PlannedAllocations v3.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','plannedallocations_municipal','replace',False)
# importDimension('reduatdevsql-01.echo.services','Echo2_LiveCopy',r'C:\Users\tim.cattermoul\Desktop\Queries\Customer Dashboards\Veolia UK\Resource Management Queries\LeaveEntry v2.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','leaveentry_municipal','replace',False)
# importDimension('reduatdevsql-01.echo.services','Echo2_LiveCopy',r'C:\Users\tim.cattermoul\Desktop\Queries\Customer Dashboards\Veolia UK\Resource Management Queries\ResourceActivity v3.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','resourceactivity_municipal','replace',False)

import sqlalchemy

engine = sqlalchemy.create_engine("mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",
                                  connect_args=dict(host='echobivuk.mariadb.database.azure.com', port=3306,
                                                    ssl_ca=r'C:\Users\tim.cattermoul\Documents\MariaDB SQL Certificate (DO NOT DELETE)\BaltimoreCyberTrustRoot.crt.pem'))

with engine.begin() as preConn:
    print("Updating Lift Integrations")
    preConn.execute("CALL update_resourceliftintegrations_municipal();")
    print("Lift Integrations Updated")
    print("Updating Target Onboard Versions")
    preConn.execute("CALL update_targetversion_municipal()")
    print("Target Versions Updated")
    print("Calculating Emissions")
    preConn.execute("CALL update_distancetravelledemissions_municipal();")
    preConn.execute("CALL create_fact_ghgemissions_municipal();")
    print("Emissions Calculated")