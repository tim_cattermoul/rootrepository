import mysql.connector
import pyodbc
import time

def importFactv2(server,database,query_file_path,table_name,primary_key_column,user,password,host,importdb):
    # Establish a connection to your MSSQL database
    mssql_connection = pyodbc.connect('Driver={SQL Server Native Client 11.0};'
                          'Server='+server+';'
                          'Database='+database+';'
                          'Trusted_Connection=yes;')
    mssql_cursor = mssql_connection.cursor()
    print(f"Starting update of table: {table_name}")
    # Execute the query to retrieve the data from MSSQL
    with open(query_file_path, 'r') as query_file:
        query = query_file.read()
    mssql_cursor.execute(query)
    mssql_data = mssql_cursor.fetchall()
    print("Data extracted")

    # Establish a connection to your MySQL database
    mysql_connection = mysql.connector.connect(user=user, password=password, host=host, database=importdb)
    mysql_cursor = mysql_connection.cursor()

    # Retrieve the column names from the table
    mysql_cursor.execute(f"SHOW COLUMNS FROM {table_name}")
    column_names = [desc[0] for desc in mysql_cursor.fetchall()]

    # Construct the SQL statement without the primary key column
    column_names_without_pk = [col for col in column_names if col != primary_key_column]
    column_names_str = ', '.join([f"`{col}`" for col in column_names])
    value_placeholders_str = ', '.join(['%s'] * len(column_names))
    sql_template = f"REPLACE INTO {table_name} ({column_names_str}) VALUES ({value_placeholders_str})"

    # Start measuring the time
    start_time = time.time()

    # Track the number of duplicates and successful inserts
    duplicates_count = 0
    successful_inserts_count = 0
    print("Starting import of data")
    # Iterate over the MSSQL data and execute the necessary SQL statements
    for row in mssql_data:
        values = tuple(row[i] for i in range(len(row)))
        try:
            mysql_cursor.execute(sql_template, values)
            successful_inserts_count += 1
            print(f"Rows updated: {successful_inserts_count} of a possible {len(mssql_data)}")
        except mysql.connector.IntegrityError as e:
            duplicates_count += 1

    # Commit the changes and close the cursors and connections
    mysql_connection.commit()
    mysql_cursor.close()
    mysql_connection.close()
    mssql_cursor.close()
    mssql_connection.close()

    # Calculate the elapsed time
    elapsed_time = time.time() - start_time
    print(f"Table updated: {table_name}")
    print(f"Write commenced at: {start_time}, Time taken: {elapsed_time} seconds")
    print(f"Number of duplicates found: {duplicates_count}")
    print(f"Number of rows successfully added/replaced: {successful_inserts_count}")


#importFactv2('redmssqlprod-03.echo.services','Echo2',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Municipal\Queries\FactRoundInstances.sql','fact_roundinstances_municipal','roundinstanceID', 'tim.cattermoul@echobivuk', 'PHFLC7Rf4TRp','echobivuk.mariadb.database.azure.com', 'echoreporting')
#importFactv2('redmssqlprod-03.echo.services', 'Echo2',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Municipal\Queries\FactMissingLiftDiscrepancies.sql','fact_missingliftdiscrepancies_municipal', 'sourceID', 'tim.cattermoul@echobivuk', 'PHFLC7Rf4TRp','echobivuk.mariadb.database.azure.com', 'echoreporting')

#importFactv2('redmssqlprod-03.echo.services', 'Echo2',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Municipal\Queries\FactLifts.sql','fact_lifts_municipal', 'gpseventID', 'tim.cattermoul@echobivuk', 'PHFLC7Rf4TRp','echobivuk.mariadb.database.azure.com', 'echoreporting')
#importFactv2('redmssqlprod-03.echo.services','Echo2',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Municipal\Queries\FactPings.sql','fact_pings_municipal_copy', 'ID', 'tim.cattermoul@echobivuk', 'PHFLC7Rf4TRp','echobivuk.mariadb.database.azure.com', 'echoreporting')
#importFactv2('redmssqlprod-03.echo.services','Echo2',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Municipal\Queries\FactWorkingHours.sql','fact_workinghours_municipal', 'UID', 'tim.cattermoul@echobivuk', 'PHFLC7Rf4TRp','echobivuk.mariadb.database.azure.com', 'echoreporting')
#importFactv2('redmssqlprod-03.echo.services','Echo2',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Municipal\Queries\FactRoundInstances.sql','fact_roundinstances_municipal','UID', 'tim.cattermoul@echobivuk', 'PHFLC7Rf4TRp','echobivuk.mariadb.database.azure.com', 'echoreporting')