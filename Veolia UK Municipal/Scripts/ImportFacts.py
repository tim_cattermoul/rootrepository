from FactImport import importFact
from FactImportV2 import importFactv2
from DimensionImport import importDimension
from SlowLifts import UpdateSlowLifts
import os

try:
    importFactv2('redmssqlprod-03.echo.services', 'Echo2',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Municipal\Queries\FactLifts.sql','fact_lifts_municipal', 'gpseventID', 'tim.cattermoul@echobivuk', 'PHFLC7Rf4TRp','echobivuk.mariadb.database.azure.com', 'echoreporting')
    importFactv2('redmssqlprod-03.echo.services','Echo2',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Municipal\Queries\FactRoundInstances.sql','fact_roundinstances_municipal','UID', 'tim.cattermoul@echobivuk', 'PHFLC7Rf4TRp','echobivuk.mariadb.database.azure.com', 'echoreporting')
    #importFact('redmssqlprod-03.echo.services','Echo2',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Municipal\Queries\FactRoundInstances.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','fact_roundinstances_municipal','append',False,'roundinstanceID')
    importFact('redmssqlprod-03.echo.services','Echo2',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Municipal\Queries\FactLiftMatching.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','fact_liftmatching_municipal','append',False,'gpseventID')
    importFact('redmssqlprod-03.echo.services','Echo2',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Municipal\Queries\FactDelayedLiftsTEMP.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','fact_delayedlifts_municipal','replace',False,'gpseventID')
    #importFact('redmssqlprod-03.echo.services','Echo2',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Municipal\Queries\FactLifts.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','fact_lifts_municipal','append',False,'gpseventID')
    importFactv2('redmssqlprod-03.echo.services', 'Echo2',
                 r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Municipal\Queries\FactPings.sql','fact_pings_municipal', 'ID', 'tim.cattermoul@echobivuk', 'PHFLC7Rf4TRp','echobivuk.mariadb.database.azure.com', 'echoreporting')
    #importFact('redmssqlprod-03.echo.services','Echo2',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Municipal\Queries\FactPings.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','fact_pings_municipal','replace',False,'Day')
    #importFact('redmssqlprod-03.echo.services','Echo2',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Municipal\Queries\FactWorkingHours.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','fact_workinghours_municipal','replace',False,'Day')
    importFactv2('redmssqlprod-03.echo.services', 'Echo2',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Municipal\Queries\FactWorkingHours.sql','fact_workinghours_municipal', 'UID', 'tim.cattermoul@echobivuk', 'PHFLC7Rf4TRp','echobivuk.mariadb.database.azure.com', 'echoreporting')
    importDimension('redmssqlprod-03.echo.services','Echo2',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Municipal\Queries\DimRounds.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','dim_rounds_municipal','replace',False)
    #importFact('redmssqlprod-03.echo.services', 'Echo2',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Municipal\Queries\FactPingGaps.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com', 'fact_pinggaps_municipal', 'replace', False, 'Day')
    importFactv2('redmssqlprod-03.echo.services', 'Echo2',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Municipal\Queries\FactMissingLiftDiscrepancies.sql','fact_missingliftdiscrepancies_municipal', 'sourceID', 'tim.cattermoul@echobivuk', 'PHFLC7Rf4TRp','echobivuk.mariadb.database.azure.com', 'echoreporting')

    import sqlalchemy

    engine = sqlalchemy.create_engine("mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",
                                      connect_args=dict(host='echobivuk.mariadb.database.azure.com', port=3306,
                                                        ssl_ca=r'C:\Users\tim.cattermoul\Documents\MariaDB SQL Certificate (DO NOT DELETE)\BaltimoreCyberTrustRoot.crt.pem'))

    import subprocess, sys

    # print("Importing Onboard Crash Data")
    # p = subprocess.Popen(["powershell.exe",
    #               r'&"C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Municipal\Scripts\OnboardCrashData.ps1"'],
    #               stdout=sys.stdout)
    # p.communicate()

    with engine.begin() as preConn:
        print("Updating Missing and Duplicate Lifts")
        preConn.execute("CALL create_fact_missinglifts_municipal();")
        preConn.execute("CALL create_fact_duplicatelifts_municipal();")
        print("Missing and Duplicate Lifts Updated")
        print("Calculating crashes in shift")
        preConn.execute("CALL update_crashesinshift_municipal();")
        print("Calculation complete")
        print("Updating Device Contracts")
        preConn.execute("CALL update_devicecontracts_municipal();")
        print("Device Contracts Updated")
        UpdateSlowLifts()
        print("Slow Lifts Calculated")
        preConn.execute("CALL Update_slowlifts_municipal();")
        print("Slow Lifts Updated")



except Exception as e:

    from ScriptFailureEmail import sendfailedscriptemail

    sendfailedscriptemail(os.path.basename(__file__), str(e))
