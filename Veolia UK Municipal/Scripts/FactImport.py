import pyodbc
import pandas as pd
import datetime
import subprocess, sys, os
import sqlalchemy
import pymysql
from mysql.connector import (connection)

def importFact(server,database,sql,importpath,host,importtable,ifexists,importindex,key):
    conn = pyodbc.connect('Driver={SQL Server Native Client 11.0};'
                          'Server='+server+';'
                          'Database='+database+';'
                          'Trusted_Connection=yes;')

    cursor = conn.cursor()
    query = open(sql,'r')

    sql_query = pd.read_sql_query(query.read(),conn)

    engine = sqlalchemy.create_engine(importpath,
                           connect_args= dict(host=host, port=3306, ssl_ca=r'C:\Users\tim.cattermoul\Documents\MariaDB SQL Certificate (DO NOT DELETE)\BaltimoreCyberTrustRoot.crt.pem'))

    conn2 = engine.connect()
    temptable = 'temp_'+importtable
    qryDropOldTemp = r'DROP TABLE IF EXISTS '+temptable+';'
    qryRemoveExisting = r'DELETE e.* FROM '+importtable+' e WHERE '+key+' IN (SELECT '+key+' FROM (SELECT DISTINCT F.'+key+' FROM '+importtable+' F INNER JOIN '+temptable+' T ON T.'+key+' = F.'+key+') x);'
    qryAddValues = r'INSERT INTO '+importtable+' SELECT * FROM '+temptable+';'
    qryDropTemp = r'DROP TABLE '+temptable+';'

    with engine.begin() as preConn:
        print("Dropping old temp tables")
        preConn.execute(qryDropOldTemp)
        print("Temp tables dropped")

    sql_query.to_sql(temptable, con=conn2, if_exists=ifexists, index=importindex)


    #
    with engine.begin() as connection:
        print("Removing old values")
        connection.execute(qryRemoveExisting)
        print("Old values removed. Adding New Values")
        connection.execute(qryAddValues)
        print("New values added. Dropping Temp Table")
        connection.execute(qryDropTemp)

    print("Data imported to "+importtable)

