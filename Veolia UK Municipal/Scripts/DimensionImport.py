import pyodbc
import pandas as pd
import datetime
import subprocess, sys, os
import sqlalchemy
import pymysql
from mysql.connector import (connection)

def importDimension(server,database,sql,importpath,host,importtable,ifexists,importindex):
    try:
        conn = pyodbc.connect('Driver={SQL Server Native Client 11.0};'
                              'Server='+server+';'
                              'Database='+database+';'
                              'Trusted_Connection=yes;')

        cursor = conn.cursor()
        query = open(sql,'r')
        sql_query = pd.read_sql_query(query.read(),conn)


        engine = sqlalchemy.create_engine(importpath,
                               connect_args= dict(host=host, port=3306, ssl_ca=r'C:\Users\tim.cattermoul\Documents\MariaDB SQL Certificate (DO NOT DELETE)\BaltimoreCyberTrustRoot.crt.pem'))

        conn2 = engine.connect()

        sql_query.to_sql(importtable, con=conn2, if_exists=ifexists, index=importindex)
        print("Data imported to "+importtable)

    except Exception as e:

        from ScriptFailureEmail import sendfailedscriptemail

        sendfailedscriptemail(os.path.basename(__file__), str(e))


