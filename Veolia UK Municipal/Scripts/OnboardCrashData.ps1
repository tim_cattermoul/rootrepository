﻿# Load Data from SQL Server to MySQL via MySQLBulkLoader class
add-type -Path "C:\Program Files (x86)\MySQL\MySQL Connector Net 8.0.26\Assemblies\v4.5.2\MySql.Data.dll";
#uncomment the following line to calculate the time used to do the loading
$start = get-date; 
 
$csv_file = "C:\Users\tim.cattermoul\Desktop\Extracts\Crash Reporting\Municipal Onboard\Firebase Crash Data.csv"
$sql_db = 'echoreporting'; # note: your db system may be case sensitive
$mysql_instance = 'echobivuk.mariadb.database.azure.com';
$mysql_user = 'tim.cattermoul@echobivuk';
$mysql_pwd = 'PHFLC7Rf4TRp';
 
$conn_str="server=$mysql_instance;user=$mysql_user;password=$mysql_pwd;database=$sql_db;port=3306;AllowLoadLocalInfile=true";
$mysql_conn = new-object MySql.Data.MySqlClient.mysqlconnection($conn_str);
$mysql_conn.Open();

#select statement to TRUNCATE existing data
$sql = New-Object MySql.Data.MySqlClient.MySqlCommand
$sql.Connection = $mysql_conn
$sql.CommandText = 'DELETE FROM fact_onboardcrashes_municipal WHERE LifterDate >= "2022-09-01"'
$sql.ExecuteNonQuery()
Write-Host 'Existing Data Removed'
#insert new data
$bulk = New-Object Mysql.data.MySqlClient.MySqlBulkLoader($mysql_conn);

$bulk.Local=$true; # this is important setting
$bulk.FileName = $csv_file;
$bulk.TableName = 'fact_onboardcrashes_municipal'; #destination table on MySQL side
$bulk.FieldTerminator=',';
$bulk.NumberOfLinesToSkip=1;
$bulk.LineTerminator="\n";
$bulk.FieldQuotationCharacter = '"';
$bulk.EscapeCharacter = '"';
#$bulk.FieldQuotationOptional = $false;
$bulk.Load() | out-null # without out-null, it will show # of rows inserted

#calculate the time used 
write-host "Total time for method with [mysqlbulkloader] is" -ForegroundColor green;
(get-date)-$start


$mysql_conn.Close();
write-host "Firebase Data Imported" 