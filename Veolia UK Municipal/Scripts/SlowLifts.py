import os
import pandas as pd
import sqlalchemy
import numpy as np

def UpdateSlowLifts():
    try:
        importpath = "mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting"

        host = 'echobivuk.mariadb.database.azure.com'

        engine = sqlalchemy.create_engine(importpath,
                                          connect_args=dict(host=host, port=3306,
                                                            ssl_ca=r'C:\Users\tim.cattermoul\Documents\MariaDB SQL Certificate (DO NOT DELETE)\BaltimoreCyberTrustRoot.crt.pem'))

        conn2 = engine.connect()

        query = "SELECT DISTINCT gpseventID, DATE(gpseventdate) as ReceivedDay, resourceID, `Lifter Date` AS Day, gpseventdate, liftdate, timelag, ROW_NUMBER() OVER (PARTITION BY resourceID ORDER BY liftdate) row_num FROM fact_lifts_municipal WHERE DATEDIFF(NOW(),liftdate) BETWEEN 1 AND 90 AND resourceID IS NOT NULL"

        sql_query = pd.read_sql_query(query, conn2)

        # sql_query.set_index('resourceID')

        sql_query['Lower Quartile'] = sql_query.groupby('resourceID')['timelag'].transform(lambda s: s.rolling(5000, min_periods=2).quantile(0.25))
        sql_query['Slow Lift Threshold'] = sql_query['Lower Quartile'] + 120
        sql_query['Slow Lift'] = np.where(sql_query['Slow Lift Threshold'] <= sql_query['timelag'], '1',
                                          np.where(sql_query['ReceivedDay'] > sql_query['Day'],'1',
                                          '0'))

        # new_column = sql_query.groupby(['resourceID','Day'], as_index=False)['timelag'].rolling(1000, min_periods=2).median()

        # new_column = new_column.reset_index(level=0, drop=True)

        #print(sql_query)
        #
        # dir_name = r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Commercial\Extracts'
        # base_filename = 'Commercial Slow Lifts'
        # fileformat = 'csv'
        # fullpath = os.path.join(dir_name,base_filename+"."+fileformat)
        # sql_query.to_csv(fullpath,index=False, header=True)
        # print("Commercial Slow Lifts File produced")

        temptable = 'temp_municipalslowlifts'

        sql_query.to_sql(temptable, con=conn2, if_exists='append', index=False)

        print('Temp Table Created')
    except Exception as e:

        from ScriptFailureEmail import sendfailedscriptemail

        sendfailedscriptemail(os.path.basename(__file__), str(e))

#UpdateSlowLifts()