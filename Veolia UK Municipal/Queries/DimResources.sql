select resourceID, resource, contractID, R.resourcetypeID, RT.resourceclassID, CONVERT(DATETIME,R.startdate) startdate, convert(datetime,R.enddate) enddate, NULL as LiftIntegration, RT.resourcetype

from resources R

INNER JOIN resourcetypes RT on RT.resourcetypeID = R.resourcetypeID

WHERE R.enddate > getdate() and resourceclassID = 2

