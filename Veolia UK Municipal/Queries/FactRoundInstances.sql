select DISTINCT
	RI.roundinstanceID,
	RI.roundID,
	ri.startdate,
	ri.finishdate,
	ss_date,
	sf_date,
	iss_date,
	isf_date,
	dispatch_siteID,
	shiftID,
	roundstateID,
	resolutioncodeID,
	workstartdate,
	workfinishdate,
	contractID,
	isadhoc,
	adhocroundreasonID,
	isoptimised,
	isanalysed,
	lastupdated,
	ds.deviceID,
	RA.resourceID,
	getdate() as Timedataextracted,
	CONCAT(ISNULL(RI.roundinstanceID,0),'-',ISNULL(ds.deviceID,0),'-',ISNULL(RA.resourceID,0)) as UID

from roundinstances RI

LEFT JOIN devicesessions ds on ds.associated_echoID = RI.roundinstanceID and ds.associated_echotypeID = 135
LEFT JOIN (SELECT RA.roundinstanceID, RA.resourceID FROM resourceallocations RA INNER JOIN resourcetypes RT on RT.resourcetypeID = RA.resourcetypeID WHERE RT.resourceclassID =2) as RA on RA.roundinstanceID = RI.roundinstanceID

where datediff(day,lastupdated,getdate()) <= 7 and ss_date < getdate() 
