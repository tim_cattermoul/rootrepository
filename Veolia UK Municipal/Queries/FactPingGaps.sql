IF OBJECT_ID('tempdb..#pings') IS NOT NULL
    DROP TABLE #pings

	select deviceID, receiveddate, positiondate, gpstraildataID, Lag(receiveddate) OVER (PARTITION BY DEVICEID ORDER BY receiveddate) as PreviousReceivedDate

	INTO #pings

	from gpstraildata with (nolock)

	WHERE datediff(day,positiondate,getdate()) = 1



	SELECT *, DATEDIFF(minute,PreviousReceivedDate,receiveddate) as Gap, CONVERT(DATE,receiveddate) as Day FROM #pings

	WHERE DATEDIFF(minute,PreviousReceivedDate,receiveddate) > 45                             

