/****** Script for SelectTopNRows command from SSMS  ******/
WITH CTE_Sessions AS (
SELECT 
		DISTINCT
		deviceID
      ,[startdate]
      ,lastaccessed as lastaccessed -- use last accessed instead following chat with CS
      ,[associated_echoID]
  FROM devicesessions WITH (NOLOCK)
  WHERE datediff(week,startdate,getdate()) <= 1
  
  AND associated_echotypeID = 135 

  )
, CTE_ActionsToDevice AS (

	SELECT DISTINCT CTE_Sessions.associated_echoID as RoundInstanceID, RIA.roundinstanceactionID, RIA.actioncreatedDate, CTE_Sessions.deviceID, CTE_sessions.startdate, CTE_Sessions.lastaccessed

	FROM roundinstanceActions RIA
	LEFT JOIN CTE_Sessions on CTE_Sessions.associated_echoID = RIA.roundinstanceID AND (RIA.actioncreateddate BETWEEN CTE_Sessions.startdate and CTE_Sessions.lastaccessed)
	WHERE datediff(week,actioncreatedDate,getDate()) <= 1

	), 
  
  CTE_Resources as (
	   SELECT 
	   RI.roundinstanceID,
	   RG.roundgroup,
	   RO.round,
	   ss_date,
	   sf_date,
	   C.Contract,
	   R.Resource

	   FROM roundinstances RI
	   INNER JOIN resourceallocations RA on RA.roundinstanceID = RI.roundinstanceID
	   INNER JOIN resources R on R.ResourceID = RA.resourceID
	   INNER JOIN contracts C on C.contractID = R.ContractID
	   INNER JOIN resourcetypes RT on RT.resourcetypeID = R.resourceTypeID
	   INNER JOIN rounds RO on RO.roundid = RI.roundID
	   INNER JOIN roundgroups RG on RG.roundgroupID = RO.roundgroupID
	   WHERE datediff(week,ss_date,getDate()) <= 1 
	   AND RT.resourceclassID = 2
  ),

  CTE_Devices AS (

  SELECT DISTINCT
			D.DeviceID,
			D.model,
			REPLACE(DES.version,'com.twistedfish.OnBoard Release ','') as version
			FROM devices D
			INNER JOIN deviceechosystems DES on DES.deviceID = D.DeviceID and DES.echosystemID = 21 -- Onboard only - exclude Mobile for devices which operate both systems

  )

  ,CTE_RIA as (
  
  SELECT 

	RoundInstanceID
	,A.DeviceID
	,D.version
	,CAST(A.startdate AS DATE) [Day]
	,A.startdate
	,A.lastaccessed as enddate
	
	FROM CTE_ActionsToDevice A
	INNER JOIN CTE_Devices D on D.deviceID = A.deviceID

	--GROUP BY
	--	RoundInstanceID
	--	,A.DeviceID
	--	,D.version
	--	,CAST(actioncreateddate AS DATE)
	)
	, CTE_Summary as (
	SELECT DISTINCT
		R.RoundInstanceID,
		R.DeviceID,
		R.version,
		RS.Contract,
		R.Day,
		0 as [First Action],
		R.startdate as [First Action Date],
		0 as [Last Action],
		R.enddate as [Last Action Date]

	FROM CTE_RIA R
	--INNER JOIN CTE_ActionsToDevice A1 on A1.roundinstanceactionID = R.[First Action]
	--INNER JOIN CTE_ActionsToDevice A2 on A2.roundinstanceactionID = R.[Last Action]
	LEFT JOIN CTE_Resources RS on RS.roundinstanceID = R.roundinstanceID
	)

	, CTE_GPS as (
		select distinct deviceID, echoID, convert(date,positiondate) as devicedate,min(positiondate) as firstpositiondate, max(positiondate) as lastpositiondate
		from gpstraildata with (nolock)
		where 
		datediff(week, positiondate,getdate()) <= 1
		and echotypeID = 54
		and speed <> 0
		GROUP BY deviceID, echoID, convert(date,positiondate)
	), CTE_GPS2 as (
	
	select DISTINCT T.deviceID, d.platform, R.resource, D.model, DES.version, C.contract, T.devicedate, firstpositiondate, lastpositiondate, datediff(hour,firstpositiondate,lastpositiondate) as LoggedInTime
	FROM CTE_GPS T
	INNER JOIN devices D on D.deviceID = T.deviceID
	LEFT JOIN deviceechosystems des ON d.deviceID=[des].deviceID
	LEFT JOIN resources R on r.resourceID = T.echoID
	LEFT JOIN contracts C on C.contractID = R.contractID

	WHERE
	des.echosystemID=21-- and DES.version like '% 2.1%'  AND DES.version NOT LIKE '%Debug%' AND DES.version NOT LIKE '%-p%'
	AND NOT EXISTS ( SELECT DISTINCT O.deviceID from CTE_Summary O WHERE T.deviceid = O.deviceID)
	)



	SELECT CONCAT(roundinstanceID,deviceID,contract,[First Action Date]) as UID,*, Datediff(hour,R.[First Action Date], R.[Last Action Date]) as LoggedInTime, getdate() as TimeDataExtracted from CTE_Summary R

	WHERE RoundInstanceID IS NOT NULL and Datediff(hour,R.[First Action Date], R.[Last Action Date]) >= 0
	
	UNION

	SELECT CONCAT(NULL,deviceID,contract,(FORMAT(firstpositiondate, 'dd/MM/yyyy, hh:mm:ss'))) as UID,NULL, deviceID, REPLACE(version,'com.twistedfish.OnBoard Release ','') as version, contract, CONVERT(Date,firstpositiondate) as Day, NULL, firstpositiondate, NULL, lastpositiondate, LoggedInTime, getdate() from CTE_GPS2 G

	WHERE loggedintime >= 0

	ORDER BY R.RoundInstanceID, R.DeviceID, R.Day