import pandas as pd
import re

def decode_transaction(hex_data):
    hex_data = hex_data.strip()  # Remove leading/trailing spaces
    hex_data = re.sub(r'[^0-9a-fA-F]', '', hex_data)  # Remove non-hexadecimal characters
    hex_data = hex_data.ljust((len(hex_data) + 1) // 2 * 2, '0')  # Pad the hex string if necessary
    return bytearray.fromhex(hex_data).decode('ascii', errors='ignore')

def process_file(file_path):
    transactions = []
    with open(file_path, 'r') as file:
        data = file.read()
        hex_rows = data.split('\n') # split rows based on newline character
        hex_rows = [hex_row for hex_row in hex_rows if hex_row] # remove empty rows
        for hex_row in hex_rows:
            transaction = hex_row#decode_transaction(hex_row)
            transactions.append(transaction)

    df = pd.DataFrame(transactions, columns=['Transaction'])

    # Split the Transaction column into two columns after the first 29 characters
    df['Timestamp'] = df['Transaction'].str[:29]
    df['Data'] = df['Transaction'].str[29:]
    df.drop('Transaction', axis=1, inplace=True)  # Remove the original Transaction column

    return df

# Provide the path to your input file
file_path = r'C:\Users\tim.cattermoul\Desktop\data1.txt'

# Process the file and get the DataFrame
df = process_file(file_path)

# Print the resulting DataFrame
print(df)

# Provide the path for the output CSV file
output_csv_path = r'C:\Users\tim.cattermoul\Desktop\VWSSnifferOutput.csv'

# Export the DataFrame to a CSV file
df.to_csv(output_csv_path, index=False)

print(f"CSV file exported successfully to: {output_csv_path}")