import pandas as pd
from datetime import datetime, timedelta

# Load the dataframe
df = pd.read_csv('path/to/csv')

# Prompt the user for a date
input_date_str = input("Enter a date in YYYY-MM-DD format: ")
input_date = datetime.strptime(input_date_str, "%Y-%m-%d")

# Define a function to check if a login date is within 90 days of the input date
def within_90_days(date):
    return input_date - timedelta(days=90) <= date <= input_date

# Filter the dataframe to only include logins within 90 days of the input date
filtered_df = df[df['login date'].apply(within_90_days)]

# Get the unique deviceIDs and count them
unique_device_ids = filtered_df['deviceID'].unique()
num_unique_device_ids = len(unique_device_ids)

# Print the result
print(f"There are {num_unique_device_ids} unique deviceIDs that logged in within 90 days of {input_date_str}")
