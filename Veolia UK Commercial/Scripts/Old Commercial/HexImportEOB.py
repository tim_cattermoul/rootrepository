import pandas as pd
import binascii
import csv
from functools import reduce
import datetime

# Open in binary mode (so you don't read two byte line endings on Windows as one byte)
# and use with statement (always do this to avoid leaked file descriptors, unflushed files)
with open(r'C:\Users\tim.moul\OneDrive - selectedinterventions.com\Documents\Projects\Commercial\Poole\Log Analysis\EPN 18th August\Sniff Logs\Part 1\EPN Onboard.log','rb') as f:
    # Slurp the whole file and efficiently convert it to hex all at once
    hexdata = f.read().hex()

    #print(hexdata)
    hexdata = hexdata[158:]
    #print(hexdata)

    array = hexdata.split("414d4353")


with open(r'C:\Users\tim.moul\OneDrive - selectedinterventions.com\Documents\Projects\Commercial\Poole\Log Analysis\EPN 18th August\Sniff Logs\Part 1\Onboard.csv','w', newline='') as myfile:
    wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
    for message in array:
        #print("414d4353"+message)
        wr.writerow(["414d4353"+message])

    df = pd.read_csv(r'C:\Users\tim.moul\OneDrive - selectedinterventions.com\Documents\Projects\Commercial\Poole\Log Analysis\EPN 18th August\Sniff Logs\Part 1\Onboard.csv')

    df['Raw Data'] = df.iloc[:, 0]#df['414d4353']
    #print("Foo")

    pairs = []
    for index, row in df.iterrows():
        hexstring = row['Raw Data']
        # print(row['414d4353'], row['Copy'])
        # print([hexstring[i:i+2] for i in range(0,len(hexstring), 2)])
        pairs.append([hexstring[i:i + 2] for i in range(0, len(hexstring), 2)])

    df['Pairs'] = pairs

    file_name = r'C:\Users\tim.moul\OneDrive - selectedinterventions.com\Documents\Projects\Commercial\Poole\Log Analysis\EPN 18th August\Sniff Logs\Part 1\OnboardConsolidated.csv'

    lengths = []
    for value in df['Raw Data']:
        if value == '414d435301011c':
            lengths.append("EOB Lift Record Request Message")
        elif len(value) == 16:
            lengths.append("EOB Lift Record Acknowledgement Message (Probable)")
        elif len(value) == 18:
            lengths.append("EOB Lift Record Acknowledgement Message")
        elif len(value) == 26:
            lengths.append("EOB Set Time Message")
        else:
            lengths.append("EOB Unknown")

    df['Message Type'] = lengths

    CRC = []
    for value, row in df.iterrows():
        hexstring = row['Raw Data']
        if len(hexstring) == 14:
            CRC.append(hexstring[-2:])
        elif len(hexstring) == 18:
            CRC.append(hexstring[-2:])
        elif len(hexstring) == 26:
            CRC.append(hexstring[-2:])
        else:
            CRC.append("Unknown")

    df['CRC'] = CRC

    df['freq'] = df.groupby('Raw Data')['Raw Data'].transform('count')

    seq = []
    for value, row in df.iterrows():
        hexstring = row['Raw Data']
        if len(hexstring) == 18:
            seq.append(hexstring[12:16])
        elif len(hexstring) == 16:
            seq.append(hexstring[12:16])
        else:
            seq.append("")

    df['SeqHex'] = seq

    seqHex = []

    for value, row in df.iterrows():
        hexstring = row['SeqHex']
        if len(hexstring) > 0:
            seqHex.append(reduce(lambda x, y: x + (int(y[1], 16) << (8 * y[0] // 2)),
                                 ((i, hexstring[i:i + 2]) for i in range(0, len(hexstring), 2)), 0))
        else:
            seqHex.append("")

    df['Sequence Number'] = seqHex

    day = []
    for value, row in df.iterrows():
        hexstring = row['Raw Data']
        if len(hexstring) == 26:
            day.append(int(hexstring[12:14],16))
        else:
            day.append("")

    df['Day'] = day

    month = []
    for value, row in df.iterrows():
        hexstring = row['Raw Data']
        if len(hexstring) == 26:
            month.append(int(hexstring[14:16], 16))
        else:
            month.append("")

    df['Month'] = month

    year = []
    for value, row in df.iterrows():
        hexstring = row['Raw Data']
        if len(hexstring) == 26:
            year.append(int(hexstring[16:18], 16) + 2000)
        else:
            year.append("")

    df['Year'] = year

    hour = []
    for value, row in df.iterrows():
        hexstring = row['Raw Data']
        if len(hexstring) == 26:
            hour.append(int(hexstring[18:20], 16))
        else:
            hour.append("")

    df['Hour'] = hour

    min = []
    for value, row in df.iterrows():
        hexstring = row['Raw Data']
        if len(hexstring) == 26:
            min.append(int(hexstring[20:22], 16))
        else:
            min.append("")

    df['Minute'] = min

    sec = []
    for value, row in df.iterrows():
        hexstring = row['Raw Data']
        if len(hexstring) == 26:
            sec.append(int(hexstring[22:24], 16))
        else:
            sec.append("")

    df['Seconds'] = sec

    dayCheck = []
    for value, row in df.iterrows():
        hexstring = row['Day']
        if hexstring != "":
            if int(hexstring) > 31:
                dayCheck.append("Invalid Date")
            elif int(hexstring) < 1:
                dayCheck.append("Invalid Date")
            else:
                dayCheck.append("No Error")
        else:
            dayCheck.append("")

    df['Day Check'] = dayCheck

    monthCheck = []
    for value, row in df.iterrows():
        hexstring = row['Month']
        if hexstring != "":
            if int(hexstring) > 12:
                monthCheck.append("Invalid Date")
            elif int(hexstring) < 1:
                monthCheck.append("Invalid Date")
            else:
                monthCheck.append("No Error")
        else:
            monthCheck.append("")

    df['Month Check'] = monthCheck

    yearCheck = []
    for value, row in df.iterrows():
        hexstring = row['Year']
        if hexstring != "":
            if int(hexstring) > datetime.datetime.now().year:
                yearCheck.append("Invalid Date")
            elif int(hexstring) < int(2021):
                yearCheck.append("Invalid Date")
            else:
                yearCheck.append("No Error")
        else:
            yearCheck.append("")

    df['Year Check'] = yearCheck

    hourCheck = []
    for value, row in df.iterrows():
        hexstring = row['Hour']
        if hexstring != "":
            if int(hexstring) > 23:
                hourCheck.append("Invalid Date")
            elif int(hexstring) < 0:
                hourCheck.append("Invalid Date")
            else:
                hourCheck.append("No Error")
        else:
            hourCheck.append("")

    df['Hour Check'] = hourCheck

    minCheck = []
    for value, row in df.iterrows():
        hexstring = row['Minute']
        if hexstring != "":
            if int(hexstring) > 59:
                minCheck.append("Invalid Date")
            elif int(hexstring) < 0:
                minCheck.append("Invalid Date")
            else:
                minCheck.append("No Error")
        else:
            minCheck.append("")

    df['Minute Check'] = minCheck

    secCheck = []
    for value, row in df.iterrows():
        hexstring = row['Seconds']
        if hexstring != "":
            if int(hexstring) > 59:
                secCheck.append("Invalid Date")
            elif int(hexstring) < 0:
                secCheck.append("Invalid Date")
            else:
                secCheck.append("No Error")
        else:
            secCheck.append("")

    df['Second Check'] = secCheck

    cols = [0,6]
    df = df.drop(df.columns[cols], axis=1)
    print(df)
    df.to_csv(file_name)
