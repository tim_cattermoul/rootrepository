from FactImport import importFact
from DimensionImport import importDimension
from SlowLiftsCommercial import UpdateSlowLifts
import os

try:
    importFact('redmssqlprod-03.echo.services','Echo2_Commercial',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Commercial\Queries\FactRoundInstances.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','fact_roundinstances_commercial','replace',False,'roundinstanceID')
    importFact('redmssqlprod-03.echo.services','Echo2_Commercial',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Commercial\Queries\FactLiftMatching.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','fact_liftmatching_commercial','replace',False,'gpseventID')
    importFact('redmssqlprod-03.echo.services','Echo2_Commercial',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Commercial\Queries\FactLifts.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','fact_lifts_commercial','replace',False,'gpseventID')
    importFact('redmssqlprod-03.echo.services','Echo2_Commercial',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Commercial\Queries\FactDelayedLiftsTEMP.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','fact_delayedlifts_commercial','replace',False,'gpseventID')
    importFact('redmssqlprod-03.echo.services','Echo2_Commercial',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Commercial\Queries\FactPings.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','fact_pings_commercial','replace',False,'Day')
    importFact('redmssqlprod-03.echo.services','Echo2_Commercial',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Commercial\Queries\FactWorkingHours.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','fact_workinghours_commercial','replace',False,'Day')
    importFact('redmssqlprod-03.echo.services', 'Echo2_Commercial',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Municipal\Queries\FactMissingLiftDiscrepancies.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting", 'echobivuk.mariadb.database.azure.com', 'fact_missingliftdiscrepancies_commercial', 'replace', False, 'sourceID')
    importFact('VComUKSQLDev1.echo.services,10001', 'VCommUK_Echo2_APITesting2',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Commercial\Queries\FactLiftsUAT.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com', 'fact_uatlifts_commercial', 'replace', False, 'gpseventID')
    importFact('VComUKSQLDev1.echo.services,10001', 'VCommUK_Echo2_Dev2New',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Commercial\Queries\FactLiftsUAT.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com', 'fact_uatlifts_commercial', 'replace', False, 'gpseventID')
    importDimension('redmssqlprod-03.echo.services','Echo2_Commercial',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Commercial\Queries\DimRounds.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','dim_rounds_commercial','replace',False)

    import sqlalchemy

    engine = sqlalchemy.create_engine("mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",
                                      connect_args=dict(host='echobivuk.mariadb.database.azure.com', port=3306,
                                                        ssl_ca=r'C:\Users\tim.cattermoul\Documents\MariaDB SQL Certificate (DO NOT DELETE)\BaltimoreCyberTrustRoot.crt.pem'))

    import subprocess, sys
    print("Importing Onboard Crash Data")
    p = subprocess.Popen(["powershell.exe",
                  r'&"C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Commercial\Scripts\OnboardCrashData.ps1"'],
                  stdout=sys.stdout)
    p.communicate()

    with engine.begin() as preConn:
        print("Updating Missing Lifts")
        preConn.execute("CALL create_fact_missinglifts_commercial();")
        preConn.execute("CALL update_missinglifts_commercial();")
        print("Updating Duplicate Lifts")
        preConn.execute("CALL create_fact_duplicatelifts_commercial();")
        print("Missing Lifts Updated")
        print("Calculating crashes in shift")
        preConn.execute("CALL update_crashesinshift_commercial();")
        print("Calculation complete")
        print("Updating Device Contracts")
        preConn.execute("CALL update_devicecontracts_commercial();")
        print("Device Contracts Updated")
        UpdateSlowLifts()
        print("Slow Lifts Calculated")
        preConn.execute("CALL Update_slowlifts_commercial();")
        print("Slow Lifts Updated")


except Exception as e:

    from ScriptFailureEmail import sendfailedscriptemail

    sendfailedscriptemail(os.path.basename(__file__), str(e))




