import pandas as pd

amcsR = pd.read_csv(r'C:\Users\tim.moul\OneDrive - selectedinterventions.com\Documents\Projects\Commercial\Poole\Log Analysis\EPN 18th August\Sniff Logs\Part 1\AMCSConsolidated.csv')
eob = pd.read_csv(r'C:\Users\tim.moul\OneDrive - selectedinterventions.com\Documents\Projects\Commercial\Poole\Log Analysis\EPN 18th August\Sniff Logs\Part 1\OnboardConsolidated.csv')
sql = pd.read_csv(r'C:\Users\tim.moul\OneDrive - selectedinterventions.com\Documents\Projects\Commercial\Poole\Log Analysis\EPN 18th August\Sniff Logs\GPSEvents.csv')

file_name = r'C:\Users\tim.moul\OneDrive - selectedinterventions.com\Documents\Projects\Commercial\Poole\Log Analysis\EPN 18th August\Sniff Logs\Part 1\CombinedHexSummary.csv'
AMCSfile_name = r'C:\Users\tim.moul\OneDrive - selectedinterventions.com\Documents\Projects\Commercial\Poole\Log Analysis\EPN 18th August\Sniff Logs\Part 1\HexLiftComparisonSummaryAMCS.csv'
EOBfile_name = r'C:\Users\tim.moul\OneDrive - selectedinterventions.com\Documents\Projects\Commercial\Poole\Log Analysis\EPN 18th August\Sniff Logs\Part 1\HexLiftComparisonSummaryEOB.csv'
FMfile_name = r'C:\Users\tim.moul\OneDrive - selectedinterventions.com\Documents\Projects\Commercial\Poole\Log Analysis\EPN 18th August\Sniff Logs\Part 1\FullMessageComparison.csv'

amcs = amcsR.drop(columns=['Raw Data','Pairs','CRC','freq','Day','Month','Year','Gross Weight','Tare Weight','Lifter Number','Net Weight','Hour','Minute','Seconds','Day Check','Month Check','Year Check','Hour Check','Minute Check','Second Check'])
eob = eob.drop(columns=['Raw Data','Pairs','CRC','freq','Day','Month','Year','Hour','Minute','Seconds','Day Check','Month Check','Year Check','Hour Check','Minute Check','Second Check'])

eob['Source'] = 'Onboard'
amcs['Source'] = 'AMCS'

#Stack the two files and take a count of the number of occurrences of each message type

combined = [amcs,eob]
result = pd.concat(combined)
messagetypes = result['Message Type'].value_counts().rename_axis('unique_values').to_frame('counts')
messagetypes.to_csv(file_name)

#Compare the two extracts to ensure that each lift appears in both
joined = amcs
joined2 = eob
joined = joined.dropna(subset=['Sequence Number'])
joined2 = joined2.dropna(subset=['Sequence Number'])
amcs2 = amcs.dropna(subset=['Sequence Number'])
eob2 = eob.dropna(subset=['Sequence Number'])
joined['Lift count in EOB'] = joined['Sequence Number'].map(eob2['Sequence Number'].value_counts()).fillna(0).astype(int)

joined.to_csv(AMCSfile_name)
joined2['Lift count in AMCS'] = joined2['Sequence Number'].map(amcs2['Sequence Number'].value_counts()).fillna(0).astype(int)

joined2.to_csv(EOBfile_name)

#Compare Sniffed AMCS Full Message Strings to those logged against GPSEvents

amcsFullMessage = amcsR.drop(columns=['Pairs','CRC','freq','Day','Month','Year','Gross Weight','Tare Weight','Lifter Number','Net Weight','Hour','Minute','Seconds','Day Check','Month Check','Year Check','Hour Check','Minute Check','Second Check'])
amcsFullMessage = amcsFullMessage.dropna(subset=['Sequence Number'])
amcsFullMessage = amcsFullMessage.iloc[: , 1:]
amcsFullMessage['Raw Data'] = amcsFullMessage['Raw Data'].str.upper()
#Remove the parity bit from the end of the AMCS string
fm = []
for value, row in amcsFullMessage.iterrows():
    hexstring = row['Raw Data']
    if len(hexstring) == 60:
        fm.append(hexstring[:len(hexstring) - 2])
    else:
        fm.append(hexstring)

amcsFullMessage['Raw Data'] = fm

#Merge the two dataframes
mergedFM = pd.merge(amcsFullMessage,sql, on="Sequence Number")

match = []
for value, row in mergedFM.iterrows():
    hexstringA = row['Raw Data']
    hexstringG = row['Full Message']
    if hexstringA == hexstringG:
        match.append('Lifts Match')
    else:
        match.append('Lifts DO NOT Match')

mergedFM['Lift Match'] = match


mergedFM.to_csv(FMfile_name)