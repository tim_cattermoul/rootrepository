import pandas as pd
import binascii
import csv
from functools import reduce
import datetime

# Open in binary mode (so you don't read two byte line endings on Windows as one byte)
# and use with statement (always do this to avoid leaked file descriptors, unflushed files)
with open(r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Commercial\Extracts\AMCS RFID Sniffing\AMCS.log','rb') as f:
    # Slurp the whole file and efficiently convert it to hex all at once
    hexdata = f.read().hex()

    print(hexdata)
    hexdata = hexdata[158:]
    print(hexdata)

    array = hexdata.split("414d4353")

with open(r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Commercial\Extracts\AMCS RFID Sniffing\AMCS.csv','w', newline='') as myfile:
    wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
    for message in array:
        #print("414d4353"+message)
        wr.writerow(["414d4353"+message])

df = pd.read_csv(r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Commercial\Extracts\AMCS RFID Sniffing\AMCS.csv')

df['Raw Data'] = df['414d4353']

#print(df)
pairs = []
for index, row in df.iterrows():
    hexstring = row['Raw Data']
    #print(row['414d4353'], row['Copy'])
    #print([hexstring[i:i+2] for i in range(0,len(hexstring), 2)])
    pairs.append([hexstring[i:i+2] for i in range(0,len(hexstring), 2)])

df['Pairs'] = pairs

lengths = []
for value in df['Raw Data']:
    if value == '414d4353010518':
        lengths.append("AMCS Set Time Acknowledgement Message")
    elif value == '414d435301061b':
        lengths.append("AMCS Empty Lift Record Message")
    elif len(value) == 58:
        lengths.append("AMCS Lift Record Message (Probable)")
    elif len(value) == 60:
        lengths.append("AMCS Lift Record Message")
    elif len(value) == 62:
        lengths.append("AMCS Lift Record Message")
    else:
        lengths.append("AMCS Unknown")

df['Message Type'] = lengths


CRC = []
for value, row in df.iterrows():
    hexstring = row['Raw Data']
    if len(hexstring) == 14:
        CRC.append(hexstring[-2:])
    elif len(hexstring) == 60:
        CRC.append(hexstring[-2:])
    elif len(hexstring) == 62:
        CRC.append(hexstring[-2:])
    else:
        CRC.append("Unknown")

df['CRC'] = CRC

df['freq'] = df.groupby('Raw Data')['Raw Data'].transform('count')

seq = []
for value, row in df.iterrows():
    hexstring = row['Raw Data']
    if len(hexstring) >= 58:
        seq.append(hexstring[14:18])
    else:
        seq.append("")

df['SeqHex'] = seq

seqHex = []

for value, row in df.iterrows():
    hexstring = row['SeqHex']
    if len(hexstring) > 0:
        seqHex.append(reduce(lambda x, y: x  + (int(y[1], 16)<<(8 * y[0]//2) ), ((i, hexstring[i:i+2]) for i in range(0, len(hexstring), 2)) , 0))
    else:
        seqHex.append("")

df['Sequence Number'] = seqHex

lifter = []
for value, row in df.iterrows():
    hexstring = row['Raw Data']
    if len(hexstring) >= 58:
        lifter.append(hexstring[12:14])
    else:
        lifter.append("")

df['Lifter Number'] = lifter

gw = []
for value, row in df.iterrows():
    hexstring = row['Raw Data']
    if len(hexstring) >= 58:
        gw.append(hexstring[18:22])
    else:
        gw.append("")

df['gw'] = gw

GrossWt = []

for value, row in df.iterrows():
    hexstring = row['gw']
    if len(hexstring) > 0:
        GrossWt.append(reduce(lambda x, y: x  + (int(y[1], 16)<<(8 * y[0]//2) ), ((i, hexstring[i:i+2]) for i in range(0, len(hexstring), 2)) , 0))
    else:
        GrossWt.append("")

df['Gross Weight'] = GrossWt

tw = []
for value, row in df.iterrows():
    hexstring = row['Raw Data']
    if len(hexstring) >= 58:
        tw.append(hexstring[22:26])
    else:
        tw.append("")

df['tw'] = tw

TareWt = []

for value, row in df.iterrows():
    hexstring = row['tw']
    if len(hexstring) > 0:
        TareWt.append(reduce(lambda x, y: x  + (int(y[1], 16)<<(8 * y[0]//2) ), ((i, hexstring[i:i+2]) for i in range(0, len(hexstring), 2)) , 0))
    else:
        TareWt.append("")

df['Tare Weight'] = TareWt

nw = []
for value, row in df.iterrows():
    hexstring = row['Raw Data']
    if len(hexstring) >= 58:
        nw.append(hexstring[26:30])
    else:
        nw.append("")

df['nw'] = nw

NetWt = []

for value, row in df.iterrows():
    hexstring = row['nw']
    if len(hexstring) > 0:
        NetWt.append(reduce(lambda x, y: x  + (int(y[1], 16)<<(8 * y[0]//2) ), ((i, hexstring[i:i+2]) for i in range(0, len(hexstring), 2)) , 0))
    else:
        NetWt.append("")

df['Net Weight'] = NetWt

rfid = []
for value, row in df.iterrows():
    hexstring = row['Raw Data']
    if len(hexstring) >= 58:
        rfid.append(hexstring[30:46])
    else:
        rfid.append("")

df['rfid'] = rfid

day = []
for value, row in df.iterrows():
    hexstring = row['Raw Data']
    if len(hexstring) >= 58:
        day.append(hexstring[46:48])
    else:
        day.append("")

df['Day'] = day

month = []
for value, row in df.iterrows():
    hexstring = row['Raw Data']
    if len(hexstring) >= 58:
        month.append(int(hexstring[48:50],16))
    else:
        month.append("")

df['Month'] = month

year = []
for value, row in df.iterrows():
    hexstring = row['Raw Data']
    if len(hexstring) >= 58:
        year.append(int(hexstring[50:52],16)+2000)
    else:
        year.append("")

df['Year'] = year

hour = []
for value, row in df.iterrows():
    hexstring = row['Raw Data']
    if len(hexstring) >= 58:
        hour.append(int(hexstring[52:54],16))
    else:
        hour.append("")

df['Hour'] = hour

min = []
for value, row in df.iterrows():
    hexstring = row['Raw Data']
    if len(hexstring) >= 58:
        min.append(int(hexstring[54:56],16))
    else:
        min.append("")

df['Minute'] = min

sec = []
for value, row in df.iterrows():
    hexstring = row['Raw Data']
    if len(hexstring) >= 58:
        sec.append(int(hexstring[56:58],16))
    else:
        sec.append("")

df['Seconds'] = sec

dayCheck = []
for value, row in df.iterrows():
    hexstring = row['Day']
    #if len(hexstring) > 0:
       # if int(hexstring) > 31:
            #dayCheck.append("Invalid Date")
      ## elif int(hexstring) < 1:
            #dayCheck.append("Invalid Date")
       # else:
            #dayCheck.append("No Error")
    #else:
        #dayCheck.append("")

#df['Day Check'] = dayCheck

monthCheck = []
for value, row in df.iterrows():
    hexstring = row['Month']
    if hexstring != "":
        if int(hexstring) > 12:
            monthCheck.append("Invalid Date")
        elif int(hexstring) < 1:
            monthCheck.append("Invalid Date")
        else:
            monthCheck.append("No Error")
    else:
        monthCheck.append("")

df['Month Check'] = monthCheck

yearCheck = []
for value, row in df.iterrows():
    hexstring = row['Year']
    if hexstring != "":
        if int(hexstring) > datetime.datetime.now().year:
            yearCheck.append("Invalid Date")
        elif int(hexstring) < int(2021):
            yearCheck.append("Invalid Date")
        else:
            yearCheck.append("No Error")
    else:
        yearCheck.append("")

df['Year Check'] = yearCheck

hourCheck = []
for value, row in df.iterrows():
    hexstring = row['Hour']
    if hexstring != "":
        if int(hexstring) > 23:
            hourCheck.append("Invalid Date")
        elif int(hexstring) < 0:
            hourCheck.append("Invalid Date")
        else:
            hourCheck.append("No Error")
    else:
        hourCheck.append("")

df['Hour Check'] = hourCheck

minCheck = []
for value, row in df.iterrows():
    hexstring = row['Minute']
    if hexstring != "":
        if int(hexstring) > 59:
            minCheck.append("Invalid Date")
        elif int(hexstring) < 0:
            minCheck.append("Invalid Date")
        else:
            minCheck.append("No Error")
    else:
        minCheck.append("")

df['Minute Check'] = minCheck

secCheck = []
for value, row in df.iterrows():
    hexstring = row['Seconds']
    if hexstring != "":
        if int(hexstring) > 59:
            secCheck.append("Invalid Date")
        elif int(hexstring) < 0:
            secCheck.append("Invalid Date")
        else:
            secCheck.append("No Error")
    else:
        secCheck.append("")

df['Second Check'] = secCheck

df = df.drop(columns=['414d4353','SeqHex','gw','tw','nw'])

file_name = r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Commercial\Extracts\AMCS RFID Sniffing\AMCSConsolidated.csv'

print(df)
df.to_csv(file_name)
