from DimensionImport import importDimension
from FactImport import importFact

importDimension('VComUKSQLProd2.echo.services,10001','VCommUKReplica',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Commercial\Queries\DimResources.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','dim_resources_commercial','replace',False)
importDimension('VComUKSQLProd2.echo.services,10001','VCommUKReplica',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Commercial\Queries\DimContracts.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','dim_contracts_commercial','replace',False)
importDimension('VComUKSQLProd2.echo.services,10001','VCommUKReplica',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Commercial\Queries\DimDevices.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','dim_devices_commercial','replace',False)
importDimension('VComUKSQLProd2.echo.services,10001','VCommUKReplica',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Commercial\Queries\DimRounds.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','dim_rounds_commercial','replace',False)
importDimension('VComUKSQLProd2.echo.services,10001','VCommUKReplica',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Commercial\Queries\DimDeviceEchoSystem.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','dim_deviceechosystems_commercial','append',False)
#importDimension('VComUKSQLProd2.echo.services,10001','VCommUKReplica',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Commercial\Queries\FactRoundInstances.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','fact_roundinstances_commercial','replace',False)
importFact('VComUKSQLProd2.echo.services,10001','VCommUKReplica',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Commercial\Queries\FactDistanceTravelled.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com', 'fact_distancetravelled_commercial', 'replace', False, 'Day')
importFact('VComUKSQLProd2.echo.services,10001','VCommUKReplica',r'C:\Users\tim.cattermoul\Desktop\Queries\SLA Reporting\Veolia Municipal\ECHO Object Transactions per 60 minute block TC v0.5 20230207 MUN.sql',"mysql+pymysql://onboard@simariadb:afRQcMuD6j@simariadb.mariadb.database.azure.com/slareporting",'simariadb.mariadb.database.azure.com', 'sla_sp003_raw_commercial', 'replace', False, 'Block')
importDimension('VComUKSQLProd2.echo.services,10001','VCommUKReplica',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia UK Commercial\Queries\DimDevices.sql',"mysql+pymysql://onboard@simariadb:afRQcMuD6j@simariadb.mariadb.database.azure.com/slareporting",'simariadb.mariadb.database.azure.com', 'dim_devices_commercial', 'replace', False)

import sqlalchemy

engine = sqlalchemy.create_engine("mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",
                                  connect_args=dict(host='echobivuk.mariadb.database.azure.com', port=3306,
                                                    ssl_ca=r'C:\Users\tim.cattermoul\Documents\MariaDB SQL Certificate (DO NOT DELETE)\BaltimoreCyberTrustRoot.crt.pem'))

with engine.begin() as preConn:
    print("Calculating Emissions")
    preConn.execute("CALL update_distancetravelledemissions_commercial();")
    preConn.execute("CALL create_fact_ghgemissions_commercial();")
    print("Emissions Calculated")