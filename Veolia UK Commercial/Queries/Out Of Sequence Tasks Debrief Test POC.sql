-- Declare a variable to store the roundinstanceID
declare @roundinstanceID int 
set @roundinstanceID = 271006; -- Replace with appropriate value for debrief test

select *, CASE WHEN statuslat IS NULL THEN 0 ELSE 1 END as TaskCompletedOnRound from 

(

	select *, convert(time,dateadd(second,T.ActTimeDiffS, '00:00:00')) as ActTimeDiffSec, convert(time,dateadd(second,T.OptTimeDiffS, '00:00:00')) as OptTimeDiffSec, CASE WHEN T.DifferenceInSequenceNo = 1 THEN 1 ELSE 0 END as InSequence
	--into #temp3
	from (select T.*,
	
	CASE WHEN T.TaskEndDay = T.ScheduledTaskEndDay THEN 1 ELSE 0 END AS TaskCollectedOnScheduledDay, 
	CASE WHEN T.actSequence = 1 THEN T.OptSequenceNorm ELSE LAG(T.OptSequenceNorm) over (partition by roundinstanceID ORDER BY T.ActSequence) END as PreviousOptSeq, (T.OptSequenceNorm-CASE WHEN T.actSequence = 1 THEN 0 ELSE LAG(T.OptSequenceNorm) over (partition by roundinstanceID ORDER BY T.ActSequence) END) as DifferenceInSequenceNo

from (

select
	rg.roundgroupID,
	rg.roundgroup,
	ri.roundinstanceID,
	convert(date,ri.iss_date) as Day,
	t.taskid as TaskID,
	t.servicetaskID as ServiceTaskID,
	cast(task as varchar) as Task,
	ts.taskstate as TaskState,
	row_number () over (partition by ri.roundinstanceID order by t.taskenddate ) as ActSequence,
	cast(taskenddate as time) as TaskEnd,
	cast(taskenddate as date) as TaskEndDay,
	cast(servicetaskscheduleddate as date) as ScheduledTaskEndDay,
	datediff(second, lag(taskenddate, 1) over (partition by ri.roundinstanceID order by t.taskenddate), taskenddate) as ActTimeDiffS,
	t.sequence as OptSequence,
	row_number () over (partition by ri.roundinstanceID order by t.sequence) as OptSequenceNorm,
	rip.roundinstanceplanID,
	tp.taskplanID,
	tp.sequence,
	cast(tp.timeofservice as date) as OptDateOfService,
	cast(tp.timeofservice as time) as OptTimeOfService,
	datediff(second, '00:00:00', tp.transittimefromlasttask) as OptTimeDiffS,
	t.scheduledroundinstanceID,
	tp.islastinroundsection TipAfter, -- Is this the same?
	tp.estimatedweight as EstWeight,
	tp.cumulativeweight as EstCumulativeWeight,
	t.statuslat,
	t.statuslon
--into #temp
from tasks t
inner join taskstates ts on t.taskstateid = ts.taskstateid
inner join roundinstances ri on ri.roundinstanceID = t.roundinstanceID
inner join rounds r on r.roundid = t.roundid
inner join roundgroups rg on rg.roundgroupid = r.roundgroupid
INNER join roundinstanceplans rip on rip.roundinstanceID = ri.roundinstanceID
INNER join taskplans tp on tp.taskID = t.taskID and rip.roundinstanceplanID = tp.roundinstanceplanID
where
	ri.roundinstanceID = @roundinstanceID
	and ri.isoptimised = 1
--order by rg.roundgroup, t.sequence

) T) T

) as A


where InSequence = 0

order by roundgroup, sequence