WITH CTE_Pings as (
	select gpstraildataID, echoID, deviceID, positiondate, positionquality, lat,long, speed 
	,lag(lat) OVER (PARTITION BY deviceID, echoID ORDER BY positiondate) as PrevLat
	,lag(long) OVER (PARTITION BY deviceID, echoID ORDER BY positiondate) as PrevLong

	from gpstraildata with (nolock)

	where datediff(day,positiondate,getdate()) = 1 and positionquality < 50 and echotypeID = 54

	--ORDER BY deviceID, positiondate
)
,CTE_DistanceTravelled as (
SELECT *, CASE WHEN prevlat IS NULL THEN NULL ELSE GEOGRAPHY::Point(lat, long, 4326).STDistance(GEOGRAPHY::Point(prevlat, prevlong, 4326)) END as DistanceTravelled
FROM CTE_Pings
)

SELECT 
	deviceID, 
	convert(date,positiondate) as Day,
	echoID,
	SUM(DistanceTravelled)/1000 as kmTravelled, 
	--(SUM(DistanceTravelled)/1000)*0.95752 
	NULL as KgCO2e

FROM CTE_DistanceTravelled

GROUP BY deviceID, convert(Date,positiondate), echoID

ORDER BY 1,2
