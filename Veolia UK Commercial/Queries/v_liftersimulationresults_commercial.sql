SELECT DISTINCT
	R.`Sequence Number` AS SequenceNumberRaw,
	L.`Sequence Number` AS SequenceNumberActual,
	CASE WHEN R.`Sequence Number` = L.`Sequence Number` THEN TRUE ELSE FALSE END AS SequenceNumberMatch,
	R.`<lift>` AS BinLifterRaw,
	L.`Bin Lifter` AS BinLifterActual,
	CASE WHEN LEFT(L.`Bin Lifter`,1) = R.`<lift>` THEN TRUE ELSE FALSE END AS BinLifterMatch,
	R.`RFID Number` AS RFIDNumberRaw,
	CASE WHEN L.`RFID Number` = '' THEN '0000000000' ELSE L.`RFID Number` END AS RFIDNumberActual,
	CASE WHEN R.`RFID Number` = CASE WHEN L.`RFID Number` = '' THEN '0000000000' WHEN R.`RFID Number` LIKE '%000000000' THEN R.`RFID Number` ELSE L.`RFID Number` END THEN TRUE ELSE FALSE END AS RFIDMatch,
	R.`Net Weight` AS NetWeightRaw,
	L.`Net Weight` AS NetWeightActual,
	CASE WHEN R.`Net Weight` = L.`Net Weight` THEN TRUE ELSE FALSE END AS NetWeightMatch,
	R.`Gross Weight` AS GrossWeightRaw,
	L.`Gross Weight` AS GrossWeightActual,
	CASE WHEN R.`Gross Weight` = L.`Gross Weight` THEN TRUE ELSE FALSE END AS GrossWeightMatch,
	R.`Tare Weight` AS TareWeightRaw,
	L.`Tare Weight` AS TareWeightActual,
	CASE WHEN R.`Tare Weight` = L.`Tare Weight` THEN TRUE ELSE FALSE END AS TareWeightMatch,
	R.`Lifter Date` AS LifterDateRaw,
	L.`Lifter Date` AS LifterDateActual,
	CASE WHEN R.`Lifter Date` = L.`Lifter Date` THEN TRUE ELSE FALSE END AS LifterDateMatch,
	R.`Lifter Time` AS LifterTimeRaw,
	L.`Lifter Time` AS LifterTimeActual,
	CASE WHEN R.`Lifter Time` = L.`Lifter Time` THEN TRUE ELSE FALSE END AS LifterTimeMatch,
	CASE WHEN R.`Error Code` = '' THEN NULL ELSE R.`Error Code` END AS ErrorCodeRaw,
	SUBSTRING_INDEX( L.`Error Code`, ' - ', 1 ) AS ErrorCodeActual,
	CASE WHEN CASE WHEN R.`Error Code` = '' THEN NULL ELSE R.`Error Code` END = SUBSTRING_INDEX( L.`Error Code`, ' - ', 1 ) THEN TRUE ELSE FALSE END AS ErrorCodeMatch,
	NULL AS WeighingCodeRaw,
	NULL AS WeighingCodeActual,
	NULL AS WeighingCodeMatch,
	L.gpseventID,
	L.gpseventdate,
	L.resourceID,
	L.deviceID,
	L.version,
	L.weighingsystem

FROM fact_uatlifts_commercial L 

INNER JOIN dim_vwssimlifts_commercial R ON R.`Sequence Number` = L.`Sequence Number`

WHERE L.weighingsystem = 'VWS'

UNION ALL

SELECT DISTINCT

	R.`Sequence number of record` AS SequenceNumberRaw,
	L.`Sequence Number` AS SequenceNumberActual,
	CASE WHEN R.`Sequence Number of record` = L.`Sequence Number` THEN TRUE ELSE FALSE END AS SequenceNumberMatch,
	R.`Lifter Number` AS BinLifterRaw,
	L.`Bin Lifter` AS BinLifterActual,
	CASE WHEN RIGHT(L.`Bin Lifter`,1) = R.`Lifter Number` THEN TRUE ELSE FALSE END AS BinLifterMatch,
	R.`Tag Number MSB` AS RFIDNumberRaw,
	CASE WHEN L.`RFID Number` = '' THEN '0000000000' ELSE L.`RFID Number` END AS RFIDNumberActual,
	CASE WHEN R.`Tag Number MSB` = CASE WHEN L.`RFID Number` = '' THEN '0000000000' ELSE L.`RFID Number` END THEN TRUE ELSE FALSE END AS RFIDMatch,
	R.`Net Weight MSB`/10 AS NetWeightRaw,
	L.`Net Weight` AS NetWeightActual,
	CASE WHEN R.`Net Weight MSB` = L.`Net Weight` THEN TRUE ELSE FALSE END AS NetWeightMatch,
	R.`Gross Weight MSB`/10 AS GrossWeightRaw,
	L.`Gross Weight` AS GrossWeightActual,
	CASE WHEN R.`Gross Weight MSB` = L.`Gross Weight` THEN TRUE ELSE FALSE END AS GrossWeightMatch,
	R.`Tare Weight MSB`/10 AS TareWeightRaw,
	L.`Tare Weight` AS TareWeightActual,
	CASE WHEN R.`Tare Weight MSB` = L.`Tare Weight` THEN TRUE ELSE FALSE END AS TareWeightMatch,
	R.`Date` AS LifterDateRaw,
	L.`Lifter Date` AS LifterDateActual,
	CASE WHEN R.`Date` = L.`Lifter Date` THEN TRUE ELSE FALSE END AS LifterDateMatch,
	R.`Time` AS LifterTimeRaw,
	L.`Lifter Time` AS LifterTimeActual,
	CASE WHEN R.`Time` = L.`Lifter Time` THEN TRUE ELSE FALSE END AS LifterTimeMatch,
	R.`Error Code2` AS ErrorCodeRaw,
	SUBSTRING_INDEX( L.`Error Code`, ' - ', 1 ) AS ErrorCodeActual,
	CASE WHEN  R.`Error Code2` = SUBSTRING_INDEX( L.`Error Code`, ' - ', 1 ) THEN TRUE ELSE FALSE END AS ErrorCodeMatch,
	NULL AS WeighingCodeRaw,
	NULL AS WeighingCodeActual,
	NULL AS WeighingCodeMatch,
	L.gpseventID,
	L.gpseventdate,
	L.resourceID,
	L.deviceID,
	L.version,
	L.weighingsystem 


FROM fact_uatlifts_commercial L

INNER JOIN dim_amcssimlifts_commercial R ON R.`Sequence number of record` = L.`Sequence Number`

WHERE L.weighingsystem = 'AMCS'

UNION ALL

SELECT DISTINCT
	R.`Sequence Number` AS SequenceNumberRaw,
	L.`Sequence Number` AS SequenceNumberActual,
	CASE WHEN R.`Sequence Number` = L.`Sequence Number` THEN TRUE ELSE FALSE END AS SequenceNumberMatch,
	R.`Bin Lifter` AS BinLifterRaw,
	L.`Bin Lifter` AS BinLifterActual,
	CASE WHEN LEFT(L.`Bin Lifter`,1) = R.`Bin Lifter` THEN TRUE ELSE FALSE END AS BinLifterMatch,
	R.`RFID Number` AS RFIDNumberRaw,
	CASE WHEN L.`RFID Number` = '' THEN '0000000000' ELSE L.`RFID Number` END AS RFIDNumberActual,
	CASE WHEN R.`RFID Number` = CASE WHEN L.`RFID Number` = '' THEN '0000000000' ELSE L.`RFID Number` END THEN TRUE ELSE FALSE END AS RFIDMatch,
	R.`Net Weight`/10 AS NetWeightRaw,
	L.`Net Weight` AS NetWeightActual,
	CASE WHEN R.`Net Weight`/10 = L.`Net Weight` THEN TRUE ELSE FALSE END AS NetWeightMatch,
	R.`Gross Weight`/10 AS GrossWeightRaw,
	L.`Gross Weight` AS GrossWeightActual,
	CASE WHEN R.`Gross Weight`/10 = L.`Gross Weight` THEN TRUE ELSE FALSE END AS GrossWeightMatch,
	R.`Tare Weight`/10 AS TareWeightRaw,
	L.`Tare Weight` AS TareWeightActual,
	CASE WHEN R.`Tare Weight`/10 = L.`Tare Weight` THEN TRUE ELSE FALSE END AS TareWeightMatch,
	R.`Lifter Date` AS LifterDateRaw,
	L.`Lifter Date` AS LifterDateActual,
	CASE WHEN R.`Lifter Date` = L.`Lifter Date` THEN TRUE ELSE FALSE END AS LifterDateMatch,
	R.`Lifter Time` AS LifterTimeRaw,
	L.`Lifter Time` AS LifterTimeActual,
	CASE WHEN R.`Lifter Time` = L.`Lifter Time` THEN TRUE ELSE FALSE END AS LifterTimeMatch,
	R.`Error Code` AS ErrorCodeRaw,
	LEFT(SUBSTRING_INDEX( L.`Error Code`, 'WeightError: ', -1 ),1) AS ErrorCodeActual,
	CASE WHEN R.`Error Code`= LEFT(SUBSTRING_INDEX( L.`Error Code`, 'WeightError: ', -1 ),1) THEN TRUE ELSE FALSE END AS ErrorCodeMatch,
	R.`RFID Error` AS RFIDCodeRaw,
	LEFT(SUBSTRING_INDEX( L.`Error Code`, 'TagError: ', -1 ),1) AS RFIDCodeActual,
	CASE WHEN R.`RFID Error`= LEFT(SUBSTRING_INDEX( L.`Error Code`, 'TagError: ', -1 ),1) THEN TRUE ELSE FALSE END AS WeighingCodeMatch,
	L.gpseventID,
	L.gpseventdate,
	L.resourceID,
	L.deviceID,
	L.version,
	L.weighingsystem
	
FROM fact_uatlifts_commercial L

INNER JOIN dim_terbergsimlifts_commercial R ON R.`Sequence Number` = L.`Sequence Number`

WHERE L.weighingsystem = 'Terberg' AND `Full Message` NOT LIKE '%lift%'