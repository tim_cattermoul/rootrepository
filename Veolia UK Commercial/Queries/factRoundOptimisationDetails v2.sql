drop table #temp;
drop table #temp2;
drop table #temp3;

select
	rg.roundgroupID,
	rg.roundgroup,
	ri.roundinstanceID,
	convert(date,ri.iss_date) as Day,
	t.taskid as TaskID,
	t.servicetaskID as ServiceTaskID,
	task as Task,
	ts.taskstate as TaskState,
	row_number () over (partition by ri.roundinstanceID order by t.taskenddate ) as ActSequence,
	cast(taskenddate as time) as TaskEnd,
	cast(taskenddate as date) as TaskEndDay,
	cast(servicetaskscheduleddate as date) as ScheduledTaskEndDay,
	datediff(second, lag(taskenddate, 1) over (partition by ri.roundinstanceID order by t.taskenddate), taskenddate) as ActTimeDiffS,
	t.sequence as OptSequence,
	row_number () over (partition by ri.roundinstanceID order by t.sequence) as OptSequenceNorm,
	CASE WHEN ts.taskstate like '%Complete%' then dense_rank () over (partition by ri.roundinstanceID, case when ts.taskstate like '%Complete%' THEN 1 ELSE NULL end order by t.sequence) ELSE NULL END as OptSequenceNormAttempted,
	--rip.roundinstanceplanID,
	--tp.taskplanID,
	--tp.sequence,
	--cast(rtv.timeofservice as date) as OptDateOfService,
	--cast(rtv.timeofservice as time) as OptTimeOfService,
	cast(tp.timeofservice as date) as OptDateOfService,
	cast(tp.timeofservice as time) as OptTimeOfService,
	--datediff(second, '00:00:00', rtv.transittimefromlasttask) as OptTimeDiffS,
	datediff(second, '00:00:00', tp.transittimefromlasttask) as OptTimeDiffS,
	--rtv.echoscheduledroundinstanceid,
	t.scheduledroundinstanceID,
	--rtv.echoroundinstanceid,
	rip.roundinstanceID as echoroundinstanceID,
	--rtv.islastinload TipAfter,
	tp.islastinroundsection TipAfter, -- Is this the same?
	--rtv.estimatedweight as EstWeight,
	--rtv.cumulativeweight as EstCumulativeWeight,
	tp.estimatedweight as EstWeight,
	tp.cumulativeweight as EstCumulativeWeight,
	t.statuslat,
	t.statuslon
into #temp
from tasks t
inner join taskstates ts on t.taskstateid = ts.taskstateid
--left join [EchoReschedule]..[res_taskvisits] rtv on t.taskID = rtv.echotaskid and rtv.echoroundinstanceid = t.roundinstanceID
inner join roundinstances ri on ri.roundinstanceID = t.roundinstanceID
inner join rounds r on r.roundid = t.roundid
inner join roundgroups rg on rg.roundgroupid = r.roundgroupid
left join roundinstanceplans rip on rip.roundinstanceID = ri.roundinstanceID
left join taskplans tp on tp.taskID = t.taskID and rip.roundinstanceplanID = tp.roundinstanceplanID
where
	ri.roundinstanceID in (216231, 216331) and ts.taskstate not in ('On Stop','Suspended')
order by rg.roundgroup, t.sequence

select T.*, CASE WHEN T.roundinstanceID = T.scheduledroundinstanceID THEN 1 ELSE 0 END as TaskOnScheduledRound, --Option 1
--NULL as TaskOnRound,
--NULL as ActionedTaskOnRound,

CASE WHEN T.roundinstanceID = T.echoroundinstanceid THEN 1 ELSE 0 END as TaskOnRound, -- Option 2a
CASE WHEN T.roundinstanceID = T.echoroundinstanceid and taskstate like '%Complete%' THEN 1 ELSE 0 END as ActionedTaskOnRound, -- Option 2a
	
	CASE WHEN T.TaskEndDay = T.ScheduledTaskEndDay THEN 1 ELSE 0 END AS TaskCollectedOnScheduledDay, 
	CASE WHEN T.actSequence = 1 THEN T.OptSequenceNormAttempted ELSE LAG(T.OptSequenceNormAttempted) over (partition by roundinstanceID ORDER BY T.ActSequence) END as PreviousOptSeq, (T.OptSequenceNormAttempted-CASE WHEN T.actSequence = 1 THEN 0 ELSE LAG(T.OptSequenceNormAttempted) over (partition by roundinstanceID ORDER BY T.ActSequence) END) as DifferenceInSequenceNo

into #temp2
from #temp T

select *, convert(time,dateadd(second,T.ActTimeDiffS, '00:00:00')) as ActTimeDiffSec, convert(time,dateadd(second,T.OptTimeDiffS, '00:00:00')) as OptTimeDiffSec, CASE WHEN T.DifferenceInSequenceNo = 1 THEN 1 ELSE 0 END as InSequence
into #temp3
from #temp2 T

select *, CASE WHEN statuslat IS NULL THEN 0 ELSE 1 END as TaskCompletedOnRound from #temp3

