select
	rg.roundgroupID,
	rg.roundgroup,
	ri.roundinstanceID,
	convert(date,ri.iss_date) as Day,
	t.taskid as TaskID,
	t.servicetaskID as ServiceTaskID,
	cast(task as varchar) as Task,
	ts.taskstate as TaskState,
	row_number () over (partition by ri.roundinstanceID order by t.taskenddate ) as ActSequence,
	cast(taskenddate as time) as TaskEnd,
	cast(taskenddate as date) as TaskEndDay,
	cast(servicetaskscheduleddate as date) as ScheduledTaskEndDay,
	datediff(second, lag(taskenddate, 1) over (partition by ri.roundinstanceID order by t.taskenddate), taskenddate) as ActTimeDiffS,
	t.sequence as OptSequence,
	row_number () over (partition by ri.roundinstanceID order by t.sequence) as OptSequenceNorm,
	rip.roundinstanceplanID,
	tp.taskplanID,
	tp.sequence,
	--cast(rtv.timeofservice as date) as OptDateOfService,
	--cast(rtv.timeofservice as time) as OptTimeOfService,
	cast(tp.timeofservice as date) as OptDateOfService,
	cast(tp.timeofservice as time) as OptTimeOfService,
	--datediff(second, '00:00:00', rtv.transittimefromlasttask) as OptTimeDiffS,
	datediff(second, '00:00:00', tp.transittimefromlasttask) as OptTimeDiffS,
	--rtv.echoscheduledroundinstanceid,
	t.scheduledroundinstanceID,
	--rtv.echoroundinstanceid,
	--rtv.islastinload TipAfter,
	tp.islastinroundsection TipAfter, -- Is this the same?
	--rtv.estimatedweight as EstWeight,
	--rtv.cumulativeweight as EstCumulativeWeight,
	tp.estimatedweight as EstWeight,
	tp.cumulativeweight as EstCumulativeWeight,
	t.statuslat,
	t.statuslon
--into #temp
from tasks t
inner join taskstates ts on t.taskstateid = ts.taskstateid
--left join [EchoReschedule]..[res_taskvisits] rtv on t.taskID = rtv.echotaskid and rtv.echoroundinstanceid = t.roundinstanceID
inner join roundinstances ri on ri.roundinstanceID = t.roundinstanceID
inner join rounds r on r.roundid = t.roundid
inner join roundgroups rg on rg.roundgroupid = r.roundgroupid
left join roundinstanceplans rip on rip.roundinstanceID = ri.roundinstanceID
left join taskplans tp on tp.taskID = t.taskID and rip.roundinstanceplanID = tp.roundinstanceplanID
where
	datediff(day,ri.iss_date,getdate()) between 1 and 70
	and ri.isoptimised = 1
	--and TS.taskstate <> 'Cancelled'
	--and rg.roundgroup not in ('Delivery 1', 'RORO', 'F1','GL1','G5','Spare', 'Training Rec','Training Ref')
order by ri.roundinstanceID, t.sequence