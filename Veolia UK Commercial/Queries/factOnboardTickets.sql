select cg.customergroupID, customergroup, rd.requestdata, COUNT(r.requestID) as Requests from requests r

INNER JOIN requestdata rd on rd.requestID = r.requestID and rd.requestdatafieldID = 3
INNER JOIN systems s on s.systemID = r.systemID
INNER JOIN customergroups cg on cg.customergroupID = r.customergroupID

WHERE r.systemID = 68 -- Onboard
AND rd.requestdata IS NOT NULL AND rd.requestdata NOT IN ('','N/A') -- Version is filled
AND r.parent_requestID = 0 -- Parent / Orphan Tickets only
and cg.customergroupID in (284) -- Veolia UK - Commercial

GROUP BY cg.customergroupID, customergroup, rd.requestdata