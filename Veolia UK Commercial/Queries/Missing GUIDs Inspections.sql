select I.inspectionID, I.inspectioncreateddate, I.sourceechoID, R.resourceID, R.resource,GEI.sourceGUID, E.gpseventID from gpseventinspections GEI



LEFT JOIN GPSevents E on E.guid = GEI.sourceGUID

INNER JOIN inspections I on I.inspectionID = GEI.targetechoID

INNER JOIN roundinstances RI on RI.roundinstanceID = I.sourceechoID

LEFT JOIN resourceallocations RA on RA.roundinstanceID = RI.roundinstanceID

LEFT JOIN resourcetypes RT on rt.resourcetypeID = RA.resourcetypeID

LEFT JOIN resources R on R.resourceID = RA.resourceID







WHERE E.gpseventID IS NULL and GEI.sourceGUID IS NOT NULL and RT.resourceclassID = 2



ORDER BY inspectioncreateddate desc