select distinct tl.tasklineID, t.taskcompleteddate, t.roundinstanceID, R.resource, tl.gpseventguid, E.guid, E.gpseventID from tasklines TL



LEFT JOIN GPSevents E on E.guid = TL.gpseventguid

LEFT JOIN tasks T on t.taskID = tl.taskID

LEFT JOIN resourceallocations RA on RA.roundinstanceID = T.roundinstanceID

LEFT JOIN resourcetypes RT on rt.resourcetypeID = RA.resourcetypeID

LEFT JOIN resources R on R.resourceID = RA.resourceID



WHERE E.gpseventID IS NULL and tl.gpseventguid IS NOT NULL and RT.resourceclassID = 2



ORDER BY tl.tasklineID desc