WITH CTE_temp as (
select
       GPS.gpseventID,
	   GPS.gpstraildataID
       ,GPS.gpseventdate
	   ,GPS.roundinstanceID -- Added 2022/9/6
	   ,GPS.guid
	   ,TD.lat,
		TD.long
	   ,max(C.contractID) as ContractID
       ,max(R.resourceID) as resourceID
       ,max(GPS.gpseventtypeID) as gpseventtpeID


	   -- ECHO2_Commercial
          ,max(case when gpseventtypedatatypeID =2 then gpseventdata end) as [RFID Tag] 
		  ,max(case when gpseventtypedatatypeID =8 then gpseventdata end) as [RFID Number]
          ,max(case when gpseventtypedatatypeID =6 then gpseventdata end) as [Net Weight]
          ,max(case when gpseventtypedatatypeID =11 then gpseventdata end) as [Gross Weight]  
          ,max(case when gpseventtypedatatypeID =12 then gpseventdata end) as [Tare Weight] 
          ,max(case when gpseventtypedatatypeID =7 then gpseventdata end) as [Units] 
          ,max(case when gpseventtypedatatypeID =9 then gpseventdata end) as [Bin Lifter] 
		  ,max(case when gpseventtypedatatypeID =5 then gpseventdata end) as [Lifter Date] 
          ,max(case when gpseventtypedatatypeID =10 then gpseventdata end) as [Lifter Time] 

       ,max(case when gpseventtypedatatypeID =4 then gpseventdata end) as [Sequence Number]
	             ,max(case when gpseventtypedatatypeID =54 then gpseventdata end) as [Error Code] 
         ,max(case when gpseventtypedatatypeID =13 then gpseventdata end) as [Full Message] 

from
       gpsevents GPS with (nolock)
	   INNER JOIN gpstraildata TD with (nolock) on TD.gpstraildataID = GPS.gpstraildataID
		left join resources R on (GPS.echotypeID =54 and GPS.echoID=R.resourceID)
		left join contracts C on C.contractID = R.contractID
        left join gpseventdata  GPSD on GPS.gpseventId=GPSD.gpseventID
         
where
       gpseventtypeID=1
       --and echotypeId=54
          --and datediff(hour,gpseventdate,getdate()) <= 24 --
		  and gpseventdate between '2023-03-01 00:00:00.000' and '2023-03-02 00:00:00.000'
		  --and gpseventdate < getDate()
		  --and datediff(minute,gpseventdate,getdate()) > 5

group by
        GPS.gpseventID,
		GPS.gpstraildataID
       ,GPS.gpseventdate
	   ,GPS.roundinstanceID, GPS.guid, TD.lat, TD.long
), CTE_Lifts as (

select L.*, RI.startdate, RI.finishdate,
convert(datetime,concat(convert(date,[Lifter Date] ,103),' ',[Lifter Time] )) as liftdate,
CASE	WHEN [Full Message] like '%VWS%' THEN 'VWS' 
		WHEN [Full Message] like '41 4D 43 53%' THEN 'AMCS'
		WHEN [Full Message] = 'Bin lift trigger detected' THEN 'Pewatron'
		ELSE 'Terberg' END AS WeighingSystem,
G.positiondate, G.receiveddate, G.deviceID, G.speed 

--into #lifts
from CTE_temp L
INNER JOIN gpstraildata G with(nolock) on G.gpstraildataID = L.gpstraildataID
		-- Changed joins to get round instance ID from GPSEvent
		left join roundinstances RI on L.roundinstanceID = RI.roundInstanceID
		left join resourceallocations RA on RA.resourceID = L.resourceID and ra.roundinstanceID = ri.roundinstanceID
)
,CTE_Lifts3 as (
select *, datediff_big(second,LiftDate,dateadd(minute,-datepart(tz,gpseventdate at time zone 'GMT Standard Time'),gpseventdate)) as TimeLag
--into #lifts3
from cte_lifts
--order by gpseventID asc
), CTE_Lifts4 as (
select *
,NULL as SlowLift
,CASE WHEN convert(date,liftdate) <> convert(date,dateadd(minute,-datepart(tz,gpseventdate at time zone 'GMT Standard Time'),gpseventdate)) THEN 1 
		WHEN dateadd(minute,-datepart(tz,gpseventdate at time zone 'GMT Standard Time'),gpseventdate) > finishdate THEN 1
		ELSE 0 END as LateLift
--into #lifts4
from  CTE_lifts3
), CTE_Final as (
-- RAW DATA QUERY

SELECT DISTINCT
	gpseventID,
	guid,
	contractID,
	gpseventdate,
	dateadd(minute,-datepart(tz,gpseventdate at time zone 'GMT Standard Time'),gpseventdate) as UTCGPSEventDate,
	convert(date,[Lifter Date],103) as [Lifter Date],
	[Lifter Time],
	[lat],
	[long],
	[Sequence Number],
	[Net Weight],
	[Gross Weight],
	[Tare Weight],
	[Bin Lifter],
	[RFID Number],
	[Error Code],
	Replace([Full Message], nchar(65533) COLLATE Latin1_General_BIN2, '')[Full Message],
	gpstraildataID,
	resourceID,
	gpseventtpeID,
	--resource,
	[RFID Tag],
	units,
	roundinstanceID,
	startdate,
	finishdate,
	liftdate,
	weighingsystem,
	positiondate,
	receiveddate,
	speed,
	deviceid,
	timelag,
	slowlift,
	LateLift,
	CASE WHEN [Net Weight] in ('Max Wt','Min Wt') THEN 0 WHEN CAST([Tare Weight] as float)> CAST([Gross Weight] as float) THEN 1 WHEN [Net Weight] = 'Neg Wt' THEN 1 ELSE 0 END as NegativeLift,
	getdate() as TimeDataExtracted from CTE_lifts4
	--where datepart(year,liftdate) >= 2020
--ORDER by 1,3,4

),CTE_Med as (

SELECT DISTINCT [Lifter Date], deviceID,-- STDEV(timelag) as StDLag, AVG(timelag) as AvgLag,
PERCENTILE_CONT(0.5) WITHIN GROUP (ORDER BY timelag) OVER (PARTITION BY [Lifter Date], deviceID) AS Median

FROM CTE_Final

GROUP BY [Lifter Date], deviceID, timelag)

,CTE_StD as (

SELECT DISTINCT [Lifter Date], deviceID, STDEV(timelag) as StDLag, AVG(timelag) as AvgLag
FROM CTE_Final

GROUP BY [Lifter Date], deviceID)
, CTE_Tasks as (

SELECT T.taskID, T.roundinstanceID, RG.roundgroup, T.echotypeID, T.echoID, T.task, T.lat,t.long, SUM(TL.scheduledassetquantity) as ScheduledAssetQuantity from tasks T

INNER JOIN tasklines TL on TL.taskID = T.taskID
INNER JOIN rounds R on R.roundID = T.roundID
INNER JOIN roundgroups RG on RG.roundgroupID = R.roundgroupID

WHERE T.roundinstanceID in 
(
212704,
212705,
212708,
212703,
212707
) GROUP BY T.taskID, T.roundinstanceID, RG.roundgroup, T.echotypeID, T.echoID, T.task, T.lat,t.long), CTE_Tasklines as (

SELECT T.TaskID,TL.tasklineID, tL.gpseventguid FROM Tasklines TL INNER JOIN tasks T on T.TaskID = TL.taskID


WHERE T.roundinstanceID in 
(
212704,
212705,
212708,
212703,
212707
)

and TL.gpseventguid IS NOT NULL
)
,CTE_Inspections as (
select I.inspectionID, I.inspectioncreateddate, I.sourceobjectdesc, GI.* 
--into #inspections
from inspections I
left join gpseventinspections GI on gi.targetechoID = I.inspectionID
where GI.sourceGUID in (SELECT distinct guid from CTE_Lifts)--CONVERT(DATE,inspectioncreateddate) >= '2021-06-18 00:00:00.000' AND DATEDIFF(day,inspectioncreateddate,getdate()) = 0
)


, CTE_Consol as (
SELECT DISTINCT F.gpseventID,
F.guid,
	F.contractID,
	gpseventdate,
	UTCGPSEventDate,
	F.[Lifter Date],
	[Lifter Time],
	F.[lat],
	F.[long],
	[Sequence Number],
	[Net Weight],
	[Gross Weight],
	[Tare Weight],
	[Bin Lifter],
	[RFID Number],
	[Error Code],
	[Full Message],
	gpstraildataID,
	resourceID,
	gpseventtpeID,
	--resource,
	[RFID Tag],
	units,
	F.roundinstanceID,
	startdate,
	finishdate,
	liftdate,
	weighingsystem,
	positiondate,
	receiveddate,
	speed,
	F.deviceid,
	CASE WHEN weighingsystem = 'Pewatron' THEN 0 ELSE timelag end as timelag,
	CASE WHEN weighingsystem = 'Pewatron' THEN 0 WHEN F.timelag - M.median > 120 THEN 1 ELSE 0 END as slowlift,
	CASE WHEN weighingsystem = 'Pewatron' THEN 0 ELSE LateLift END as LateLift,
	NegativeLift,
	CASE WHEN WeighingSystem = 'Pewatron' THEN case when speed > 8 THEN 1 ELSE 0 END ELSE 0 END as FalseTiltLift,
	REPLACE(DES.version,'com.twistedfish.OnBoard Release ','') as version,
	TimeDataExtracted,
		CASE WHEN F.deviceID = 16 THEN 212704
	WHEN F.deviceID = 100 THEN 212705
	WHEN F.deviceID = 101 THEN 212708
	WHEN F.deviceID = 103 THEN 212703
	WHEN F.deviceID = 102 THEN 212707
	ELSE NULL END AS AdjustedRoundInstanceID,
	T.taskID,
	T.roundinstanceID as TaskRoundInstanceID,
	T.roundgroup,
	T.echotypeID, T.echoID,
	T.task,
	T.ScheduledAssetQuantity,
	T.lat as tasklat,
	T.long as tasklong,
	TL.tasklineID as MatchedToTaskline,
	TL.taskID as MatchedToTask,
	I.inspectionID as MatchedToInspection


--, Std.StDLag , M.Median, CASE WHEN F.timelag - M.median > 120 THEN 1 ELSE 0 END as Var2M
FROM CTE_Final F

LEFT JOIN CTE_StD StD on Std.deviceID = F.deviceID and Std.[Lifter Date] = F.[Lifter Date]
LEFT JOIN CTE_Med M on M.deviceID = F.deviceID and M.[Lifter Date] = F.[Lifter Date]
LEFT JOIN deviceechosystems DES on DES.deviceID = F.deviceID
LEFT JOIN CTE_Inspections I on I.sourceGUID = F.guid
LEFT JOIN CTE_Tasklines TL on TL.gpseventguid = F.guid
LEFT JOIN CTE_Tasks T on T.roundinstanceID = CASE WHEN F.deviceID = 16 THEN 212704
	WHEN F.deviceID = 100 THEN 212705
	WHEN F.deviceID = 101 THEN 212708
	WHEN F.deviceID = 103 THEN 212703
	WHEN F.deviceID = 102 THEN 212707
	ELSE NULL END
	AND T.TaskID not in (SELECT Distinct TaskID from CTE_Tasklines)
)

, CTE_Distance as (
SELECT *,GEOGRAPHY::Point(lat, long, 4326).STDistance(GEOGRAPHY::Point(Tasklat, Tasklong, 4326)) as DistanceFromTaskToLift from CTE_Consol
where MatchedToTaskline IS NULL AND MatchedToInspection IS NULL

)

, CTE_DistanceRank as (
SELECT *, ROW_NUMBER () OVER (PARTITION BY GPSEventID order by DistanceFromTaskToLift asc) as DistanceRank from CTE_Distance
)

SELECT *
	, CASE WHEN DistanceRank = 1 and DistanceFromTaskToLift > 50 then 1 ELSE 0 END as NoTasksWithinProximity -- CHANGE the >50 to whatever distance you want
	, CASE WHEN DistanceFromTaskToLift <= 50 then 1 ELSE 0 END as TaskWithinProximity -- CHANGE the >50 to whatever distance you want, but make sure it is the same as the above value

FROM CTE_DistanceRank where DistanceRank <= 5 and FalseTiltLift = 0