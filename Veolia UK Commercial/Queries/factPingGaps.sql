SELECT *, datediff(minute,previouspositiondate,positiondate) as timegap, ROUND(previousspeed - speed,1) as changeinspeed, CASE WHEN lat = 0 or previouslat = 0 THEN NULL ELSE GEOGRAPHY::Point(lat, long, 4326).STDistance(GEOGRAPHY::Point(previouslat, previouslong, 4326)) END as MetresTravelled
FROM (
	select 
	gpstraildataID, 
	deviceID,
	echotypeID,
	echoID,
	--resource,
	positiondate, 
	ROUND(speed,1) as speed, 
	lat, 
	long,
	LAG (gpstraildataID) over (partition by deviceid, convert(date,positiondate) order by positiondate) as traildataID,
	LAG (positiondate) over (partition by deviceid, convert(date,positiondate) order by positiondate) as previouspositiondate,
	LAG (ROUND(speed,1)) over (partition by deviceid, convert(date,positiondate) order by positiondate) as previousspeed,
	LAG (lat) over (partition by deviceid, convert(date,positiondate) order by positiondate) as previouslat,
	LAG (long) over (partition by deviceid, convert(date,positiondate) order by positiondate) as previouslong


	from gpstraildata T with (nolock)
	--INNER JOIN resources R on T.echoID = R.resourceID

	where datediff(day,positiondate,getdate()) <= 360 and echotypeID = 54 and t.echoID in (53,1,3,44,4,6) /*and deviceID in (60,62,75,77,79,82)*/) as A

	where (speed <> 0 or previousspeed <> 0) and

	datediff(minute,previouspositiondate,positiondate) > 2 and datepart(hour,positiondate) between 3 and 14