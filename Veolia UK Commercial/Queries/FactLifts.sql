WITH CTE_temp as (
select
       GPS.gpseventID,
	   GPS.gpstraildataID
       ,GPS.gpseventdate
	   ,GPS.roundinstanceID -- Added 2022/9/6
	   ,TD.lat,
		TD.long
	   ,max(C.contractID) as ContractID
       ,max(R.resourceID) as resourceID
       ,max(GPS.gpseventtypeID) as gpseventtpeID


	   -- ECHO2_Commercial
          ,max(case when gpseventtypedatatypeID =2 then gpseventdata end) as [RFID Tag] 
		  ,max(case when gpseventtypedatatypeID =8 then gpseventdata end) as [RFID Number]
          ,max(case when gpseventtypedatatypeID =6 then gpseventdata end) as [Net Weight]
          ,max(case when gpseventtypedatatypeID =11 then gpseventdata end) as [Gross Weight]  
          ,max(case when gpseventtypedatatypeID =12 then gpseventdata end) as [Tare Weight] 
          ,max(case when gpseventtypedatatypeID =7 then gpseventdata end) as [Units] 
          ,max(case when gpseventtypedatatypeID =9 then gpseventdata end) as [Bin Lifter] 
		  ,max(case when gpseventtypedatatypeID =5 then gpseventdata end) as [Lifter Date] 
          ,max(case when gpseventtypedatatypeID =10 then gpseventdata end) as [Lifter Time] 

       ,max(case when gpseventtypedatatypeID =4 then gpseventdata end) as [Sequence Number]
	             ,max(case when gpseventtypedatatypeID =14 then gpseventdata end) as [Error Code] 
         ,max(case when gpseventtypedatatypeID =13 then gpseventdata end) as [Full Message] 

from
       gpsevents GPS with (nolock)
	   INNER JOIN gpstraildata TD with (nolock) on TD.gpstraildataID = GPS.gpstraildataID
		left join resources R on (GPS.echotypeID =54 and GPS.echoID=R.resourceID)
		left join contracts C on C.contractID = R.contractID
        left join gpseventdata  GPSD on GPS.gpseventId=GPSD.gpseventID
         
where
       gpseventtypeID=1
       --and echotypeId=54
          and datediff(hour,gpseventdate,getdate()) <= 24 --
		  --and gpseventdate between '2022-10-13 00:00:00.000' and '2022-10-14 00:00:00.000'
		  --and gpseventdate < getDate()
		  --and datediff(minute,gpseventdate,getdate()) > 5

group by
        GPS.gpseventID,
		GPS.gpstraildataID
       ,GPS.gpseventdate
	   ,GPS.roundinstanceID, TD.lat, TD.long
), CTE_Lifts as (

select L.*, RI.startdate, RI.finishdate,
convert(datetime,concat(convert(date,[Lifter Date] ,103),' ',[Lifter Time] )) as liftdate,
CASE	WHEN [Full Message] like '%VWS%' THEN 'VWS' 
		WHEN [Full Message] like '41 4D 43 53%' THEN 'AMCS'
		WHEN [Full Message] = 'Bin lift trigger detected' THEN 'Pewatron'
		ELSE 'Terberg' END AS WeighingSystem,
G.positiondate, G.receiveddate, G.deviceID, G.speed 

--into #lifts
from CTE_temp L
INNER JOIN gpstraildata G with(nolock) on G.gpstraildataID = L.gpstraildataID
		-- Changed joins to get round instance ID from GPSEvent
		left join roundinstances RI on L.roundinstanceID = RI.roundInstanceID
		left join resourceallocations RA on RA.resourceID = L.resourceID and ra.roundinstanceID = ri.roundinstanceID
)
,CTE_Lifts3 as (
select *, datediff_big(second,LiftDate,dateadd(minute,-datepart(tz,gpseventdate at time zone 'GMT Standard Time'),gpseventdate)) as TimeLag
--into #lifts3
from cte_lifts
--order by gpseventID asc
), CTE_Lifts4 as (
select *
,NULL as SlowLift
,CASE WHEN convert(date,liftdate) <> convert(date,dateadd(minute,-datepart(tz,gpseventdate at time zone 'GMT Standard Time'),gpseventdate)) THEN 1 
		WHEN dateadd(minute,-datepart(tz,gpseventdate at time zone 'GMT Standard Time'),gpseventdate) > finishdate THEN 1
		ELSE 0 END as LateLift
--into #lifts4
from  CTE_lifts3
), CTE_Final as (
-- RAW DATA QUERY

SELECT DISTINCT
	gpseventID,
	contractID,
	gpseventdate,
	dateadd(minute,-datepart(tz,gpseventdate at time zone 'GMT Standard Time'),gpseventdate) as UTCGPSEventDate,
	convert(date,[Lifter Date],103) as [Lifter Date],
	[Lifter Time],
	[lat],
	[long],
	[Sequence Number],
	[Net Weight],
	[Gross Weight],
	[Tare Weight],
	[Bin Lifter],
	[RFID Number],
	[Error Code],
	Replace([Full Message], nchar(65533) COLLATE Latin1_General_BIN2, '')[Full Message],
	gpstraildataID,
	resourceID,
	gpseventtpeID,
	--resource,
	[RFID Tag],
	units,
	roundinstanceID,
	startdate,
	finishdate,
	liftdate,
	weighingsystem,
	positiondate,
	receiveddate,
	speed,
	deviceid,
	timelag,
	slowlift,
	LateLift,
	CASE WHEN [Net Weight] in ('Max Wt','Min Wt') THEN 0 WHEN CAST([Tare Weight] as float)> CAST([Gross Weight] as float) THEN 1 WHEN [Net Weight] = 'Neg Wt' THEN 1 ELSE 0 END as NegativeLift,
	getdate() as TimeDataExtracted from CTE_lifts4
	--where datepart(year,liftdate) >= 2020
--ORDER by 1,3,4

),CTE_Med as (

SELECT DISTINCT [Lifter Date], deviceID,-- STDEV(timelag) as StDLag, AVG(timelag) as AvgLag,
PERCENTILE_CONT(0.5) WITHIN GROUP (ORDER BY timelag) OVER (PARTITION BY [Lifter Date], deviceID) AS Median

FROM CTE_Final

GROUP BY [Lifter Date], deviceID, timelag)

,CTE_StD as (

SELECT DISTINCT [Lifter Date], deviceID, STDEV(timelag) as StDLag, AVG(timelag) as AvgLag
FROM CTE_Final

GROUP BY [Lifter Date], deviceID)


SELECT DISTINCT gpseventID,
	F.contractID,
	gpseventdate,
	UTCGPSEventDate,
	F.[Lifter Date],
	[Lifter Time],
	[lat],
	[long],
	[Sequence Number],
	[Net Weight],
	[Gross Weight],
	[Tare Weight],
	[Bin Lifter],
	[RFID Number],
	[Error Code],
	[Full Message],
	gpstraildataID,
	resourceID,
	gpseventtpeID,
	--resource,
	[RFID Tag],
	units,
	roundinstanceID,
	startdate,
	finishdate,
	liftdate,
	weighingsystem,
	positiondate,
	receiveddate,
	speed,
	F.deviceid,
	CASE WHEN weighingsystem = 'Pewatron' THEN 0 ELSE timelag end as timelag,
	CASE WHEN weighingsystem = 'Pewatron' THEN 0 WHEN F.timelag - M.median > 120 THEN 1 ELSE 0 END as slowlift,
	CASE WHEN weighingsystem = 'Pewatron' THEN 0 ELSE LateLift END as LateLift,
	NegativeLift,
	CASE WHEN WeighingSystem = 'Pewatron' THEN case when speed > 8 THEN 1 ELSE 0 END ELSE 0 END as FalseTiltLift,
	REPLACE(DES.version,'com.twistedfish.OnBoard Release ','') as version,
	TimeDataExtracted

--, Std.StDLag , M.Median, CASE WHEN F.timelag - M.median > 120 THEN 1 ELSE 0 END as Var2M
FROM CTE_Final F

LEFT JOIN CTE_StD StD on Std.deviceID = F.deviceID and Std.[Lifter Date] = F.[Lifter Date]
LEFT JOIN CTE_Med M on M.deviceID = F.deviceID and M.[Lifter Date] = F.[Lifter Date]
LEFT JOIN deviceechosystems DES on DES.deviceID = F.deviceID



