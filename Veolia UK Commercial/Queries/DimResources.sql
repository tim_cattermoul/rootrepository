select resourceID, resource, contractID, R.resourcetypeID, RT.resourceclassID, CONVERT(DATETIME,R.startdate) startdate, convert(datetime,R.enddate) enddate, RT.resourcetype

from resources R

INNER JOIN resourcetypes RT on RT.resourcetypeID = R.resourcetypeID

WHERE R.enddate > getdate()

