from FactImport import importFact
from DimensionImport import importDimension
import os

try:
    #importFact('apacsqlprod-02.echo.services,10001','Echo_Aus_Prod',r'C:\Users\tim.cattermoul\Documents\Repositories\SUEZ Australia\Queries\FactRoundInstances.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','fact_roundinstances_suezaustralia','replace',False,'roundinstanceID')
    #importFact('apacsqlprod-02.echo.services,10001','Echo_Aus_Prod',r'C:\Users\tim.cattermoul\Documents\Repositories\SUEZ Australia\Queries\FactLiftMatching.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','fact_liftmatching_suezaustralia','replace',False,'gpseventID')
    #importFact('apacsqlprod-02.echo.services,10001','Echo_Aus_Prod',r'C:\Users\tim.cattermoul\Documents\Repositories\SUEZ Australia\Queries\FactLifts.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','fact_lifts_suezaustralia','replace',False,'gpseventID')
    #importFact('apacsqlprod-02.echo.services,10001','Echo_Aus_Prod',r'C:\Users\tim.cattermoul\Documents\Repositories\SUEZ Australia\Queries\FactDelayedLiftsTEMP.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','fact_delayedlifts_suezaustralia','replace',False,'gpseventID')
    importFact('apacsqlprod-02.echo.services,10001','Echo_Aus_Prod',r'C:\Users\tim.cattermoul\Documents\Repositories\SUEZ Australia\Queries\FactPings.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','fact_pings_suezaustralia','replace',False,'Day')
    importFact('apacsqlprod-02.echo.services,10001','Echo_Aus_Prod',r'C:\Users\tim.cattermoul\Documents\Repositories\SUEZ Australia\Queries\FactWorkingHours.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','fact_workinghours_suezaustralia','replace',False,'Day')
    #importDimension('apacsqlprod-02.echo.services,10001','Echo_Aus_Prod',r'C:\Users\tim.cattermoul\Documents\Repositories\SUEZ Australia\Queries\DimRounds.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','dim_rounds_suezaustralia','replace',False)

    import sqlalchemy

    engine = sqlalchemy.create_engine("mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",
                                      connect_args=dict(host='echobivuk.mariadb.database.azure.com', port=3306,
                                                        ssl_ca=r'C:\Users\tim.cattermoul\Documents\MariaDB SQL Certificate (DO NOT DELETE)\BaltimoreCyberTrustRoot.crt.pem'))

    import subprocess, sys
    print("Importing Onboard Crash Data")
    p = subprocess.Popen(["powershell.exe",
                  r'&"C:\Users\tim.cattermoul\Documents\Repositories\SUEZ Australia\Scripts\OnboardCrashData.ps1"'],
                  stdout=sys.stdout)
    p.communicate()

    with engine.begin() as preConn:
        #print("Updating Missing Lifts")
        #preConn.execute("CALL create_fact_missinglifts_suezaustralia();")
        #preConn.execute("CALL create_fact_duplicatelifts_suezaustralia();")
        #print("Missing Lifts Updated")
        print("Calculating crashes in shift")
        preConn.execute("CALL update_crashesinshift_suezaustralia();")
        print("Calculation complete")
        print("Updating Device Contracts")
        preConn.execute("CALL update_devicecontracts_suezaustralia();")
        print("Device Contracts Updated")

except Exception as e:

    from ScriptFailureEmail import sendfailedscriptemail

    sendfailedscriptemail(os.path.basename(__file__), str(e))




