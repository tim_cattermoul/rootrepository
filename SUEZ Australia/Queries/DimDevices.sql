select C.contractID, d.deviceID, R.resourceID, des.registereddate, des.lastreported, d.model, replace(des.version, 'com.twistedfish.OnBoard Release ','') as version from devices D
LEFT JOIN resources R on R.resourceID = d.associated_echoID and d.associated_echotypeID = 54
INNER JOIN deviceechosystems DES on DES.deviceID = d.deviceID and DES.echosystemID = 21 -- Onboard only - exclude Mobile for devices which operate both systems
LEFT JOIN contracts C on C.contractID = R.contractID

WHERE datediff(month,des.lastreported,getdate()) <= 1 

