from DimensionImport import importDimension

importDimension('redazsqlprod-01.echo.services','echo2',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia Australia\Queries\DimResources.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','dim_resources_australia','replace',False)
importDimension('redazsqlprod-01.echo.services','echo2',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia Australia\Queries\DimContracts.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','dim_contracts_australia','replace',False)
importDimension('redazsqlprod-01.echo.services','echo2',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia Australia\Queries\DimDevices.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','dim_devices_australia','replace',False)
importDimension('redazsqlprod-01.echo.services','echo2',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia Australia\Queries\DimRounds.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','dim_rounds_australia','replace',False)
importDimension('redazsqlprod-01.echo.services','echo2',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia Australia\Queries\DimDeviceEchoSystem.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','dim_deviceechosystems_australia','append',False)
#importDimension('redazsqlprod-01.echo.services','echo2',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia Australia\Queries\FactRoundInstances.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','fact_roundinstances_australia','replace',False)

import sqlalchemy

engine = sqlalchemy.create_engine("mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",
                                  connect_args=dict(host='echobivuk.mariadb.database.azure.com', port=3306,
                                                    ssl_ca=r'C:\Users\tim.cattermoul\Documents\MariaDB SQL Certificate (DO NOT DELETE)\BaltimoreCyberTrustRoot.crt.pem'))
with engine.begin() as preConn:
	print("Updating Target Onboard Versions")
    preConn.execute("CALL update_targetversion_australia()")
    print("Target Versions Updated")