from FactImport import importFact
from DimensionImport import importDimension
import os

try:
    #importFact('redazsqlprod-01.echo.services','echo2',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia Australia\Queries\FactRoundInstances.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','fact_roundinstances_australia','replace',False,'roundinstanceID')
    #importFact('redazsqlprod-01.echo.services','echo2',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia Australia\Queries\FactLiftMatching.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','fact_liftmatching_australia','replace',False,'gpseventID')
    #importFact('redazsqlprod-01.echo.services','echo2',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia Australia\Queries\FactLifts.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','fact_lifts_australia','replace',False,'gpseventID')
    #importFact('redazsqlprod-01.echo.services','echo2',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia Australia\Queries\FactDelayedLiftsTEMP.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','fact_delayedlifts_australia','replace',False,'gpseventID')
    importFact('redazsqlprod-01.echo.services','echo2',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia Australia\Queries\FactPings.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','fact_pings_australia','replace',False,'Day')
    importFact('redazsqlprod-01.echo.services','echo2',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia Australia\Queries\FactWorkingHours.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','fact_workinghours_australia','replace',False,'Day')
    #importDimension('redazsqlprod-01.echo.services','echo2',r'C:\Users\tim.cattermoul\Documents\Repositories\Veolia Australia\Queries\DimRounds.sql',"mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",'echobivuk.mariadb.database.azure.com','dim_rounds_australia','replace',False)

    import sqlalchemy

    engine = sqlalchemy.create_engine("mysql+pymysql://tim.cattermoul@echobivuk:PHFLC7Rf4TRp@echobivuk.mariadb.database.azure.com/echoreporting",
                                      connect_args=dict(host='echobivuk.mariadb.database.azure.com', port=3306,
                                                        ssl_ca=r'C:\Users\tim.cattermoul\Documents\MariaDB SQL Certificate (DO NOT DELETE)\BaltimoreCyberTrustRoot.crt.pem'))

    import subprocess, sys
    print("Importing Onboard Crash Data")
    p = subprocess.Popen(["powershell.exe",
                  r'&"C:\Users\tim.cattermoul\Documents\Repositories\Veolia Australia\Scripts\OnboardCrashData.ps1"'],
                  stdout=sys.stdout)
    p.communicate()

    with engine.begin() as preConn:
        #print("Updating Missing Lifts")
        #preConn.execute("CALL create_fact_missinglifts_australia();")
        #preConn.execute("CALL create_fact_duplicatelifts_australia();")
        #print("Missing Lifts Updated")
        print("Calculating crashes in shift")
        preConn.execute("CALL update_crashesinshift_australia();")
        print("Calculation complete")
        print("Updating Device Contracts")
        preConn.execute("CALL update_devicecontracts_australia();")
        print("Device Contracts Updated")

except Exception as e:

    from ScriptFailureEmail import sendfailedscriptemail

    sendfailedscriptemail(os.path.basename(__file__), str(e))




