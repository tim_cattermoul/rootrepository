WITH CTE_Lifts as (

	select
       GPS.gpseventID,
	   GPS.gpseventdate,
	   R.resourceID,
	   C.ContractID,
	   TD.deviceID,
	   GPS.guid,
	   --RA.roundinstanceID,
	   TD.lat,
	   TD.long
	   ,max(case when gpseventtypedatatypeID =5 then gpseventdata end) as [Lifter Date] 
       ,max(case when gpseventtypedatatypeID =10 then gpseventdata end) as [Lifter Time] 

from
       gpsevents GPS with (nolock)

INNER JOIN gpstraildata TD with (nolock) on TD.gpstraildataID = GPS.gpstraildataID
		left join resources R on (GPS.echotypeID =54 and GPS.echoID=R.resourceID)
		left join contracts C on C.contractID = R.contractID
		left join gpseventdata  GPSD on GPS.gpseventId=GPSD.gpseventID
		--convert(datetime,concat(convert(date,[Lifter Date] ,103),' ',[Lifter Time] ))
		--left join resourceallocations RA on RA.resourceID = R.resourceID and gpseventdate between RA.startdate and RA.enddate
         -- left join resourceallocations RA on RA.resourceID = L.resourceID and convert(date,RA.startdate) = convert(date,[Lifter Date] ,103) and convert(datetime,concat(convert(date,[Lifter Date] ,103),' ',[Lifter Time] )) between RA.startdate and RA.enddate
where
       gpseventtypeID=1
       --and echotypeId=54
         and datediff(hour,gpseventdate,getdate()) <= 24
		 
		GROUP BY GPS.gpseventID,
	   GPS.gpseventdate,
	   R.resourceID,
	   C.ContractID,
	   TD.deviceID,
	   GPS.guid,
	   --RA.roundinstanceID,
	   TD.lat,
	   TD.long

),CTE_Tasklines as (
select t.taskID, tl.tasklineID, tl.gpseventguid, tl.completeddate, tl.autoconfirmed, T.lat, T.long


from tasklines tl
INNER JOIN Tasks T on T.taskid = tl.taskid
WHERE tl.gpseventguid in (SELECT distinct guid from CTE_Lifts)--DATEDIFF(day,T.taskscheduleddate,getdate()) = 0
),CTE_Tasks as (
select t.taskID, t.gpseventguid, T.autoconfirmed, T.lat, T.long

--into #tasks
from tasks T
WHERE gpseventguid in (SELECT distinct guid from CTE_Lifts)--convert(date,T.taskscheduleddate) >= '2021-06-18 00:00:00.000' AND DATEDIFF(day,T.taskscheduleddate,getdate()) = 0
),CTE_Inspections as (
select I.inspectionID, I.inspectioncreateddate, I.sourceobjectdesc, GI.* 
--into #inspections
from inspections I
left join gpseventinspections GI on gi.targetechoID = I.inspectionID
where GI.sourceGUID in (SELECT distinct guid from CTE_Lifts)--CONVERT(DATE,inspectioncreateddate) >= '2021-06-18 00:00:00.000' AND DATEDIFF(day,inspectioncreateddate,getdate()) = 0
)
,CTE_Combined as (
SELECT L.gpseventID, CONVERT(date,L.gpseventdate) as Day, L.guid, L.contractID, L.resourceID, L.deviceID, RA.roundinstanceID, L.lat, L.long, T.taskID, TL.tasklineID, I.inspectionID, 
 
CASE WHEN t.taskID IS NOT NULL AND tl.tasklineID IS NULL THEN 'Task' WHEN TL.tasklineID IS NOT NULL THEN 'Taskline' WHEN I.inspectionID IS NOT NULL THEN 'Inspection' ELSE 'Unmatched' END as LiftStatus, CASE WHEN T.taskID IS NULL AND tl.tasklineID IS NULL AND I.inspectionID IS NULL THEN 0 ELSE 1 END AS Matched, 
CASE WHEN t.taskID IS NOT NULL AND tl.tasklineID IS NULL THEN T.autoconfirmed WHEN TL.tasklineID IS NOT NULL THEN TL.autoconfirmed ELSE NULL END as Autoconfirmation,
CASE	WHEN t.taskID IS NOT NULL AND tl.tasklineID IS NULL THEN GEOGRAPHY::Point(L.lat, L.long, 4326).STDistance(GEOGRAPHY::Point(T.lat, T.long, 4326))
		WHEN TL.tasklineID IS NOT NULL THEN GEOGRAPHY::Point(L.lat, L.long, 4326).STDistance(GEOGRAPHY::Point(TL.lat, TL.long, 4326)) ELSE NULL END as LiftProximity,
tl.completeddate as TLcompleteddate, I.inspectioncreateddate, CASE WHEN CASE WHEN t.taskID IS NOT NULL AND tl.tasklineID IS NULL THEN T.autoconfirmed WHEN TL.tasklineID IS NOT NULL THEN TL.autoconfirmed ELSE NULL END = 3 THEN 1 ELSE 0 END as AutoMatched

FROM CTE_Lifts L
LEFT JOIN CTE_Tasks T on T.gpseventguid = L.guid
LEFT JOIN CTE_Tasklines TL on TL.gpseventguid = L.guid
LEFT JOIN CTE_Inspections I on I.sourceGUID = L.guid
left join resourceallocations RA on RA.resourceID = L.resourceID and convert(date,RA.startdate) = convert(date,[Lifter Date] ,103) and convert(datetime,concat(convert(date,[Lifter Date] ,103),' ',[Lifter Time] )) between RA.startdate and RA.enddate
)

SELECT *,
CASE WHEN Autoconfirmation = 1 THEN 'Proximity' WHEN Autoconfirmation = 2 THEN 'RFID' WHEN Autoconfirmation = 3 THEN 'Autoconfirmed' WHEN Autoconfirmation = 4 THEN 'GPS Event' WHEN Autoconfirmation IS NULL THEN 'Unmatched' END as ConfirmationMethod
,getDate() as TimeDataExtracted

FROM CTE_Combined

ORDER BY 2 desc


