select 
	T.taskID,
	T.tasktypeID,
	TT.tasktype,
	T.echotypeID,
	T.task,
	T.Tasknotes,
	T.Taskcreateddate,
	T.taskscheduleddate,
	T.taskduedate,
	T.taskcompleteddate,
	T.serviceunitID,
	T.roundinstanceID,
	T.TaskstateID,
	TS.taskstate,
	T.resolutioncodeID,
	RC.resolutioncode,
	T.servicetaskID,
	T.servicetaskscheduleID,
	T.agreementID,
	T.lastupdated
	
	from tasks T

	INNER JOIN tasktypes TT on TT.tasktypeID = T.TasktypeID
	INNER JOIN TaskStates TS on TS.taskstateID = T.TaskStateID
	LEFT JOIN resolutioncodes RC on RC.resolutioncodeID = T.resolutioncodeID


