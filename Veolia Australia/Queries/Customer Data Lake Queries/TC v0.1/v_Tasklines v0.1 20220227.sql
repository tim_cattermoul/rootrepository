select 
	TL.tasklineID,
	363 as echotypeID,
	TL.tasklineID as echoID,
	TL.tasklinetypeID,
	TLT.tasklinetype,
	TL.taskID,
	TL.scheduledassetquantity,
	TL.scheduledproductquantity,
	TL.actualassetquantity,
	TL.actualproductquantity,
	TL.productID,
	P.product,
	TL.product_unitID,
	U.Unit,
	TL.completeddate,
	TL.tasklinestateID,
	TLS.tasklinestate,
	TL.resolutioncodeID,
	RC.resolutioncode,
	TL.parent_tasklineID,
	TL.isserialised,
	TL.autoconfirmed,
	TL.gpseventguid,
	TL.preferred_siteID,
	TL.siteproductID,
	TL.assettypeID,
	AT.assettype,
	AT.clientreference,
	AT.unitID,
	U2.unit as AssetUnit,
	AT.size


	from Tasklines TL

	LEFT JOIN products P on P.productID = TL.productID
	LEFT JOIN productunits PU on PU.productunitiD = TL.product_unitID
	LEFT JOIN units U on U.unitID = PU.unitID
	INNER JOIN tasklinestates TLS on TLS.tasklinestateID = TL.tasklinestateID
	LEFT JOIN resolutioncodes RC on RC.resolutioncodeID = TL.resolutioncodeID
	INNER JOIN tasklinetypes TLT on TLT.tasklinetypeID = TL.tasklinetypeID
	INNER JOIN assettypes AT on AT.assettypeID = TL.assettypeID
	LEFT JOIN Units U2 on U2.unitID = AT.unitID




