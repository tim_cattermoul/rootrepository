select 
	
	agreementlineID,
	AL.agreementlinetypeID,
	ALT.agreementlinetype,
	AL.startdate,
	AL.enddate,
	agreementID,
	serviceID,
	isadhoc,
	siteID,
	invoicescheduleID,
	invoice_siteID,
	invoice_contactID,
	billingrule
	
	from agreementlines AL

	INNER JOIN agreementlinetypes ALT on ALT.agreementlinetypeID = AL.agreementlinetypeID

	