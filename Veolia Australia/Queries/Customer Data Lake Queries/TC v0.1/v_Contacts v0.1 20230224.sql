select 
	C.contactID,
	C.title,
	C.firstname,
	C.lastname,
	C.position,
	C.telephone,
	C.mobile,
	C.email,
	C.partyID,
	C.startdate,
	C.enddate

from contacts C