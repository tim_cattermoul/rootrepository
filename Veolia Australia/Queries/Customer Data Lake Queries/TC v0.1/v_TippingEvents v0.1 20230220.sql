select DISTINCT

rie.roundinstanceeventID,
roundinstanceID,
rie.roundeventtypeID,
eventdatetime,
rie.eventstartdatetime,
rie.eventenddatetime,
location_lat,
location_lon,
resourceID,
ried.roundinstanceeventdataID,
ried.roundeventtypedatatypeID,
roundinstanceeventdata,
RIE.tasklineID

from roundinstanceevents RIE

LEFT JOIN roundinstanceeventdata RIED on RIED.roundinstanceeventID = RIE.roundinstanceeventID
LEFT JOIN roundeventtypedatatypes DT on DT.roundeventtypedatatypeID = RIED.roundeventtypedatatypeID

where RIE.roundeventtypeID = 3 -- Tipping ID in Red AUS = 3 - not the same ID across customer environments
and DT.datatypeID = 4 -- Net Weight