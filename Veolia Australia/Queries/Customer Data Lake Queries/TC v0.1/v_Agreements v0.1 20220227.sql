select 
	agreementID,
	A.agreementtypeID,
	AT.agreementtype,
	346 as echotypeID,
	agreementID as echoID,
	partyID,
	A.startdate,
	A.enddate,
	A.startdate,
	A.clientreference,
	A.paymentmethodID,
	P.paymentmethod,
	A.invoicescheduleID,
	[IS].invoiceschedule,
	A.agreementstate,
	EC.description as agreementstatedesc,
	A.contactID,
	A.siteID,
	A.invoice_contactID,
	A.invoice_siteID,
	A.accountingreference,
	--W.workorderreference
	A.onstop

	from agreements A

	INNER JOIN agreementtypes AT on AT.agreementtypeID = A.agreementtypeID
	LEFT JOIN echocodes EC on EC.code = A.agreementstate and EC.echocodefileID = 86
	INNER JOIN invoiceschedules [IS] on [IS].invoicescheduleID = A.invoicescheduleID
	INNER JOIN paymentmethods P on P.paymentmethodID = A.paymentmethodID
	--LEFT JOIN workorderrefference W on W.echoID = A.agreementID and W.echotypeID = 346


