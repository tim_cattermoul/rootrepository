select 
	RIE.roundinstanceeventID,
	RIE.roundeventtypeID,
	RIE.roundinstanceID,
	RIE.eventstartdatetime,
	RIE.eventenddatetime,
	location_lat,
	location_lon,
	resourceID

from roundinstanceevents RIE


where

RIE.roundeventtypeID in (5,8) -- Rest Break 1 Rest Break 2