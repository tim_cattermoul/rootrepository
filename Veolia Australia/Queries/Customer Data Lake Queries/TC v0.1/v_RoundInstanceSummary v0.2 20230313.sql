
/************************************************************************************************************************************************************************
* Purpose of this view is to provide the basic roundinstance details being the primary vehicle and driver allocated to the rounsinstance
* and the start and finish times.
* 
* Clarifications / Potential issues:
* 1. Clarification: will roundinstanceID always be unique?
* 2. Is it possinble driver subquery return if multiple resources allocated and/or resources added, removed etc
* 3. Is it possible vehicle subquery return if multiple resources allocated and/or resources added, removed etc
* 4. For consistencey should driver and vehicle subquery be both getting the allocated resource from resourceallocations
* 5. Using roundinstanceresources for drivers returns nulls
* 6. Suggest to include the clientreference for both vehicle and driver - this is a reference into Veolia AUS system and they may use it in profit centre determination
*  
************************************************************************************************************************************************************************/


IF OBJECT_ID('tempdb..#DriverAllocations') IS NOT NULL
    DROP TABLE #DriverAllocations

select DISTINCT
	RA.resourceallocationID,
	RA.roundinstanceID,
	RA.resourceID as resourceID_driver
INTO 
	#DriverAllocations
from 
	resourceallocations RA with (nolock) 
	join resourcetypes RT on RA.resourcetypeID = RT.resourcetypeID
	join roundinstances RI with (nolock) on RA.roundinstanceID = RI.roundinstanceID
WHERE 
	RA.resourcetypeID = 1 -- Driver
	/*and
	RI.contractID = 3
	and
	RI.ss_date between '2023-01-30 00:00:00' and '2023-02-12 23:59:59'*/;



IF OBJECT_ID('tempdb..#VehicleAllocations') IS NOT NULL
    DROP TABLE #VehicleAllocations


select DISTINCT
	RA.resourceallocationID,
	RA.roundinstanceID,
	RA.resourceID as resourceID_vehicle,	
	RA.startdate as startdate_vehicle,				---- BW: giving it more context
	RA.enddate as enddate_vehicle,					---- BW: giving it more context
	RA.actualstartdate as actualstartdate_vehicle,	---- BW: giving it more context - always null - never set in the Veolia Aus Comm instance
	RA.actualenddate as actualenddate_vehicle		---- BW: giving it more context - always null - never set in the Veolia Aus Comm instance
INTO 
	#VehicleAllocations
from 
	resourceallocations RA with (nolock) 
	join resourcetypes RT with (nolock) on RA.resourcetypeID = RT.resourcetypeID
	join roundinstances RI with (nolock) on RA.roundinstanceID = RI.roundinstanceID

WHERE 
	RT.resourceclassID = 2 -- Vehicle
	/*and
	RI.contractID = 3
	and
	RI.ss_date between '2023-01-30 00:00:00' and '2023-02-12 23:59:59'*/;


select DISTINCT 

	RI.roundinstanceID,
	RI.clientreference,
	RI.roundID,
	RS.roundstate,

	DA.resourceID_driver,

	VA.resourceID_vehicle,

	RI.ss_date as scheduled_date_ri,
	RI.startdate as startdate_ri,
	RI.finishdate as finishdate_ri,

	--VA.startdate_vehicle,
	--VA.enddate_vehicle,
	VA.actualstartdate_vehicle,		---- BW: always null - never set in the Veolia Aus Comm instance
	VA.actualenddate_vehicle		---- BW: always null - never set in the Veolia Aus Comm instance
from 
	RoundInstances RI with (nolock) 
	left join roundstates RS with (nolock) on RI.roundstateID = RS.roundstateID
	left join #VehicleAllocations VA on VA.roundinstanceID = RI.roundinstanceID
	left join #DriverAllocations DA on DA.roundinstanceID = RI.roundinstanceID

where
	RI.contractID = 3
	and
	RI.ss_date between '2023-01-30 00:00:00' and '2023-02-12 23:59:59';


