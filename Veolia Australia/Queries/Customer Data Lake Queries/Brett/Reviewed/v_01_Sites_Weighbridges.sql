use RedAZEcho2_Dev3;
--use RedAZEcho2_Int;
go

----select * from sitetypes;

/************************************************************************************************************************************************************************
* The purpose of this view is get the Weighbridge Sites only.
* A join is done through the regions---contracts---contractsites---sites relationship to resolve the contract and regions.
* 
* Important: There is pre-existing data in PRD environment that originated from SAP3 / PTS.  This can be identified based on the SITE clientreference value and UPRN.
*
*  - When originates from SAP3/PTS, the value is prefixed with Disposal-.  This prefix is added to the incoming value from SAP3/PTS by the VeoliaAusIntegrationService.
* 
*  - When originates from Salesforce the value is prefixed with Loc????? - TO BE DISCUSSED
* 
* Issues: In DEV3, it appears that the sitedata records has not been imported from RedAZEcho2_Int to RedAZEcho2_Dev3.  The sitedata, particularly sap_location_name
* has a purpose on existing data and OnBoard functionality.  It is all currently used in PRD.  
* 
************************************************************************************************************************************************************************/

/***** Use the this qry to get a list of the datatypes used in the sitedata records.  It returns: [LocationID],[SalesOrg]  which is used below *****/
--select
--	string_agg(quotename(datatype),',') 
--from
--(
--	select
--		distinct case
--			when STDT.sitetypedatatype is null then DT.datatype
--			when len(trim(STDT.sitetypedatatype)) = 0 then DT.datatype
--			else STDT.sitetypedatatype
--		end as datatype
--	from
--		sitetypes ST
--		join sitetypedatatypes STDT with (nolock) on ST.sitetypeID = STDT.sitetypeID
--		left join datatypes DT with (nolock) on STDT.datatypeID = DT.datatypeID
--	where
--		ST.sitetypeID = 3 ---- Weighbridge
--	order by
--		datatype offset 0 rows
--) t;



select
	sitetype,
	record_source,
	regionID,
	contractID,
	siteID,
	pointaddressID,
	region,
	contract,
	site,
	site_clientreference,
	sap_locationID,
	LocationID as sap_location_name, 
	SalesOrg as sap_sales_org,
	sourcedescription,
	lat,
	lon,
	sitestartdate,
	siteenddate
from
(
select
	STY.sitetype,
	case
		when S.clientreference like '%Disposal-%' then 'SAP3'
		else 'Unknown'
	end as record_source,
	R.regionID,
	C.contractID,
	S.siteID,
	PA.pointaddressID,
	--PA.uprn,
	R.region,
	C.contract,
	S.site,
	
	S.clientreference as site_clientreference,
	case when S.clientreference like '%Disposal-%' then trim(replace(S.clientreference, 'Disposal-', '')) else null end as sap_locationID,
	PA.sourcedescription,
	PA.lat,
	PA.lon,
	S.startdate as sitestartdate,
	S.enddate as siteenddate,
	case
		when STDT.sitetypedatatype is null then DT.datatype
		when len(trim(STDT.sitetypedatatype)) = 0 then DT.datatype
		else STDT.sitetypedatatype
	end as datatype,
	SD.sitedata
from
	regions R with (nolock)
	join contracts C with (nolock) on R.regionID = C.regionID
	join contractsites CS with (nolock) on C.contractID = CS.contractID
	join sites S with (nolock) on CS.siteID = S.siteID
	join sitetypes STY with (nolock) on S.sitetypeID = STY.sitetypeID
	join pointaddresses PA with (nolock) on S.pointID = PA.pointaddressID and S.pointtypeID = PA.pointtypeID
	join postcodes PC with (nolock) on PA.postcodeID = PC.postcodeID
	join posttowns PT with (nolock) on PC.posttownID = PT.posttownID
	join countries CN with (nolock) on PA.countryID = CN.countryID
	join streets ST with (nolock) on PA.primary_streetID = ST.streetID
	left join sitedata SD with (nolock) on S.siteID = SD.siteID
	left join sitetypedatatypes STDT with (nolock) on SD.sitetypedatatypeID = STDT.sitetypedatatypeID
	left join datatypes DT with (nolock) on STDT.datatypeID = DT.datatypeID
where
	S.sitetypeID = 3 ---- WeigIthbridge
) t
pivot
(
	max(sitedata) for datatype in ([LocationID], [SalesOrg])
) p
order by
	site


