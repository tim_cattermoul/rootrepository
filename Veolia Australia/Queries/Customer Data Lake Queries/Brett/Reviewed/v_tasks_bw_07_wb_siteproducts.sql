
/***********************************************************************************************************************************
* wb_siteproducts - view of weighbridge sites and their products.
* Important for tasklines and roundinstancevents where the driver is instructed to tip at a preferred site.
* This view can be related to tasklines_scheduled and preferred_siteID and siteproductID.
* 
***********************************************************************************************************************************/

select
	WBSP.siteproductID
	,WBSP.siteID
	,WBSP.siteproduct
	,WBSP.clientreference
	,WBSP.startdate
	,WBSP.enddate
from
	wb_siteproducts WBSP with (nolock)
	join products P with (nolock) on WBSP.productID = P.productID
