
/************************************************************************************************************************************************************************
* Inspections can have extended or additional data associated with them e.g. Engine Hours Start, KM Start
* This view provides the list of extended or additional data for inspections.
* 
************************************************************************************************************************************************************************/

select
	X.inspectiondataID
	,X.inspectionID
	,X.datatype
	,convert(nvarchar(max), isnull(LU.description, isnull(X.inspectiondata, ''))) as inspectiondata ---- THE VALUE USED IN REPORTING ETC ETC
	,LU.clientreference
from
(
select
	ID.inspectiondataID
	,ID.inspectionID
	,ITDT.[order] as sortorder
	,ID.parent_inspectiondataID
	,ID.inspectiontypedatatypeID
	,DT.datatypeID
	,CDT.coredatatype
	,case
		when ITDT.inspectiontypedatatype is null then DT.datatype
		when len(trim(ITDT.inspectiontypedatatype)) = 0 then DT.datatype
		else ITDT.inspectiontypedatatype
	end as datatype
	,isnull(DT.valuedomaintypeID, 0) as valuedomaintypeID
	,isnull(DT.valuedomain, '0') as valuedomain
	,case 
		when CDT.coredatatype <> 'boolean' then ID.inspectiondata
		else 
			case
				when ID.inspectiondata = '0' then 'No'
				when ID.inspectiondata = '1' then 'Yes'
				else ID.inspectiondata
			end
		end
	as inspectiondata


from
	inspectiondata ID with (nolock)
	join inspectiontypedatatypes ITDT with (nolock) on ID.inspectiontypedatatypeID = ITDT.inspectiontypedatatypeID
	join datatypes DT with (nolock) on ITDT.datatypeID = DT.datatypeID
	join coredatatypes CDT with (nolock) on DT.coredatatypeID = CDT.coredatatypeID
) X
	left join TemporaryStore..X_v_master_datattype_lookup_values LU on
		X.datatypeID = LU.datatypeID 
		and 
		X.valuedomaintypeID = LU.valuedomaintypeID 
		and 
		X.valuedomain = LU.valuedomain 
		and 
		X.inspectiondata = isnull(LU.code, X.inspectiondata)



