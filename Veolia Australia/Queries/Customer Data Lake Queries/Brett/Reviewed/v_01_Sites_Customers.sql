use RedAZEcho2_Dev3;
--use RedAZEcho2_Int;  ---- UAT db for the as-is SAP3/PTS - Echo integration.  This will continue even after go-live.
go

----select * from sitetypes;

/****************************************************************************************************************************************************************************************************************
* The purpose of this view is get the Customer Service Site and Invoice Site only.
* A join is done through the site---serviceunit---services relationship to resolve the contract and regions.
* 
* Important: There is pre-existing data in PRD environment that originated from SAP3 / PTS.  This can be identified based on the clientreference value.
*
*  - When originates from SAP3/PTS, the value is prefixed with Location-.  This prefix is added to the incoming value from SAP3/PTS by the VeoliaAusIntegrationService and used as site.clientreference.  
*    use RedAZEcho2_Int for examples.
* 
*  - When originates from Salesforce the value is prefixed with Loc
* 
* Issues: 
* 1. In DEV3, if pointaddress is created via API, it appears that the postcodeID value for pointaddresses is not being set - majority are null
* 
***************************************************************************************************************************************************************************************************************/

select distinct 
	STY.sitetype,
	case
		when SI.clientreference like '%Location-%' then 'SAP3'
		when SI.clientreference like '%Loc%' then 'Salesforce'
		else 'Unknown'
	end as record_source,
	SI.clientreference as site_clientreference,
	R.regionID,
	C.contractID,
	SI.siteID,
	PA.pointaddressID,
	
	R.region,
	C.contract,
	SI.site,
	
	case when SI.clientreference like '%Location-%' then trim(replace(SI.clientreference, 'Location-', '')) else null end as sap_customer_locationID,
	PA.uprn,
	PA.sourcedescription,
	ST.Street,
	PT.posttown as locality,
	PC.postcode,
	CN.country,
	PA.lat,
	PA.lon,
	S.startdate as sitestartdate,
	S.enddate as siteenddate
from
	regions R with (nolock)
	join contracts C with (nolock) on R.regionID = C.regionID
	join servicegroups SG with (nolock) on C.contractID = SG.contractID
	join services S with (nolock) on SG.servicegroupID = S.servicegroupID
	join serviceunits SU with (nolock) on S.serviceID = SU.serviceID
	join sites SI with (nolock) on SU.siteID = SI.siteID
	join sitetypes STY with (nolock) on SI.sitetypeID = STY.sitetypeID
	join pointaddresses PA with (nolock) on SI.pointID = PA.pointaddressID and SI.pointtypeID = PA.pointtypeID
	left join postcodes PC with (nolock) on PA.postcodeID = PC.postcodeID
	left join posttowns PT with (nolock) on PC.posttownID = PT.posttownID
	join countries CN with (nolock) on PA.countryID = CN.countryID
	left join streets ST with (nolock) on PA.primary_streetID = ST.streetID
where
	SI.sitetypeID in (2, 4); ---- 2 = Customer Service Site;  4 = Invoice Site


