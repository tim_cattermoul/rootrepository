

/************************************************************************************************************************************************************************
* Lists the events that have occurred for a roundinstance.  
* 
************************************************************************************************************************************************************************/

select distinct
	RE.roundinstanceeventID
	,RE.roundinstanceID
	,RE.resourceID
	,RE.tasklineID
	,RE.tasklineID
	,RET.roundeventtype
	,RET.roundeventtypeID
	,RE.eventstartdatetime
	,RE.eventenddatetime
into
	TemporaryStore..X_v_roundinstance_events
from
	roundinstanceevents RE with (nolock)
	join roundeventtypes RET with (nolock) on RE.roundeventtypeID = RET.roundeventtypeID






