use RedAZEcho2_Dev3;
--use RedAZEcho2_Int;
go

----select * from sitetypes;

/************************************************************************************************************************************************************************
* The purpose of this view is get the Depot Sites only.
* A join is done through the regions---contracts---contractsites---sites relationship to resolve the contract and regions.
* 
* Important: There is pre-existing data in PRD environment that originated from SAP3 / PTS.  This can be identified based on the SITE clientreference value and UPRN.
*
*  - When originates from SAP3/PTS, the value is prefixed with Plant-.  This prefix is added to the incoming value from SAP3/PTS by the VeoliaAusIntegrationService.
* 
*  - When originates from Salesforce the value is prefixed with Loc
* 
* Issues: In DEV3, it appears that the postcodeID value for pointaddresses is not being set - majority are null
* 
************************************************************************************************************************************************************************/

select
	STY.sitetype,
	case
		when S.clientreference like '%Plant-%' then 'SAP3'
		else 'Unknown'
	end as record_source,
	R.regionID,
	C.contractID,
	S.siteID,
	PA.pointaddressID,
	--PA.uprn,
	R.region,
	C.contract,
	S.site,
	S.clientreference as site_clientreference,
	case when S.clientreference like '%Plant-%' then trim(replace(S.clientreference, 'Plant-', '')) else null end as sap_plantID,
	PA.sourcedescription,
	PA.lat,
	PA.lon,
	S.startdate as sitestartdate,
	S.enddate as siteenddate
from
	regions R with (nolock)
	join contracts C with (nolock) on R.regionID = C.regionID
	join contractsites CS with (nolock) on C.contractID = CS.contractID
	join sites S with (nolock) on CS.siteID = S.siteID
	join sitetypes STY with (nolock) on S.sitetypeID = STY.sitetypeID
	join pointaddresses PA with (nolock) on S.pointID = PA.pointaddressID and S.pointtypeID = PA.pointtypeID
	--join postcodes PC with (nolock) on PA.postcodeID = PC.postcodeID
	--join posttowns PT with (nolock) on PC.posttownID = PT.posttownID
	--join countries CN with (nolock) on PA.countryID = CN.countryID
	--join streets ST with (nolock) on PA.primary_streetID = ST.streetID
where
	S.sitetypeID = 1; ---- Depot

