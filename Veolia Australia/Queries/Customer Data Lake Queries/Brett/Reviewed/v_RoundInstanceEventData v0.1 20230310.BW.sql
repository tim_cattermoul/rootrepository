

/************************************************************************************************************************************************************************
* Roundinstanceevents can have extended or additional data associated with them e.g. Disposal, Tipping, Fuel
* This view provides the list of extended or additional data for roundinstancevents.
* 
************************************************************************************************************************************************************************/

select distinct
	X.roundinstanceeventdataID
	,X.roundinstanceeventID
	,X.datatype
	,convert(nvarchar(max), isnull(LU.description, isnull(X.roundinstanceeventdata, ''))) as roundinstanceeventdata ---- THE VALUE USED IN REPORTING ETC ETC
	,LU.clientreference
from
(
select 
	RIED.roundinstanceeventdataID
	,RIED.roundinstanceeventID
	,RIED.parent_roundinstanceeventdataID
	,RIED.roundeventtypedatatypeID
	,DT.datatypeID
	,CDT.coredatatype
	,case
		when RETDT.roundeventtypedatatype is null then DT.datatype
		when len(trim(RETDT.roundeventtypedatatype)) = 0 then DT.datatype
		else RETDT.roundeventtypedatatype
	end as datatype
	,isnull(DT.valuedomaintypeID, 0) as valuedomaintypeID
	,isnull(DT.valuedomain, '0') as valuedomain
	,case 
		when CDT.coredatatype <> 'boolean' then RIED.roundinstanceeventdata
		else 
			case
				when RIED.roundinstanceeventdata = '0' then 'No'
				when RIED.roundinstanceeventdata = '1' then 'Yes'
				else RIED.roundinstanceeventdata
			end
		end
	as roundinstanceeventdata
from
	roundinstanceeventdata RIED with (nolock)
	join roundeventtypedatatypes RETDT with (nolock) on RIED.roundeventtypedatatypeID = RETDT.roundeventtypedatatypeID
	join datatypes DT with (nolock) on RETDT.datatypeID = DT.datatypeID
	join coredatatypes CDT with (nolock) on DT.coredatatypeID = CDT.coredatatypeID
) X
	left join TemporaryStore..X_v_master_datattype_lookup_values LU on
		X.datatypeID = LU.datatypeID 
		and 
		X.valuedomaintypeID = LU.valuedomaintypeID 
		and 
		X.valuedomain = LU.valuedomain 
		and 
		X.roundinstanceeventdata = isnull(LU.code, X.roundinstanceeventdata);


