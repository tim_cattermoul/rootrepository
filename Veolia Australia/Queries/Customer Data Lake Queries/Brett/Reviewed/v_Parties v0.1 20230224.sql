select 
	P.partyID,
	P.Party,
	P.clientreference,
	P.startdate as PartyStartDate,
	P.enddate as PartyEnddate,
	P.correspondence_siteID,
	P.invoice_siteID,
	P.isinternal,
	P.primary_contactID,
	P.accounttypeID,
	AT.accounttype
	--,P.* 
from 
parties P

LEFT JOIN accounttypes AT on AT.accounttypeID = P.accounttypeID