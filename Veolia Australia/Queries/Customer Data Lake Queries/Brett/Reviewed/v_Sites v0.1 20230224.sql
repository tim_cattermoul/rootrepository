select DISTINCT
	S.siteID,
	S.site,
	ST.sitetype,
	s.pointID,
	s.startdate as sitestartdate,
	s.enddate as siteenddate,
	PA.pointaddressID,
	PA.sourcedescription,
	PA.property,
	SE.Street,
	SE.locality,
	PC.postcode,
	PA.UPRN,
	PA.lat,
	PA.lon,
	CN.country,
	PS.sectorID,
	SC.sector,
	SCT.sectortype,
	SC.startdate,
	SC.enddate,
	S.partyID

from sites S

INNER JOIN sitetypes ST on ST.sitetypeID = S.sitetypeID
INNER JOIN pointaddresses PA on PA.pointaddressID = S.pointID and S.pointtypeID = 1
LEFT JOIN postcodes PC on PC.postcodeID = PA.postcodeID
INNER JOIN pointsectors PS on PS.pointID = S.pointID and S.pointtypeID = 1
INNER JOIN sectors SC on SC.sectorID = PS.sectorID and SC.sectortypeID = 1 -- Contract Area
INNER JOIN sectortypes SCT on SCT.sectortypeID = SC.sectortypeID
INNER JOIN streets SE on SE.streetID = PA.primary_streetID
LEFT JOIN countries CN on CN.countryID = PA.countryID

