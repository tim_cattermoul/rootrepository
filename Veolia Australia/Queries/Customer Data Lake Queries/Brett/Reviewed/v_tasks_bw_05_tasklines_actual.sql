
/***********************************************************************************************************************************
* tasklines_actual - what was actually done.  Operations always wants to compare actual vs scheduled.
* In Echo tasklines alone cannot explicitly indicate what was scheduled vs actual.
* In some records the task is completed, but tasklines still in a state of Allocated.
* Also can have situation where driver services some bins and futiles others.
* This view attempts to pull the data togethor through a series of unions:
*	1. (TL.parent_tasklineID is null or TL.parent_tasklineID = 0) and TS.coretaskstateID in (3, 4, 7) ---- Closed, Cancelled, Failed and TLS.coretaskstateID <> 7
*   2. TL.parent_tasklineID > 0 and TS.coretaskstateID in (3, 4, 7) ---- Closed, Cancelled, Failed and TLS.coretaskstateID <> 7 
*   3. TS.coretaskstateID in (3, 4, 7) ---- Closed, Cancelled, Failed and TLS.coretaskstateID = 7 ---- FAILED aka Futile
* 
* These 3 tasks provide a glimpse of the challenges:
* taskID: 6281065 - contractID: 3 - Sydney/Illawarra/Hunter
* taskID: 6327980 - contractID: 3 - Sydney/Illawarra/Hunter
* taskID: 6328222 - contractID: 3 - Sydney/Illawarra/Hunter
* taskID: 6377541 - contractID: 3 - Sydney/Illawarra/Hunter
* taskID: 6337255 - contractID: 3 - Sydney/Illawarra/Hunter
* taskID: 6341140 - contractID: 1 - South East Queensland
* taskID: 6341224 - contractID: 1 - South East Queensland
* 
***********************************************************************************************************************************/

if object_ID('tempdb..#tasklines_actual') is not null drop table #tasklines_actual;

select
	t1.taskID
	,max(t1.tasklineID) as tasklineID
	,t1.parent_tasklineID
	,sum(t1.actualassetquantity) as actualassetquantity
	,sum(t1.actualproductquantity) as actualproductquantity
	,sum(case when t1.adjustedproductquantity = 0 then t1.actualproductquantity else t1.adjustedproductquantity end) as adjustedproductquantity
	,t1.tasklinestate
	,t1.resolutioncode
into
	#tasklines_actual
from
(
select
	TL.taskID
	,TL.tasklineID
	,case 
		when TL.parent_tasklineID is null then TL.tasklineID
		when TL.parent_tasklineID > 0 then TL.parent_tasklineID 
		else TL.tasklineID 
	end as parent_tasklineID
	,isnull(TL.actualassetquantity, 0) as actualassetquantity
	,isnull(TL.actualproductquantity, 0) as actualproductquantity
	,isnull(TL.adjustedproductquantity, 0) as adjustedproductquantity
	,TS.taskstate as tasklinestate
	,RC.resolutioncode
from
	tasks T with (nolock)
	join tasklines TL with (nolock) on T.taskID = TL.taskID
	join tasklinestates TLS  with (nolock) on TL.tasklinestateID = TLS.tasklinestateID
	left join resolutioncodes RC with (nolock) on TL.resolutioncodeID = RC.resolutioncodeID
	join taskstates TS with (nolock) on T.taskstateID = TS.taskstateID
where
	(TL.parent_tasklineID is null or TL.parent_tasklineID = 0)
	and
	TS.coretaskstateID in (3, 4, 7) ---- Closed, Cancelled, Failed
	and
	TLS.coretaskstateID <> 7
	--and
	--T.taskID  in (6341140,  6341224, 6327980)
union
select
	TL.taskID
	,TL.tasklineID
	,case when TL.parent_tasklineID > 0 then TL.parent_tasklineID else TL.tasklineID end as parent_tasklineID
	,isnull(TL.actualassetquantity, 0) as actualassetquantity
	,isnull(TL.actualproductquantity, 0) as actualproductquantity
	,isnull(TL.adjustedproductquantity, 0) as adjustedproductquantity
	,TS.taskstate as tasklinestate
	,RC.resolutioncode
from
	tasks T with (nolock)
	join tasklines TL with (nolock) on T.taskID = TL.taskID
	join tasklinestates TLS  with (nolock) on TL.tasklinestateID = TLS.tasklinestateID
	left join resolutioncodes RC with (nolock) on TL.resolutioncodeID = RC.resolutioncodeID
	join taskstates TS with (nolock) on T.taskstateID = TS.taskstateID

where
	TL.parent_tasklineID > 0
	and
	TS.coretaskstateID in (3, 4, 7) ---- Closed, Cancelled, Failed
	and
	TLS.coretaskstateID <> 7
	--and
	--T.taskID  in (6341140,  6341224, 6327980)
union
select
	TL.taskID
	,TL.tasklineID
	,case when TL.parent_tasklineID > 0 then TL.parent_tasklineID else TL.tasklineID end as parent_tasklineID
	,case when TL.actualassetquantity > 0 then TL.actualassetquantity else TL.scheduledassetquantity end as actualassetquantity
	,isnull(TL.actualproductquantity, 0) as actualproductquantity
	,isnull(TL.adjustedproductquantity, 0) as adjustedproductquantity
	,TLS.tasklinestate as tasklinestate
	,RC.resolutioncode
from
	tasks T with (nolock)
	join tasklines TL with (nolock) on T.taskID = TL.taskID
	join tasklinestates TLS  with (nolock) on TL.tasklinestateID = TLS.tasklinestateID
	left join resolutioncodes RC with (nolock) on TL.resolutioncodeID = RC.resolutioncodeID
	join taskstates TS with (nolock) on T.taskstateID = TS.taskstateID

where
	TS.coretaskstateID in (3, 4, 7) ---- Closed, Cancelled, Failed
	and
	TLS.coretaskstateID = 7 ---- FAILED aka Futile
	--and
	--T.taskID  in (6341140,  6341224, 6327980)
) t1
group by
	t1.taskID
	,t1.parent_tasklineID
	,t1.tasklinestate
	,t1.resolutioncode;



select
	t2.tasklineID
	,t2.taskID
	,t2.parent_tasklineID
	,t2.actualassetquantity
	,t2.actualproductquantity
	,t2.adjustedproductquantity
	,t2.tasklinestate
	,t2.resolutioncode
	,max(TLA.tasklineactioncreateddate) as tasklineactiondate
from
	#tasklines_actual t2
	join tasklineactions TLA on t2.tasklineID = TLA.tasklineID
group by
	t2.taskID
	,t2.tasklineID
	,t2.parent_tasklineID
	,t2.actualassetquantity
	,t2.actualproductquantity
	,t2.adjustedproductquantity
	,t2.tasklinestate
	,t2.resolutioncode
order by
	taskID
	,tasklineID;













--select
--	t2.taskID
--	,t2.tasklineID
--	,t2.parent_tasklineID
--	,t2.actualassetquantity
--	,t2.actualproductquantity
--	,t2.adjustedproductquantity
--	,t2.tasklinestate
--	,t2.resolutioncode
--	,max(TLA.tasklineactioncreateddate) as tasklineactiondate
--into
--	TemporaryStore..X_v_tasklines_actual
--from
--(
	--select
	--	t1.taskID
	--	,max(t1.tasklineID) as tasklineID
	--	,t1.parent_tasklineID
	--	,sum(t1.actualassetquantity) as actualassetquantity
	--	,sum(t1.actualproductquantity) as actualproductquantity
	--	,sum(case when t1.adjustedproductquantity = 0 then t1.actualproductquantity else t1.adjustedproductquantity end) as adjustedproductquantity
	--	,t1.tasklinestate
	--	,t1.resolutioncode
	--from
	--(
	--select
	--	TL.taskID
	--	,TL.tasklineID
	--	,case when TL.parent_tasklineID > 0 then TL.parent_tasklineID else TL.tasklineID end as parent_tasklineID
	--	,isnull(TL.actualassetquantity, 0) as actualassetquantity
	--	,isnull(TL.actualproductquantity, 0) as actualproductquantity
	--	,isnull(TL.adjustedproductquantity, 0) as adjustedproductquantity
	--	,TS.taskstate as tasklinestate
	--	,RC.resolutioncode
	--from
	--	tasks T with (nolock)
	--	join tasklines TL with (nolock) on T.taskID = TL.taskID
	--	join tasklinestates TLS  with (nolock) on TL.tasklinestateID = TLS.tasklinestateID
	--	left join resolutioncodes RC with (nolock) on TL.resolutioncodeID = RC.resolutioncodeID
	--	join taskstates TS with (nolock) on T.taskstateID = TS.taskstateID

	--	join TemporaryStore..X_v_tasks x on T.taskID = x.taskID ----------------------------------------------------

	--where
	--	TL.parent_tasklineID = 0
	--	and
	--	TS.coretaskstateID in (3, 4, 7) ---- Closed, Cancelled, Failed
	--	and
	--	TLS.coretaskstateID <> 7
	--	--and
	--	--T.taskID  in (6341140,  6341224, 6327980)
	--union
	--select
	--	TL.taskID
	--	,TL.tasklineID
	--	,case when TL.parent_tasklineID > 0 then TL.parent_tasklineID else TL.tasklineID end as parent_tasklineID
	--	,isnull(TL.actualassetquantity, 0) as actualassetquantity
	--	,isnull(TL.actualproductquantity, 0) as actualproductquantity
	--	,isnull(TL.adjustedproductquantity, 0) as adjustedproductquantity
	--	,TS.taskstate as tasklinestate
	--	,RC.resolutioncode
	--from
	--	tasks T with (nolock)
	--	join tasklines TL with (nolock) on T.taskID = TL.taskID
	--	join tasklinestates TLS  with (nolock) on TL.tasklinestateID = TLS.tasklinestateID
	--	left join resolutioncodes RC with (nolock) on TL.resolutioncodeID = RC.resolutioncodeID
	--	join taskstates TS with (nolock) on T.taskstateID = TS.taskstateID

	--	join TemporaryStore..X_v_tasks x on T.taskID = x.taskID ----------------------------------------------------

	--where
	--	TL.parent_tasklineID > 0
	--	and
	--	TS.coretaskstateID in (3, 4, 7) ---- Closed, Cancelled, Failed
	--	and
	--	TLS.coretaskstateID <> 7
	--	--and
	--	--T.taskID  in (6341140,  6341224, 6327980)
	--union
	--select
	--	TL.taskID
	--	,TL.tasklineID
	--	,case when TL.parent_tasklineID > 0 then TL.parent_tasklineID else TL.tasklineID end as parent_tasklineID
	--	,case when TL.actualassetquantity > 0 then TL.actualassetquantity else TL.scheduledassetquantity end as actualassetquantity
	--	,isnull(TL.actualproductquantity, 0) as actualproductquantity
	--	,isnull(TL.adjustedproductquantity, 0) as adjustedproductquantity
	--	,TLS.tasklinestate as tasklinestate
	--	,RC.resolutioncode
	--from
	--	tasks T with (nolock)
	--	join tasklines TL with (nolock) on T.taskID = TL.taskID
	--	join tasklinestates TLS  with (nolock) on TL.tasklinestateID = TLS.tasklinestateID
	--	left join resolutioncodes RC with (nolock) on TL.resolutioncodeID = RC.resolutioncodeID
	--	join taskstates TS with (nolock) on T.taskstateID = TS.taskstateID

	--	join TemporaryStore..X_v_tasks x on T.taskID = x.taskID ----------------------------------------------------

	--where
	--	TS.coretaskstateID in (3, 4, 7) ---- Closed, Cancelled, Failed
	--	and
	--	TLS.coretaskstateID = 7 ---- FAILED aka Futile
	--	--and
	--	--T.taskID  in (6341140,  6341224, 6327980)
	--) t1
	--group by
	--	t1.taskID
	--	,t1.parent_tasklineID
	--	,t1.tasklinestate
	--	,t1.resolutioncode
--) t2
--join tasklineactions TLA on t2.tasklineID = TLA.tasklineID
--group by
--	t2.taskID
--	,t2.tasklineID
--	,t2.parent_tasklineID
--	,t2.actualassetquantity
--	,t2.actualproductquantity
--	,t2.adjustedproductquantity
--	,t2.tasklinestate
--	,t2.resolutioncode
--order by
--	taskID
--	,tasklineID