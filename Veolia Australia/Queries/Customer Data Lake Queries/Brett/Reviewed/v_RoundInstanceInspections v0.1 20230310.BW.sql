
/************************************************************************************************************************************************************************
* Lists the inspections that have occurred for a roundinstance.  
* 
************************************************************************************************************************************************************************/

select
	I.inspectionID
	,I.sourceechoID as roundinstanceID
	,I.echoID as resourceID
	,IT.inspectiontype
	,I.inspectiontypeID
	,I.inspectiondate as startdate
	,I.completiondate
from
	inspections I with (nolock)
	join inspectiontypes IT with (nolock) on I.inspectiontypeID = IT.inspectiontypeID
where
	I.sourceechotypeID = 135	-- roundinstance
	and				
	I.echotypeID = 54			-- resource












