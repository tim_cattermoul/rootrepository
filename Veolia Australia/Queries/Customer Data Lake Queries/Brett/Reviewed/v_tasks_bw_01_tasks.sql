
/******************************************************************************************************
* Lists the tasks
* 
* 
* 
******************************************************************************************************/


select distinct
	T.taskID,
	T.taskreference,
	T.clientreference as task_clientreference,
	ET.echotypename as echotype,
	T.echoID,
	T.roundID,
	T.roundinstanceID,
	T.scheduledroundinstanceID,

	TT.tasktype,
	TT.clientreference as tasktype_clientreference,
	TS.taskstate,
	RC.resolutioncode,
	P.priority,
	T.task,
	T.tasknotes,
	T.taskcreateddate,
	T.taskscheduleddate,
	T.taskduedate,
	T.taskcompleteddate,
	T.taskenddate,
	T.contractID,
	T.partyID,
	T.agreementID,
	T.serviceunitID,
	SU.siteID,
	T.servicetaskID,
	T.servicetaskscheduleID,
	T.echotypeID,
	T.tasktypeID,
	T.taskstateID,
	T.resolutioncodeID,
	T.lastupdated
from
	tasks T with (nolock)
	join echotypes ET with (nolock) on T.echotypeID = ET.echotypeID
	join tasktypes TT with (nolock) on TT.tasktypeID = T.TasktypeID
	join taskstates TS with (nolock) on TS.taskstateID = T.TaskStateID
	left join resolutioncodes RC with (nolock) on RC.resolutioncodeID = T.resolutioncodeID
	left join priorities P with (nolock) on T.priorityID = P.priorityID
	join serviceunits SU with (nolock) on T.serviceunitID = SU.serviceunitID



