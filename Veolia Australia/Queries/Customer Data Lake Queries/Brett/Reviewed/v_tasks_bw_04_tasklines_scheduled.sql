
/***********************************************************************************************************************
* tasklines_scheduled - the "scheduled" tasklines.  Operations always wants to compare actual vs scheduled.
* In Echo tasklines alone cannot explicitly indicate what was scheduled vs actual.
* This view is purely for scheduled based on tasklineactions where:
* tasklineactioncreatedbyuserID = 0 and actiontype = 'Create'
* 
* For SAP3/PTS jobs, the clientreference of the deliver type is NOT being populated in the integration - we need this
* to be in the view.  As such, first get data into a temp table where we make sure it is set.
* 
***********************************************************************************************************************/


if object_ID('tempdb..#tasklines_clientref') is not null drop table #tasklines_clientref;


with cte_tasklines_clientref as 
(
	select
		TL.taskID
		,TL.tasklineID
		,row_number() over (partition by TL.taskID order by TL.clientreference desc) as row_id
		,TLTYP.tasklinetype
		,TL.clientreference
	from
		tasklines TL with (nolock) 
		join tasklineactions TLA with (nolock) on TL.tasklineID = TLA.tasklineID
		join tasklinetypes TLTYP with (nolock) on TLA.tasklinetypeID = TLTYP.tasklinetypeID

	where
		TLA.tasklineactioncreatedbyuserID = 0
		and
		TLA.actiontype = 'Create'
)
select
	taskID
	,tasklineID
	,row_id
	,tasklinetype
	,first_value(clientreference) over (partition by taskID order by row_id asc) as clientreference
into
	#tasklines_clientref
from 
	cte_tasklines_clientref



select distinct
	TL.tasklineID
	,T.taskID
	,X.clientreference
	,TLA.tasklineactionID
	,TLTYP.tasklinetype
	,P.product
	,ACLASS.assetclass
	,ATYPE.assettype
	,ATYPE.clientreference as asset_clientreference
	,isnull(ATYPE.size, 0) as asset_volume
	,TLA.scheduledassetquantity
	,TLA.scheduledproductquantity
	,TL.preferred_siteID
	,TL.siteproductID
	,TLA.tasklineactioncreateddate as tasklineactiondate
from
	tasks T with (nolock)
	join tasklines TL with (nolock) on T.taskID = TL.taskID
	join tasklineactions TLA with (nolock) on TL.tasklineID = TLA.tasklineID
	join tasklinetypes TLTYP with (nolock) on TLA.tasklinetypeID = TLTYP.tasklinetypeID
	join products P with (nolock) on TLA.productID = P.productID
	join assettypes ATYPE with (nolock) on TLA.assettypeID = ATYPE.assettypeID
	join assetclasses ACLASS with (nolock) on ATYPE.assetclassID = ACLASS.assetclassID
	left join units U with (nolock) on ATYPE.unitID = U.unitID
	left join tasklinedata TLD with (nolock) on TLA.tasklineID = TLD.tasklineID
	left join tasklinetypedatatypes TLTDT with (nolock) on TLD.tasklinetypedatatypeID = TLTDT.tasklinetypedatatypeID
	left join datatypes DT with (nolock) on TLTDT.datatypeID = DT.datatypeID

	join #tasklines_clientref X on TL.tasklineID = X.tasklineID

where

	TLA.tasklineactioncreatedbyuserID = 0
	and
	TLA.actiontype = 'Create'



