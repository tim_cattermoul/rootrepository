


/******************************************************************************************************
* customer_sites - lists the customer sites.
* View of customer sites only - sitetypeID = 2  ---- Customer Service Site
* 
* 
******************************************************************************************************/


select distinct
	S.siteID,
	P.partyID,
	S.clientreference as site_clientreference,
	P.clientreference as party_clientreference,
	STY.sitetype,
	S.site,
	R.regionID,
	C.contractID,
	PA.sourcedescription as site_address,
	cast(isnull(ST.locality, isnull(ST.townname, isnull(PT.posttown, ''))) as nvarchar(255)) as locality,
	PC.postcode,
	PA.lat,
	PA.lon,
	PA.pointaddressID,
	S.startdate,
	S.enddate
from
	sites S with (nolock)
	join sitetypes STY with (nolock) on S.sitetypeID = STY.sitetypeID
	left join pointaddresses PA with (nolock) on S.pointID = PA.pointaddressID and S.pointtypeID = PA.pointtypeID
	left join postcodes PC with (nolock) on PA.postcodeID = PC.postcodeID
	left join posttowns PT with (nolock) on PC.posttownID = PT.posttownID
	left join streets ST with (nolock) on PA.primary_streetID = ST.streetID
	left join parties P with (nolock) on S.partyID = P.partyID
	left join contracts C with (nolock) on P.defaultcontractID = C.contractID
	left join regions R with (nolock) on C.regionID = R.regionID
	
where
	S.sitetypeID = 2  ---- Customer Service Site






