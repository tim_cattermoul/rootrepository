IF OBJECT_ID('tempdb..#DriverAllocations') IS NOT NULL
    DROP TABLE #DriverAllocations

select DISTINCT
	RIR.roundinstanceresourceID,
	RIR.roundinstanceID,
	RIR.resourcetypeID,
	RT.resourcetype,
	RIR.roundresourceallocationID,
	RRA.resourceID,
	R.resource as Resource_Driver

	INTO #DriverAllocations
	from roundinstanceresources RIR

INNER JOIN resourcetypes RT on RT.resourcetypeID = RIR.resourcetypeID
LEFT JOIN roundresourceallocations RRA on RRA.roundresourceallocationID = RIR.roundresourceallocationID
LEFT JOIN resources R on R.resourceID = RRA.resourceID

WHERE RT.resourcetypeID = 1 -- Driver

IF OBJECT_ID('tempdb..#VehicleAllocations') IS NOT NULL
    DROP TABLE #VehicleAllocations

	select DISTINCT
		RA.resourceallocationID,
		RA.roundinstanceID,
		RA.resourceID,
		R.resource as Resource_vehicle,
		RD.resourcedata as FleetNo,
		RA.resourcetypeID,
		RT.resourceclassID,
		RT.resourcetype,
		RA.startdate,
		RA.enddate,
		RA.actualstartdate,
		RA.actualenddate
	
	INTO #VehicleAllocations
	from resourceallocations RA

	INNER JOIN resources R on R.resourceID = RA.resourceID
	INNER JOIN resourcetypes RT on RT.resourcetypeID = RA.resourcetypeID
	LEFT JOIN resourcedata RD on RD.resourceID = R.resourceID

	WHERE resourceclassID = 2 -- Vehicle
	AND (RD.resourcedatatypeID = 4 OR RD.resourcedatatypeID IS NULL) -- FleetObjectNo

select DISTINCT 

RG.businessunitID as Cost_centre, -- Optional, could act as an FK to SAP cost centre if elected to enter into ECHO
RI.roundinstanceID, 
ri.roundid,
r.round,
VA.resourceallocationID,
VA.resourceID,
VA.Resource_vehicle,
VA.fleetno,
VA.resourcetypeID,
VA.resourceclassID,
VA.resourcetype,
VA.startdate,
VA.enddate,
VA.actualstartdate,
VA.actualenddate,
DA.Resource_Driver

from 

RoundInstances RI

INNER JOIN Rounds R on R.roundID = RI.roundID
INNER JOIN roundgroups RG on RG.roundgroupID = R.roundgroupID
LEFT JOIN #VehicleAllocations VA on VA.roundinstanceID = RI.roundinstanceID
LEFT JOIN #DriverAllocations DA on DA.roundinstanceID = RI.roundinstanceID
