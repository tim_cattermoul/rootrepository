
/************************************************************************************************************************************************************************
* Lists the sites
* 
************************************************************************************************************************************************************************/

select distinct
	S.siteID,
	S.clientreference,
	STY.sitetype,
	S.site,
	C.contractID,
	R.regionID,
	PA.sourcedescription as site_address,
	cast(isnull(ST.locality, isnull(ST.townname, isnull(PT.posttown, ''))) as nvarchar(255)) as locality,
	PC.postcode,
	PA.lat,
	PA.lon,
	
	
	PA.pointaddressID,
	S.startdate,
	S.enddate
from
	regions R with (nolock)
	join contracts C with (nolock) on R.regionID = C.regionID
	join contractsites CS with (nolock) on C.contractID = CS.contractID
	join sites S with (nolock) on CS.siteID = S.siteID
	join sitetypes STY with (nolock) on S.sitetypeID = STY.sitetypeID
	join pointaddresses PA with (nolock) on S.pointID = PA.pointaddressID and S.pointtypeID = PA.pointtypeID
	left join postcodes PC with (nolock) on PA.postcodeID = PC.postcodeID
	left join posttowns PT with (nolock) on PC.posttownID = PT.posttownID
	join streets ST with (nolock) on PA.primary_streetID = ST.streetID
where
	S.sitetypeID in (1, 3) ---- Depot, Weighbridge



