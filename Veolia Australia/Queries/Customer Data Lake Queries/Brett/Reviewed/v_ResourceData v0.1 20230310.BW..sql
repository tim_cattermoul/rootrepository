
/************************************************************************************************************************************************************************
* Resources can have extended or additional data associated with them e.g. FleetObjectNo, TruckMaxWeight.
* This view provides the list of extended or additional data for resources.
* 
************************************************************************************************************************************************************************/

select
	X.resourcedataID
	,X.resourceID
	,X.datatype
	,convert(nvarchar(max), isnull(LU.description, isnull(X.resourcedata, ''))) as resourcedata ---- THE VALUE USED IN REPORTING ETC ETC
	,LU.clientreference
from
(
select distinct
	RD.resourcedataID,
	RD.resourceID,
	RDT.sortorder,
	DT.datatypeID,
	case
		when RDT.resourcedatatype is null then DT.datatype
		when len(trim(RDT.resourcedatatype)) = 0 then DT.datatype
		else RDT.resourcedatatype
	end as datatype,
	isnull(DT.valuedomaintypeID, 0) as valuedomaintypeID,
	isnull(DT.valuedomain, '0') as valuedomain,
	case 
		when CDT.coredatatype <> 'boolean' then RD.resourcedata
		else 
			case
				when RD.resourcedata = '0' then 'No'
				when RD.resourcedata = '1' then 'Yes'
				else RD.resourcedata
			end
		end
	as resourcedata

	
from
	resourcedata RD with (nolock)
	join resourcedatatypes RDT with (nolock) on RD.resourcedatatypeID = RDT.resourcedatatypeID
	join datatypes DT with (nolock) on RDT.datatypeID = DT.datatypeID
	join coredatatypes CDT with (nolock) on DT.coredatatypeID = CDT.coredatatypeID

) X
	left join TemporaryStore..X_v_master_datattype_lookup_values LU on
		X.datatypeID = LU.datatypeID 
		and 
		X.valuedomaintypeID = LU.valuedomaintypeID 
		and 
		X.valuedomain = LU.valuedomain 
		and 
		X.resourcedata = isnull(LU.code, X.resourcedata)
