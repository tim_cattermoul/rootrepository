select 

rie.roundinstanceeventID,
roundinstanceID,
rie.roundeventtypeID,
ret.roundeventtype,
eventdatetime,
rie.eventstartdatetime,
rie.eventenddatetime,
location_echotypeID,
ET.echotypename as LocationType,
location_echoID,
location_lat,
location_lon,
resourceID,
ried.roundinstanceeventdataID,
ried.roundeventtypedatatypeID,
DT.datatype,
RIED.roundinstanceeventdata,
RIE.tasklineID 

from roundinstanceevents RIE

LEFT JOIN roundinstanceeventdata RIED on RIED.roundinstanceeventID = RIE.roundinstanceeventID
INNER JOIN roundeventtypes RET on RET.roundeventtypeID = RIE.roundeventtypeID
LEFT JOIN roundeventtypedatatypes RETD on RETD.roundeventtypedatatypeID = RIED.roundeventtypedatatypeID
LEFT JOIN datatypes DT on RETD.datatypeID = DT.datatypeID
LEFT JOIN echotypes ET on ET.echotypeID = RIE.location_echotypeID

