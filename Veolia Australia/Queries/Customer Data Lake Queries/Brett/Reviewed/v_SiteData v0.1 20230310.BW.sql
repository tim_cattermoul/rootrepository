--use RedAZEcho2_Dev3;
--use RedAZEcho2_Int;
--go


/************************************************************************************************************************************************************************
* Resources can have extended or additional data associated with them e.g. FleetObjectNo, TruckMaxWeight.
* This view provides the list of extended or additional data for resources.*  
* 
************************************************************************************************************************************************************************/

select distinct
	X.sitedataID
	,X.siteID
	,X.datatype
	,convert(nvarchar(max), isnull(LU.description, isnull(X.sitedata, ''))) as sitedata ---- THE VALUE USED IN REPORTING ETC ETC
	,LU.clientreference
from
(
select 
	SD.sitedataID,
	SD.siteID,
	SD.parent_sitedataID,
	SD.sitetypedatatypeID,
	DT.datatypeID,
	case
		when SDT.sitetypedatatype is null then DT.datatype
		when len(trim(SDT.sitetypedatatype)) = 0 then DT.datatype
		else SDT.sitetypedatatype
	end as datatype,
	isnull(DT.valuedomaintypeID, 0) as valuedomaintypeID,
	isnull(DT.valuedomain, '0') as valuedomain,
	case 
		when CDT.coredatatype <> 'boolean' then SD.sitedata
		else 
			case
				when SD.sitedata = '0' then 'No'
				when SD.sitedata = '1' then 'Yes'
				else SD.sitedata
			end
		end
	as sitedata

from
	sitedata SD with (nolock)
	join sitetypedatatypes SDT with (nolock) on SD.sitetypedatatypeID = SDT.sitetypedatatypeID
	join datatypes DT with (nolock) on SDT.datatypeID = DT.datatypeID
	join coredatatypes CDT with (nolock) on DT.coredatatypeID = CDT.coredatatypeID
) X
	left join TemporaryStore..X_v_master_datattype_lookup_values LU on
		X.datatypeID = LU.datatypeID 
		and 
		X.valuedomaintypeID = LU.valuedomaintypeID 
		and 
		X.valuedomain = LU.valuedomain 
		and 
		X.sitedata = isnull(LU.code, X.sitedata)




