
/***********************************************************************************************************************************
* wb_siteproducts - view of weighbridge sites and their products.
* Important for tasklines and roundinstancevents where the driver is instructed to tip at a preferred site.
* This view can be related to tasklines_scheduled and preferred_siteID and siteproductID.
* 
***********************************************************************************************************************************/

create view [dbo].[vw_v_wb_siteproducts] as 
select
	WBSP.siteproductID
	,WBSP.siteID
	,WBSP.productID
	,WBSP.siteproduct
	,P.default_unitID
	,u.unit
	,WBSP.clientreference
	,WBSP.startdate --Watermark
	,WBSP.enddate -- Watermark

from
	dbo.wb_siteproducts WBSP with (nolock)
	join dbo.products P with (nolock) on WBSP.productID = P.productID
	left join dbo.units u on u.unitID = p.default_unitID


	

