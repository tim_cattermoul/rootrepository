create view [dbo].[vw_v_contacts] as 

select 
	C.contactID,
	C.title,
	C.firstname,
	C.lastname,
	C.position,
	C.telephone,
	C.mobile,
	C.email,
	C.partyID,
	C.startdate, -- Watermark
	C.enddate -- Watermark

from contacts C