
/************************************************************************************************************************************************************************
* Resources can have extended or additional data associated with them e.g. FleetObjectNo, TruckMaxWeight.
* This view provides the list of extended or additional data for resources.
* 
************************************************************************************************************************************************************************/
create view [dbo].[vw_v_resourcedata] as 
select distinct
	RD.resourcedataID,
	RD.resourceID,
	RDT.sortorder,
	DT.datatypeID,
	case
		when RDT.resourcedatatype is null then DT.datatype
		when len(trim(RDT.resourcedatatype)) = 0 then DT.datatype
		else RDT.resourcedatatype
	end as datatype,
	isnull(DT.valuedomaintypeID, 0) as valuedomaintypeID,
	isnull(DT.valuedomain, '0') as valuedomain,
	CASE WHEN DT.valuedomaintypeID = 10 then C.description else RD.resourcedata end as resourcedata -- commenting out for consistency with other views. Can revert if we want to convert 'True' to 'Yes' and 'False' to 'No'
	--case 
	--	when CDT.coredatatype <> 'boolean' then RD.resourcedata
	--	else 
	--		case
	--			when RD.resourcedata = '0' then 'No'
	--			when RD.resourcedata = '1' then 'Yes'
	--			else RD.resourcedata
	--		end
	--	end
	--as resourcedata

	
from
	resourcedata RD with (nolock)
	left join resourcedatatypes RDT with (nolock) on RD.resourcedatatypeID = RDT.resourcedatatypeID
	left join datatypes DT with (nolock) on RDT.datatypeID = DT.datatypeID
	left join coredatatypes CDT with (nolock) on DT.coredatatypeID = CDT.coredatatypeID
	left join codes C on CAST(C.codefileID as nvarchar) = CAST(DT.valuedomain as nvarchar) and RD.resourcedata = C.code and DT.valuedomaintypeID = 10
