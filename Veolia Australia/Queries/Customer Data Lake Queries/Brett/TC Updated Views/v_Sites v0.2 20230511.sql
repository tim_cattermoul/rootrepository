
create view [dbo].[vw_v_sites] as 

select DISTINCT
	S.siteID,
	S.site,
	ST.sitetype,
	s.clientreference,
	s.pointID,
	s.startdate as sitestartdate, -- Watermark
	s.enddate as siteenddate, -- Watermark
	PA.pointaddressID,
	PA.sourcedescription,
	PA.property,
	SE.Street,
	SE.locality,
	PC.postcode,
	PA.UPRN,
	PA.lat,
	PA.lon,
	CN.country,
	C.contractID,
	C.contract,
	--CS.contractsiteID,
	--NULL as ServiceUnitID,
	R.regionID,
	R.region,
	S.partyID,
	S.contactID

from sites S

INNER JOIN sitetypes ST on ST.sitetypeID = S.sitetypeID
INNER JOIN pointaddresses PA on PA.pointaddressID = S.pointID and S.pointtypeID = pa.pointtypeID
LEFT JOIN postcodes PC on PC.postcodeID = PA.postcodeID
INNER JOIN contractsites CS on cs.siteID = S.siteID
INNER JOIN contracts C on cs.contractID = C.contractID
INNER JOIN regions R on R.regionID = C.regionID
LEFT JOIN streets SE on SE.streetID = PA.primary_streetID
LEFT JOIN countries CN on CN.countryID = PA.countryID

UNION ALL

select DISTINCT
	S.siteID,
	S.site,
	ST.sitetype,
	s.clientreference,
	s.pointID,
	s.startdate as sitestartdate, -- Watermark
	s.enddate as siteenddate, -- Watermark
	PA.pointaddressID,
	PA.sourcedescription,
	PA.property,
	SE.Street,
	SE.locality,
	PC.postcode,
	PA.UPRN,
	PA.lat,
	PA.lon,
	CN.country,
	C.contractID,
	C.contract,
	--NULL as contractsiteID,
	--su.serviceunitID as ServiceUnitID,
	R.regionID,
	R.region,
	S.partyID,
	S.contactID

from sites S

INNER JOIN sitetypes ST on ST.sitetypeID = S.sitetypeID
INNER JOIN pointaddresses PA on PA.pointaddressID = S.pointID and S.pointtypeID = pa.pointtypeID
LEFT JOIN postcodes PC on PC.postcodeID = PA.postcodeID
INNER JOIN serviceunits SU on su.siteID = S.siteID
LEFT JOIN services SV on SV.serviceID = SU.serviceID
LEFT JOIN servicegroups SG on SG.servicegroupID = SV.servicegroupID
LEFT JOIN contracts C on sg.contractID = C.contractID
LEFT JOIN regions R on R.regionID = C.regionID
LEFT JOIN streets SE on SE.streetID = PA.primary_streetID
LEFT JOIN countries CN on CN.countryID = PA.countryID

--order by siteID


