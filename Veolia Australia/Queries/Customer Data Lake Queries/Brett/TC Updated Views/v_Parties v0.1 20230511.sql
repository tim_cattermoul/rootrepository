create view [dbo].[vw_v_parties] as 

select 
	P.partyID,
	P.Party,
	P.clientreference,
	P.startdate as PartyStartDate, -- Watermark
	P.enddate as PartyEnddate, -- Watermark
	P.correspondence_siteID,
	P.invoice_siteID,
	P.isinternal,
	P.primary_contactID,
	P.defaultcontractID,
	P.accounttypeID,
	AT.accounttype
	--,P.* 
from 
parties P

LEFT JOIN accounttypes AT on AT.accounttypeID = P.accounttypeID