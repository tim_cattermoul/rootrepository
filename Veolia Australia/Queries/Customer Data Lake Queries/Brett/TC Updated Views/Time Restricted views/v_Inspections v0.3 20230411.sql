
/************************************************************************************************************************************************************************
* Lists the inspections that have occurred for a roundinstance.  
* 
************************************************************************************************************************************************************************/
CREATE VIEW [dbo].[vw_v_Inspections] as
--declare @start datetime = dateadd(d,datediff(d,0,getdate()),-56)
--declare @end datetime = dateadd(d,datediff(d,0,getdate()),0)
--declare @startday int = datediff(d,0,@start), @days int = 56

--declare @endday int = @startday + @days  

select
	I.inspectionID
	,I.sourceechoID as roundinstanceID
	,I.echoID as resourceID
	,IT.inspectiontype
	,I.inspectiontypeID
	,I.sourceechotypeID
	,ET1.echotypename as sourceechotypename
	,I.echotypeID
	,ET2.echotypename
	,I.inspectiondate as startdate
	,I.completiondate
	,I.lastupdated -- Watermark
from
	inspections I with (nolock)
	join inspectiontypes IT with (nolock) on I.inspectiontypeID = IT.inspectiontypeID
	left join echotypes ET1 with (nolock) on ET1.echotypeID = I.sourceechotypeID
	left join echotypes ET2 with (nolock) on ET2.echotypeID = I.echotypeID

	-- Removed filters on echotypeIDs to futureproof, and because other divisions have inspections against other EchoTypes


where I.lastupdated  <= dateadd(d,datediff(d,0,getdate()),0) and I.lastupdated  > dateadd(d,datediff(d,0,getdate()),-56)







