declare @start datetime = dateadd(d,datediff(d,0,getdate()),-56)
declare @end datetime = dateadd(d,datediff(d,0,getdate()),0)
declare @startday int = datediff(d,0,@start), @days int = 56

declare @endday int = @startday + @days  

select 
	TLA.tasklineactionID,
	TLA.actiontype,
	TLA.tasklineactioncreatedbyuserID,
	US.username as tasklineactioncreatedbyusername, -- tasklineactioncreatedbyuserID = 0: System update
	TLA.tasklineactioncreateddate, -- Watermark
	TLA.tasklineID,
	363 as echotypeID,
	TLA.tasklineID as echoID,
	TLA.tasklinetypeID,
	TLT.tasklinetype,
	TLA.clientreference as TasklineClientReference,
	TLA.taskID,
	TLA.scheduledassetquantity,
	TLA.scheduledproductquantity,
	TLA.actualassetquantity,
	TLA.actualproductquantity,
	TLA.expectedproductquantity,
	TLA.adjustedproductquantity,
	TLA.productID,
	P.product,
	TLA.product_unitID,
	U.Unit,
	TLA.completeddate,
	TLA.tasklinestateID,
	TLS.tasklinestate,
	TLA.resolutioncodeID,
	RC.resolutioncode,
	--TLA.parent_tasklineID,
	TLA.isserialised,
	TLA.autoconfirmed,
	--TLA.gpseventguid,
	TLA.preferred_siteID,
	TLA.siteproductID,
	TLA.assettypeID,
	AT.assettype,
	AT.clientreference as AssetTypeClientReference,
	AT.unitID,
	U2.unit as AssetUnit,
	AT.size

from tasklineactions TLA

	LEFT JOIN products P on P.productID = TLA.productID
	LEFT JOIN productunits PU on PU.productunitiD = TLA.product_unitID
	LEFT JOIN units U on U.unitID = PU.unitID
	LEFT JOIN tasklinestates TLS on TLS.tasklinestateID = TLA.tasklinestateID
	LEFT JOIN resolutioncodes RC on RC.resolutioncodeID = TLA.resolutioncodeID
	INNER JOIN tasklinetypes TLT on TLT.tasklinetypeID = TLA.tasklinetypeID
	LEFT JOIN assettypes AT on AT.assettypeID = TLA.assettypeID
	LEFT JOIN Units U2 on U2.unitID = AT.unitID
	LEFT JOIN Users US on US.userID = TLA.tasklineactioncreatedbyuserID

where TLA.tasklineactioncreateddate  <= @end and TLA.tasklineactioncreateddate  > @start