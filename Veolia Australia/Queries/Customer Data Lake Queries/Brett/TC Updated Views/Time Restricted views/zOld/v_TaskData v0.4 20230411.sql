
/***********************************************************************************************************************************
* taskdata - view of extensible data related to tasks
* 
***********************************************************************************************************************************/

declare @start datetime = dateadd(d,datediff(d,0,getdate()),-56)
declare @end datetime = dateadd(d,datediff(d,0,getdate()),0)
declare @startday int = datediff(d,0,@start), @days int = 56

declare @endday int = @startday + @days  

select 
	TD.taskdataID
	,TD.taskID
	,T.lastupdated as Tasklastupdateddate -- Watermark
	,TD.parent_taskdataID
	,DT.datatypeID
	,CDT.coredatatype
	,case
		when TDT.tasktypedatatype is null then DT.datatype
		when len(trim(TDT.tasktypedatatype)) = 0 then DT.datatype
		else TDT.tasktypedatatype
	end as datatype
	,isnull(DT.valuedomaintypeID, 0) as valuedomaintypeID
	,isnull(DT.valuedomain, '0') as valuedomain
	,CASE WHEN DT.valuedomaintypeID = 10 then C.description else TD.taskdata end as taskdata -- Commenting the below out for consistency with other views
	--,case 
	--	when CDT.coredatatype <> 'boolean' then TD.taskdata
	--	else 
	--		case
	--			when TD.taskdata = '0' then 'No'
	--			when TD.taskdata = '1' then 'Yes'
	--			else TD.taskdata
	--		end
	--	end
	--as taskdata
from
	taskdata TD with (nolock)
	left join tasktypedatatypes TDT with (nolock) on TD.tasktypedatatypeID = TDT.tasktypedatatypeID
	left join datatypes DT with (nolock) on TDT.datatypeID = DT.datatypeID
	left join coredatatypes CDT with (nolock) on DT.coredatatypeID = CDT.coredatatypeID
	left join codes C on CAST(C.codefileID as nvarchar) = CAST(DT.valuedomain as nvarchar) and TD.taskdata = C.code and DT.valuedomaintypeID = 10
	INNER JOIN tasks T on T.taskID = TD.taskID

where T.lastupdated  <= @end and T.lastupdated  > @start	