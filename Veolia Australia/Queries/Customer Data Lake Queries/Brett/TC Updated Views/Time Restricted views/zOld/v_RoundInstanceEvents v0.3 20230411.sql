

/************************************************************************************************************************************************************************
* Lists the events that have occurred for a roundinstance.  
* 
************************************************************************************************************************************************************************/

declare @start datetime = dateadd(d,datediff(d,0,getdate()),-56)
declare @end datetime = dateadd(d,datediff(d,0,getdate()),0)
declare @startday int = datediff(d,0,@start), @days int = 56

declare @endday int = @startday + @days  

select distinct
	RE.roundinstanceeventID
	,RE.roundinstanceID
	,RE.resourceID
	,RE.tasklineID
	,RET.roundeventtype 
	,RET.roundeventtypeID
	,RE.eventstartdatetime -- Watermark
	,RE.eventenddatetime -- Watermark
	,location_echotypeID,
	ET.echotypename as LocationType,
	location_echoID,
	location_lat,
	location_lon

from
	roundinstanceevents RE with (nolock)
	join roundeventtypes RET with (nolock) on RE.roundeventtypeID = RET.roundeventtypeID
	LEFT JOIN echotypes ET on ET.echotypeID = RE.location_echotypeID



where RE.eventstartdatetime  <= @end and RE.eventstartdatetime  > @start


