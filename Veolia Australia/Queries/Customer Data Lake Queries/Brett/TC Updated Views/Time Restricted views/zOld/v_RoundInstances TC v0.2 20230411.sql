
/************************************************************************************************************************************************************************
* Purpose of this view is to provide the basic roundinstance details being the primary vehicle and driver allocated to the rounsinstance
* and the start and finish times.
* 
* Clarifications / Potential issues:
* 1. Clarification: will roundinstanceID always be unique?
* 2. Is it possinble driver subquery return if multiple resources allocated and/or resources added, removed etc
* 3. Is it possible vehicle subquery return if multiple resources allocated and/or resources added, removed etc
* 4. For consistencey should driver and vehicle subquery be both getting the allocated resource from resourceallocations
* 5. Using roundinstanceresources for drivers returns nulls
* 6. Suggest to include the clientreference for both vehicle and driver - this is a reference into Veolia AUS system and they may use it in profit centre determination
*  
************************************************************************************************************************************************************************/


declare @start datetime = dateadd(d,datediff(d,0,getdate()),-56)
declare @end datetime = dateadd(d,datediff(d,0,getdate()),0)
declare @startday int = datediff(d,0,@start), @days int = 56

declare @endday int = @startday + @days  


select DISTINCT 

	RI.roundinstanceID,
	RI.clientreference,
	RI.roundID,
	RS.roundstate,

	--DA.resourceID_driver,

	--VA.resourceID_vehicle,

	RI.ss_date as scheduled_date_ri,
	RI.startdate as startdate_ri,
	RI.finishdate as finishdate_ri,

	--VA.startdate_vehicle,
	--VA.enddate_vehicle,
	--VA.actualstartdate_vehicle,		---- BW: always null - never set in the Veolia Aus Comm instance
	--VA.actualenddate_vehicle		---- BW: always null - never set in the Veolia Aus Comm instance
	RI.lastupdated -- Watermark
from 
	RoundInstances RI with (nolock) 
	left join roundstates RS with (nolock) on RI.roundstateID = RS.roundstateID
	--left join #VehicleAllocations VA on VA.roundinstanceID = RI.roundinstanceID
	--left join #DriverAllocations DA on DA.roundinstanceID = RI.roundinstanceID;


where RI.lastupdated  <= @end and RI.lastupdated  > @start