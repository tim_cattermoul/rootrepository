
/***********************************************************************************************************************************
* tasklinedata - view of extensible data related to tasklines
* 
***********************************************************************************************************************************/
create view [dbo].[vw_v_TasklineData] as
--declare dateadd(d,datediff(d,0,getdate()),-56) datetime = dateadd(d,datediff(d,0,getdate()),-56)
--declare dateadd(d,datediff(d,0,getdate()),0) datetime = dateadd(d,datediff(d,0,getdate()),0)
--declare dateadd(d,datediff(d,0,getdate()),-56)day int = datediff(d,0,dateadd(d,datediff(d,0,getdate()),-56)), @days int = 56

--declare dateadd(d,datediff(d,0,getdate()),0)day int = dateadd(d,datediff(d,0,getdate()),-56)day + @days

WITH CTE_TasklineUpdates as
(

select tasklineID, MAX(tasklineactioncreateddate) as lastupdated from tasklineactions

group by tasklineID

)


select 
	TLD.tasklinedataID
	,TLD.tasklineID
	,TU.lastupdated as tasklinelastupdateddate -- Watermark
	,TLD.parent_tasklinedataID
	,DT.datatypeID
	,CDT.coredatatype
	,case
		when TLTDT.tasklinetypedatatype is null then DT.datatype
		when len(trim(TLTDT.tasklinetypedatatype)) = 0 then DT.datatype
		else TLTDT.tasklinetypedatatype
	end as datatype
	,isnull(DT.valuedomaintypeID, 0) as valuedomaintypeID
	,isnull(DT.valuedomain, '0') as valuedomain
	,CASE WHEN DT.valuedomaintypeID = 10 then C.description else tld.tasklinedata end as tasklinedata -- Commenting the below out for consistency with other views
	--,case 
	--	when CDT.coredatatype <> 'boolean' then TLD.tasklinedata
	--	else 
	--		case
	--			when TLD.tasklinedata = '0' then 'No'
	--			when TLD.tasklinedata = '1' then 'Yes'
	--			else TLD.tasklinedata
	--		end
	--	end
	--as tasklinedata
from
	tasklinedata TLD with (nolock)
	left join tasklinetypedatatypes TLTDT with (nolock) on TLD.tasklinetypedatatypeID = TLTDT.tasklinetypedatatypeID
	left join datatypes DT with (nolock) on TLTDT.datatypeID = DT.datatypeID
	left join coredatatypes CDT with (nolock) on DT.coredatatypeID = CDT.coredatatypeID
	left join codes C on CAST(C.codefileID as nvarchar) = CAST(DT.valuedomain as nvarchar) and tld.tasklinedata = C.code and DT.valuedomaintypeID = 10
	LEFT JOIN CTE_TasklineUpdates TU on TU.tasklineID = TLD.tasklineID

where TU.lastupdated  <= dateadd(d,datediff(d,0,getdate()),0) and TU.lastupdated  > dateadd(d,datediff(d,0,getdate()),-56)
	