
/******************************************************************************************************
* Lists the tasks
* 
* 
* 
******************************************************************************************************/
create view [dbo].[vw_v_Tasks] as
--declare dateadd(d,datediff(d,0,getdate()),-56) datetime = dateadd(d,datediff(d,0,getdate()),-56)
--declare dateadd(d,datediff(d,0,getdate()),0) datetime = dateadd(d,datediff(d,0,getdate()),0)
--declare dateadd(d,datediff(d,0,getdate()),-56)day int = datediff(d,0,dateadd(d,datediff(d,0,getdate()),-56)), @days int = 56

--declare dateadd(d,datediff(d,0,getdate()),0)day int = dateadd(d,datediff(d,0,getdate()),-56)day + @days  


select distinct
	T.taskID,
	T.taskreference,
	T.clientreference as task_clientreference,
	T.echotypeID,
	ET.echotypename as echotype,
	T.echoID,
	T.roundID,
	T.roundinstanceID,
	T.scheduledroundinstanceID,

	TT.tasktype,
	TT.clientreference as tasktype_clientreference,
	TS.taskstate,
	RC.resolutioncode,
	P.priority,
	T.task,
	T.tasknotes,
	T.taskcreateddate,
	T.taskscheduleddate,
	T.taskduedate,
	T.taskcompleteddate,
	T.taskenddate,
	T.contractID,
	T.partyID,
	T.agreementID,
	T.serviceunitID,
	SU.siteID,
	T.servicetaskID,
	T.servicetaskscheduleID,
	T.tasktypeID,
	T.taskstateID,
	T.resolutioncodeID,
	T.lastupdated -- Watermark
from
	tasks T with (nolock)
	join echotypes ET with (nolock) on T.echotypeID = ET.echotypeID
	join tasktypes TT with (nolock) on TT.tasktypeID = T.TasktypeID
	join taskstates TS with (nolock) on TS.taskstateID = T.TaskStateID
	left join resolutioncodes RC with (nolock) on RC.resolutioncodeID = T.resolutioncodeID
	left join priorities P with (nolock) on T.priorityID = P.priorityID
	left join serviceunits SU with (nolock) on T.serviceunitID = SU.serviceunitID

where T.lastupdated  <= dateadd(d,datediff(d,0,getdate()),0) and T.lastupdated  > dateadd(d,datediff(d,0,getdate()),-56)

