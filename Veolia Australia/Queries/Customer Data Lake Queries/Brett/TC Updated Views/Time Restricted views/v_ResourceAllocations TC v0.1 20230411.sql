CREATE VIEW [dbo].[vw_v_ResourceAllocations] as

--declare @start datetime = dateadd(d,datediff(d,0,getdate()),-56)
--declare @end datetime = dateadd(d,datediff(d,0,getdate()),0)
--declare @startday int = datediff(d,0,@start), @days int = 56

--declare @endday int = @startday + @days   

select DISTINCT
	RA.resourceallocationID,
	RT.resourceclassID,
	RA.roundinstanceID,
	RA.resourceID --as resourceID_vehicle,	
	,RA.startdate --as startdate_vehicle,				---- BW: giving it more context
	,RA.enddate --as enddate_vehicle,					---- BW: giving it more context
	,RA.actualstartdate --as actualstartdate_vehicle,	---- BW: giving it more context - always null - never set in the Veolia Aus Comm instance
	,RA.actualenddate --as actualenddate_vehicle,		---- BW: giving it more context - always null - never set in the Veolia Aus Comm instance
	,RA.lastupdatedutcdate -- watermark

from 
	resourceallocations RA with (nolock) 
	join resourcetypes RT with (nolock) on RA.resourcetypeID = RT.resourcetypeID
	join roundinstances RI with (nolock) on RA.roundinstanceID = RI.roundinstanceID

/*WHERE 
	RT.resourceclassID = 2 -- Vehicle*/

where RA.lastupdatedutcdate <= dateadd(d,datediff(d,0,getdate()),0) and RA.lastupdatedutcdate > dateadd(d,datediff(d,0,getdate()),-56)