-- Create a synonym for the table in database - CHANGE DATABASE FOR EACH CUSTOMER 

--DECLARE @databaseName NVARCHAR(128) = 'VeoliaAZReplica';

CREATE SYNONYM accounttypes FOR VeoliaAZReplica.dbo.accounttypes;
CREATE SYNONYM agreementlines FOR VeoliaAZReplica.dbo.agreementlines;
CREATE SYNONYM agreementlinetypes FOR VeoliaAZReplica.dbo.agreementlinetypes;
CREATE SYNONYM agreements FOR VeoliaAZReplica.dbo.agreements;
CREATE SYNONYM agreementtypes FOR VeoliaAZReplica.dbo.agreementtypes;
CREATE SYNONYM assetclasses FOR VeoliaAZReplica.dbo.assetclasses;
CREATE SYNONYM assettypes FOR VeoliaAZReplica.dbo.assettypes;
CREATE SYNONYM businessunits FOR VeoliaAZReplica.dbo.businessunits;
CREATE SYNONYM codes FOR VeoliaAZReplica.dbo.codes;
CREATE SYNONYM contacts FOR VeoliaAZReplica.dbo.contacts;
CREATE SYNONYM contracts FOR VeoliaAZReplica.dbo.contracts;
CREATE SYNONYM contractsites FOR VeoliaAZReplica.dbo.contractsites;
CREATE SYNONYM countries FOR VeoliaAZReplica.dbo.countries;
CREATE SYNONYM coredatatypes FOR VeoliaAZReplica.dbo.coredatatypes;
CREATE SYNONYM datatypes FOR VeoliaAZReplica.dbo.datatypes;
CREATE SYNONYM echocodes FOR VeoliaAZReplica.dbo.echocodes;
CREATE SYNONYM echotypes FOR VeoliaAZReplica.dbo.echotypes;
CREATE SYNONYM inspectiondata FOR VeoliaAZReplica.dbo.inspectiondata;
CREATE SYNONYM inspections FOR VeoliaAZReplica.dbo.inspections;
CREATE SYNONYM inspectiontypes FOR VeoliaAZReplica.dbo.inspectiontypes;
CREATE SYNONYM inspectiontypedatatypes FOR VeoliaAZReplica.dbo.inspectiontypedatatypes;
CREATE SYNONYM invoiceschedules FOR VeoliaAZReplica.dbo.invoiceschedules;
CREATE SYNONYM parties FOR VeoliaAZReplica.dbo.parties;
CREATE SYNONYM paymentmethods FOR VeoliaAZReplica.dbo.paymentmethods;
CREATE SYNONYM pointaddresses FOR VeoliaAZReplica.dbo.pointaddresses;
CREATE SYNONYM postcodes FOR VeoliaAZReplica.dbo.postcodes;
CREATE SYNONYM priorities FOR VeoliaAZReplica.dbo.priorities;
CREATE SYNONYM products FOR VeoliaAZReplica.dbo.products;
CREATE SYNONYM productunits FOR VeoliaAZReplica.dbo.productunits;
CREATE SYNONYM regions FOR VeoliaAZReplica.dbo.regions;
CREATE SYNONYM resolutioncodes FOR VeoliaAZReplica.dbo.resolutioncodes;
CREATE SYNONYM resourceallocations FOR VeoliaAZReplica.dbo.resourceallocations;
CREATE SYNONYM resourceclasses FOR VeoliaAZReplica.dbo.resourceclasses;
CREATE SYNONYM resourcedata FOR VeoliaAZReplica.dbo.resourcedata;
CREATE SYNONYM resourcedatatypes FOR VeoliaAZReplica.dbo.resourcedatatypes;
CREATE SYNONYM resources FOR VeoliaAZReplica.dbo.resources;
CREATE SYNONYM resourcetypes FOR VeoliaAZReplica.dbo.resourcetypes;
CREATE SYNONYM roundeventtypedatatypes FOR VeoliaAZReplica.dbo.roundeventtypedatatypes;
CREATE SYNONYM roundeventtypes FOR VeoliaAZReplica.dbo.roundeventtypes;
CREATE SYNONYM roundgroups FOR VeoliaAZReplica.dbo.roundgroups;
CREATE SYNONYM roundinstanceeventdata FOR VeoliaAZReplica.dbo.roundinstanceeventdata;
CREATE SYNONYM roundinstanceevents FOR VeoliaAZReplica.dbo.roundinstanceevents;
CREATE SYNONYM roundinstances FOR VeoliaAZReplica.dbo.roundinstances;
CREATE SYNONYM rounds FOR VeoliaAZReplica.dbo.rounds;
CREATE SYNONYM roundstates FOR VeoliaAZReplica.dbo.roundstates;
CREATE SYNONYM roundtypes FOR VeoliaAZReplica.dbo.roundtypes;
CREATE SYNONYM servicegroups FOR VeoliaAZReplica.dbo.servicegroups;
CREATE SYNONYM services FOR VeoliaAZReplica.dbo.services;
CREATE SYNONYM serviceunits FOR VeoliaAZReplica.dbo.serviceunits;
CREATE SYNONYM shifts FOR VeoliaAZReplica.dbo.shifts;
CREATE SYNONYM sitedata FOR VeoliaAZReplica.dbo.sitedata;
CREATE SYNONYM sites FOR VeoliaAZReplica.dbo.sites;
CREATE SYNONYM sitetypedatatypes FOR VeoliaAZReplica.dbo.sitetypedatatypes;
CREATE SYNONYM sitetypes FOR VeoliaAZReplica.dbo.sitetypes;
CREATE SYNONYM streets FOR VeoliaAZReplica.dbo.streets;
CREATE SYNONYM taskdata FOR VeoliaAZReplica.dbo.taskdata;
CREATE SYNONYM tasklineactions FOR VeoliaAZReplica.dbo.tasklineactions;
CREATE SYNONYM tasklinedata FOR VeoliaAZReplica.dbo.tasklinedata;
CREATE SYNONYM tasklines FOR VeoliaAZReplica.dbo.tasklines;
CREATE SYNONYM tasklinestates FOR VeoliaAZReplica.dbo.tasklinestates;
CREATE SYNONYM tasklinetypedatatypes FOR VeoliaAZReplica.dbo.tasklinetypedatatypes;
CREATE SYNONYM tasklinetypes FOR VeoliaAZReplica.dbo.tasklinetypes;
CREATE SYNONYM tasktypedatatypes FOR VeoliaAZReplica.dbo.tasktypedatatypes;
CREATE SYNONYM tasktypes FOR VeoliaAZReplica.dbo.tasktypes;
CREATE SYNONYM tasks FOR VeoliaAZReplica.dbo.tasks;
CREATE SYNONYM taskstates FOR VeoliaAZReplica.dbo.taskstates;
CREATE SYNONYM units FOR VeoliaAZReplica.dbo.units;
CREATE SYNONYM users FOR VeoliaAZReplica.dbo.users;
CREATE SYNONYM wb_siteproducts FOR VeoliaAZReplica.dbo.wb_siteproducts
