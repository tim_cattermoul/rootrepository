create view [dbo].[vw_v_agreementlines] as 
select 
	
	agreementlineID,
	AL.agreementlinetypeID,
	ALT.agreementlinetype,
	AL.startdate,-- Watermark
	AL.enddate,-- Watermark
	agreementID,
	serviceID,
	isadhoc,
	siteID,
	invoicescheduleID,
	invoice_siteID,
	invoice_contactID,
	billingrule

	
	from agreementlines AL

	INNER JOIN agreementlinetypes ALT on ALT.agreementlinetypeID = AL.agreementlinetypeID

	