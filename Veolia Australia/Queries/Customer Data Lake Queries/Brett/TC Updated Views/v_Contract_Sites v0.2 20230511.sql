create view [dbo].[vw_v_contractsites] as 

select
	contractsiteID
	,81 as echotypeID
	,contractID
	,siteID
	,active
	,startdate -- Watermark
	,enddate -- Watermark
from
	contractsites with (nolock)