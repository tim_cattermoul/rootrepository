
/************************************************************************************************************************************************************************
* Purpose of this view is to provide the the list of rounds - can be used as a reference table e.g. join into v_roundinstance_summary.
* Provides the region, contract, servicegroup, service, roundgroup and round.
* The customer clientreference of the round is also included.
* 
*  
************************************************************************************************************************************************************************/
create view [dbo].[vw_v_rounds] as 
select distinct
	R.roundID,
	R.clientreference,

	SI.siteID,
	C.contractID,
	REG.regionID,
	
	BU.businessunit,
	--RG.businessunitID as cost_centre,			-- Optional, could act as an FK to SAP cost centre if elected to enter into ECHO
												-- BW: never set or populated in Veolia Aus Commercial
	RT.roundtype,
	
	SG.servicegroup,
	S.service,
	RG.roundgroup,
	R.round,
	SH.shift,

	R.startdate, -- Watermark
	R.enddate -- Watermark
from
	regions REG with (nolock)
	join contracts C with (nolock) on REG.regionID = C.regionID
	join servicegroups SG with (nolock) on C.contractID = SG.contractID
	join services S with (nolock) on SG.servicegroupID = S.servicegroupID
	join roundgroups RG with (nolock) on S.serviceID = RG.serviceID
	join rounds R with (nolock) on RG.roundgroupID = R.roundgroupID
	join roundtypes RT with (nolock) on R.roundtypeID = RT.roundtypeID
	join shifts SH with (nolock) on R.shiftID = SH.shiftID
	join sites SI with (nolock) on R.dispatch_siteID = SI.siteID
	left join businessunits BU with (nolock) on RG.businessunitID = BU.businessunitID


