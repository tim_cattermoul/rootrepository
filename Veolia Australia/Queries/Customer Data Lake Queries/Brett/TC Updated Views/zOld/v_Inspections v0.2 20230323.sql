
/************************************************************************************************************************************************************************
* Lists the inspections that have occurred for a roundinstance.  
* 
************************************************************************************************************************************************************************/

select
	I.inspectionID
	,I.sourceechoID as roundinstanceID
	,I.echoID as resourceID
	,IT.inspectiontype
	,I.inspectiontypeID
	,I.sourceechotypeID
	,ET1.echotypename as sourceechotypename
	,I.echotypeID
	,ET2.echotypename
	,I.inspectiondate as startdate
	,I.completiondate
	,I.lastupdated -- Watermark
from
	inspections I with (nolock)
	join inspectiontypes IT with (nolock) on I.inspectiontypeID = IT.inspectiontypeID
	left join echotypes ET1 with (nolock) on ET1.echotypeID = I.sourceechotypeID
	left join echotypes ET2 with (nolock) on ET2.echotypeID = I.echotypeID

	-- Removed filters on echotypeIDs to futureproof, and because other divisions have inspections against other EchoTypes










