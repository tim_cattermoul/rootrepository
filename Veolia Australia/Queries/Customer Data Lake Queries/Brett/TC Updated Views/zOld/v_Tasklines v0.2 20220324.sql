WITH CTE_TasklineUpdates as
(

select tasklineID, MAX(tasklineactioncreateddate) as lastupdated from tasklineactions

group by tasklineID

)

select 
	TL.tasklineID,
	363 as echotypeID,
	TL.tasklineID as echoID,
	TL.tasklinetypeID,
	TLT.tasklinetype,
	TL.taskID,
	TL.scheduledassetquantity,
	TL.scheduledproductquantity,
	TL.actualassetquantity,
	TL.actualproductquantity,
	TL.expectedproductquantity,
	TL.adjustedproductquantity,
	TL.productID,
	P.product,
	TL.product_unitID,
	U.Unit,
	TL.completeddate,
	TL.tasklinestateID,
	TLS.tasklinestate,
	TL.resolutioncodeID,
	RC.resolutioncode,
	TL.parent_tasklineID,
	TL.isserialised,
	TL.autoconfirmed,
	TL.gpseventguid,
	TL.preferred_siteID,
	TL.siteproductID,
	TL.assettypeID,
	AC.assetclassID,
	AC.assetclass,
	AT.assettype,
	AT.clientreference,
	AT.unitID,
	U2.unit as AssetUnit,
	AT.size,
	TU.lastupdated -- Watermark

	from Tasklines TL

	LEFT JOIN products P on P.productID = TL.productID
	LEFT JOIN productunits PU on PU.productunitiD = TL.product_unitID
	LEFT JOIN units U on U.unitID = PU.unitID
	INNER JOIN tasklinestates TLS on TLS.tasklinestateID = TL.tasklinestateID
	LEFT JOIN resolutioncodes RC on RC.resolutioncodeID = TL.resolutioncodeID
	INNER JOIN tasklinetypes TLT on TLT.tasklinetypeID = TL.tasklinetypeID
	LEFT JOIN assettypes AT on AT.assettypeID = TL.assettypeID
	LEFT JOIN assetclasses AC on AT.assetclassID = AC.assetclassID
	LEFT JOIN Units U2 on U2.unitID = AT.unitID
	LEFT JOIN CTE_TasklineUpdates TU on TU.tasklineID = TL.tasklineID


