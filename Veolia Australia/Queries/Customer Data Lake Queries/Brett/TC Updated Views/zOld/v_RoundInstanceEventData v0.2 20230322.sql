select 

	RIED.roundinstanceeventdataID
	,RIED.roundinstanceeventID
	,RIED.parent_roundinstanceeventdataID
	,RIED.roundeventtypedatatypeID
	,DT.datatypeID
	,CDT.coredatatype
	,case
		when RETDT.roundeventtypedatatype is null then DT.datatype
		when len(trim(RETDT.roundeventtypedatatype)) = 0 then DT.datatype
		else RETDT.roundeventtypedatatype
	end as datatype
	,isnull(DT.valuedomaintypeID, 0) as valuedomaintypeID
	,isnull(DT.valuedomain, '0') as valuedomain,
	RIED.roundinstanceeventdata
	,CASE WHEN DT.valuedomaintypeID = 10 THEN c.description 
		WHEN CAST(DT.valuedomain as nvarchar) = '225|[Location_EchoID]' THEN W.siteproduct
		ELSE RIED.roundinstanceeventdata END AS roundinstanceeventdata -- TC note: I've commented the below out for this example as it appears that booleans are being stored as 'true' or 'false' as opposed to 1 or 0, so this might not be necessary

	--,case 
	--	when CDT.coredatatype <> 'boolean' then RIED.roundinstanceeventdata
	--	else 
	--		case
	--			when RIED.roundinstanceeventdata = '0' then 'No'
	--			when RIED.roundinstanceeventdata = '1' then 'Yes'
	--			else RIED.roundinstanceeventdata
	--		end
	--	end
	--as roundinstanceeventdata

from 

roundinstanceeventdata RIED

	left join roundeventtypedatatypes RETDT with (nolock) on RIED.roundeventtypedatatypeID = RETDT.roundeventtypedatatypeID
	left join datatypes DT with (nolock) on RETDT.datatypeID = DT.datatypeID
	left join coredatatypes CDT with (nolock) on DT.coredatatypeID = CDT.coredatatypeID -- Changed to a left join as there were some values of NULL coredatatype. They appear to be a mix of text and booleans
	left join codes C on CAST(C.codefileID as nvarchar) = CAST(DT.valuedomain as nvarchar) and RIED.roundinstanceeventdata = C.code and DT.valuedomaintypeID = 10
	left join wb_siteproducts W on CAST(w.siteproductID as nvarchar) = CAST(RIED.roundinstanceeventdata as nvarchar) and CAST(DT.valuedomain as nvarchar) = '225|[Location_EchoID]' 



