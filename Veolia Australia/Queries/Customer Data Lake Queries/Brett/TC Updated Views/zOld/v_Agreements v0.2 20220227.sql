select 
	agreementID,
	A.agreementtypeID,
	AT.agreementtype,
	346 as echotypeID,
	agreementID as echoID,
	partyID,
	A.startdate,-- Watermark
	A.enddate, -- Watermark
	A.clientreference,
	A.paymentmethodID,
	P.paymentmethod,
	A.invoicescheduleID,
	[IS].invoiceschedule,
	A.agreementstate,
	EC.description as agreementstatedesc,
	A.contactID,
	A.siteID,
	A.invoice_contactID,
	A.invoice_siteID,
	A.accountingreference,
	--W.workorderreference
	A.onstop

	from agreements A

	LEFT JOIN agreementtypes AT on AT.agreementtypeID = A.agreementtypeID
	LEFT JOIN echocodes EC on EC.code = A.agreementstate and EC.echocodefileID = 86
	LEFT JOIN invoiceschedules [IS] on [IS].invoicescheduleID = A.invoicescheduleID
	LEFT JOIN paymentmethods P on P.paymentmethodID = A.paymentmethodID
	--LEFT JOIN workorderrefference W on W.echoID = A.agreementID and W.echotypeID = 346

