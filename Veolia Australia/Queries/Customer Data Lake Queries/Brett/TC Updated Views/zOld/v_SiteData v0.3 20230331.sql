--use RedAZEcho2_Dev3;
--use RedAZEcho2_Int;
--go


/************************************************************************************************************************************************************************
* Resources can have extended or additional data associated with them e.g. FleetObjectNo, TruckMaxWeight.
* This view provides the list of extended or additional data for resources.*  
* 
************************************************************************************************************************************************************************/

select 
	SD.sitedataID,
	SD.siteID,
	SD.parent_sitedataID,
	SD.sitetypedatatypeID,
	DT.datatypeID,
	case
		when SDT.sitetypedatatype is null then DT.datatype
		when len(trim(SDT.sitetypedatatype)) = 0 then DT.datatype
		else SDT.sitetypedatatype
	end as datatype,
	isnull(DT.valuedomaintypeID, 0) as valuedomaintypeID,
	isnull(DT.valuedomain, '0') as valuedomain,
	CASE WHEN DT.valuedomaintypeID = 10 then C.description else SD.sitedata end as sitedata -- TC Note: Have commented the below out for consistency with other views
	--case 
	--	when CDT.coredatatype <> 'boolean' then SD.sitedata
	--	else 
	--		case
	--			when SD.sitedata = '0' then 'No'
	--			when SD.sitedata = '1' then 'Yes'
	--			else SD.sitedata
	--		end
	--	end
	--as sitedata

from
	sitedata SD with (nolock)
	left join sitetypedatatypes SDT with (nolock) on SD.sitetypedatatypeID = SDT.sitetypedatatypeID
	left join datatypes DT with (nolock) on SDT.datatypeID = DT.datatypeID
	left join coredatatypes CDT with (nolock) on DT.coredatatypeID = CDT.coredatatypeID
	left join codes C on CAST(C.codefileID as nvarchar) = CAST(DT.valuedomain as nvarchar) and SD.sitedata = C.code and DT.valuedomaintypeID = 10




