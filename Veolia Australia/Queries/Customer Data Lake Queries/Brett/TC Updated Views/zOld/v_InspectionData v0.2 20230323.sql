
/************************************************************************************************************************************************************************
* Inspections can have extended or additional data associated with them e.g. Engine Hours Start, KM Start
* This view provides the list of extended or additional data for inspections.
* 
************************************************************************************************************************************************************************/

select
	ID.inspectiondataID
	,ID.inspectionID
	,ITDT.[order] as sortorder
	,ID.parent_inspectiondataID
	,ID.inspectiontypedatatypeID
	,DT.datatypeID
	,CDT.coredatatype
	,case
		when ITDT.inspectiontypedatatype is null then DT.datatype
		when len(trim(ITDT.inspectiontypedatatype)) = 0 then DT.datatype
		else ITDT.inspectiontypedatatype
	end as datatype
	,isnull(DT.valuedomaintypeID, 0) as valuedomaintypeID
	,isnull(DT.valuedomain, '0') as valuedomain
	,CASE WHEN DT.valuedomaintypeID = 10 then C.description else ID.inspectiondata end as inspectiondata-- Commenting the below out for consistency with other views

	--,case 
	--	when CDT.coredatatype <> 'boolean' then ID.inspectiondata
	--	else 
	--		case
	--			when ID.inspectiondata = '0' then 'No'
	--			when ID.inspectiondata = '1' then 'Yes'
	--			else ID.inspectiondata
	--		end
	--	end
	--as inspectiondata


from
	inspectiondata ID with (nolock)
	left join inspectiontypedatatypes ITDT with (nolock) on ID.inspectiontypedatatypeID = ITDT.inspectiontypedatatypeID
	left join datatypes DT with (nolock) on ITDT.datatypeID = DT.datatypeID
	left join coredatatypes CDT with (nolock) on DT.coredatatypeID = CDT.coredatatypeID
	left join codes C on CAST(C.codefileID as nvarchar) = CAST(DT.valuedomain as nvarchar) and ID.inspectiondata = C.code and DT.valuedomaintypeID = 10

