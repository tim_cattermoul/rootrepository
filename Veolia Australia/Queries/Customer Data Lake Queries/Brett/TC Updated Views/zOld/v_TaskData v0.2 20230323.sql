
/***********************************************************************************************************************************
* taskdata - view of extensible data related to tasks
* 
***********************************************************************************************************************************/

select 
	TD.taskdataID
	,TD.taskID
	,TD.parent_taskdataID
	,DT.datatypeID
	,CDT.coredatatype
	,case
		when TDT.tasktypedatatype is null then DT.datatype
		when len(trim(TDT.tasktypedatatype)) = 0 then DT.datatype
		else TDT.tasktypedatatype
	end as datatype
	,isnull(DT.valuedomaintypeID, 0) as valuedomaintypeID
	,isnull(DT.valuedomain, '0') as valuedomain
	,TD.taskdata -- Commenting the below out for consistency with other views
	--,case 
	--	when CDT.coredatatype <> 'boolean' then TD.taskdata
	--	else 
	--		case
	--			when TD.taskdata = '0' then 'No'
	--			when TD.taskdata = '1' then 'Yes'
	--			else TD.taskdata
	--		end
	--	end
	--as taskdata
from
	taskdata TD with (nolock)
	left join tasktypedatatypes TDT with (nolock) on TD.tasktypedatatypeID = TDT.tasktypedatatypeID
	left join datatypes DT with (nolock) on TDT.datatypeID = DT.datatypeID
	left join coredatatypes CDT with (nolock) on DT.coredatatypeID = CDT.coredatatypeID


	