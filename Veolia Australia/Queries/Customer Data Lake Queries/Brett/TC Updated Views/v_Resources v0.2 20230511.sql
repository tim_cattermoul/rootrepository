
/************************************************************************************************************************************************************************
* Purpose of this view is to provide the the list of human and vehicle resources - it acts as a reference for joins onto other views.
*  
* 
************************************************************************************************************************************************************************/
create view [dbo].[vw_v_resources] as 
select distinct
	R.resourceID,
	R.resource,
	R.clientreference,

	R.businessunitID,
	RC.resourceclass,
	RT.resourcetype,
	RT.resourcetypeID,

	R.siteID,
	R.contractID,
	REG.regionID,
	
	R.startdate, --Watermark
	R.enddate --Watermark
from
	resources R with (nolock)
	join resourcetypes RT with (nolock) on R.resourcetypeID = RT.resourcetypeID
	join resourceclasses RC with (nolock) on RT.resourceclassID = RC.resourceclassID
	left join sites S with (nolock) on R.siteID = S.siteID
	join contracts C with (nolock) on R.contractID = C.contractID
	join regions REG with (nolock) on C.regionID = REG.regionID
