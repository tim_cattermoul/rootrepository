SELECT 
	ri.roundinstanceID,
	rg.roundgroupID,
	rg.roundgroup,
	r.roundID,
	r.round,
	CONCAT(rg.roundgroup,' ',r.round) as CombinedName,
	GETDATE() as TimeDataExtracted

	FROM roundinstances RI

	INNER JOIN rounds R on R.roundID = RI.roundID
	INNER JOIN roundgroups RG on RG.roundgroupID = R.roundgroupID

	where datediff(day,lastupdated,getdate()) <= 30 and ss_date < getdate()