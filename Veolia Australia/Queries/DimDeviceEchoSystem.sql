  SELECT DISTINCT
			CONVERT(DATE,getdate()) as Day,
			D.DeviceID,
			D.model,
			REPLACE(DES.version,'com.twistedfish.OnBoard Release ','') as version,
			DES.lastreported,
			DB_name() as 'Database',
			GETDATE() as TimedataExtracted
			FROM devices D
			INNER JOIN deviceechosystems DES on DES.deviceID = D.DeviceID and DES.echosystemID = 21 -- Onboard only - exclude Mobile for devices which operate both systems
			where datediff(month,lastreported,getdate()) < 6